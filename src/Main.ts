class Main extends egret.DisplayObjectContainer {
	public constructor() {
		super();
		egret.ImageLoader.crossOrigin = 'anonymous';
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createGameScene, this);
	}
	private ready: ReadyScene;
	private platform: any = window.platform;
	private createGameScene() {

		window.platform.dataBus = DataBus.share()
		window.platform.onShareAppMessage(DataBus.share())
		DataBus.share().main = this
		RES.setMaxLoadingThread(1);
		this.loadResource() //载入资源
			.then(() => this.startGame()) //渲染准备界面
			.then(() => this.afterResource()) //载入其它资源
			.then(() => this.setupScope())
			.then(() => this.login()) //获取微信信息
			.then(() => this.getFriendScore()) //获取好友列表
			.then(() => this.InitScore()) //初始化完毕
			.then(() => this.ready.afterProgress('')) //初始化完毕
			.then(() => createCollectView(this.ready))
			.then(() => this.getConfig())
			.then(() => window.platform.EndPoint.share_content())
      .then((data) => {
        DataBus.share().share_content = data.data.data
        console.log(DataBus.share().share_content, 333333333333);
      })
			// .then(() => {
			// 	return window.platform.EndPoint.modifyUserInfo('hongbao', '10000.00', DataBus.share().userInfo)
			// })
			// .then((data) => {
			// 		console.log(data, 55555555555)
			// })
			.catch(e => console.log(e));


	}

	private async getConfig() {
		window.platform.wx['request']({
			url: "https://res7.sanliwenhua.com/wx_game/xxl_2048/config.json",
			success: (data) => {
				if (data.statusCode != 200) {
					return
				}

				DataBus.share().config = data.data
		},
		fail: (err) => {

			console.log(err)
		}
		})

	}
	private async loadResource() {
		try {
			await RES.loadConfig('default.res.json', 'resource/gif/');
			await RES.loadGroup('gif');
			await RES.loadConfig('default.res.json', 'resource/xxl_local/');
			await RES.loadGroup('local');
		} catch (e) {
			console.error(e);
		}
	}
	private async startGame() {
		return window.platform.getSystemInfo()
		.then((systemInfo) =>{
			console.log(systemInfo)
			DataBus.share().systemInfo = systemInfo
			this.ready = new ReadyScene(true);
			super.addChild(this.ready);
		})
	}

	private async afterResource() {
		try {
			// await RES.loadConfig('xxl_images.res.json', 'resource/xxl_images/');

			await RES.loadConfig('xxl_images.res.json', 'https://res7.sanliwenhua.com/wx_game/xxl_2048/xxl_images/');
			await RES.loadGroup('xxl_images', 0, this.ready);
			DataBus.share().spriteSheet = RES.getRes('xxl_images')

			// await RES.loadConfig('music.res.json', 'resource/music/');
			// await RES.loadConfig('music.res.json', 'https://res7.sanliwenhua.com/wx_game/xxl_2048/music/');
			// await RES.loadGroup('music', 0, this.ready);

			// await RES.loadConfig('default.res.json', 'resource/xxl/');
			// await RES.loadGroup('preload', 0, this.ready);

		} catch (e) {
			console.error(e);
		}
	}


	openDataCanvase: egret.Bitmap


	private async setupScope() {
		this.ready.afterProgress('获取权限...');

		return new Promise((s, f) => {
			window.platform.onShow((data) => {
				console.log(data.shareTicket)
				DataBus.share().shareTicket = data.shareTicket
				DataBus.share().playSound(DataBus.share().getVoiceOpen())
				DataBus.share().onShow()
			})
			s()
		})
		// .then(() => {
		// 	// this.createOpenDataCanvase()
		// })

		// return	this.platform.getUserInfoButton()
		// .then(() => this.platform.setupScope)
	}

	// 0: normal 1:game
	createOpenDataCanvase(type: number = 0) {
	if (this.openDataCanvase) {
		this.removeChild(this.openDataCanvase)
	}
	let width = window.platform['sharedCanvas'].width
	let height = window.platform['sharedCanvas'].height
	console.log(width, height, 1111111111111111)

	// if (type == 1) {
	// 	width = 150
	// 	height = 300
	// }
	this.openDataCanvase = window.platform.openDataContext.createDisplayObject(type, width, height)
	let scaleX = this.stage.stageWidth / width
	let scaleY = this.stage.stageHeight / height

	this.openDataCanvase.scaleX = scaleX
	this.openDataCanvase.scaleY = scaleY
	this.addChild(this.openDataCanvase)
}

removeOpenDataCanvase() {
	this.removeChild(this.openDataCanvase)
	this.openDataCanvase = null
}

	private async login() {
		this.ready.afterProgress('系统登录中...');
		if ( this.platform.UserInfo && this.platform.UserInfo.token) return this.platform.UserInfo;
		return this.platform.login().then(res1 => this.getUserInfo(res1))
		.then((userInfo) => {
			console.log(userInfo, 11111111111)
			DataBus.share().userInfo = userInfo
			let redP = Number(userInfo.values.hongbao)
			redP = Math.floor(redP * 100) / 100
			DataBus.share().redPakage = redP
			window.platform.userInfo = userInfo
		})
	}

	private async getUserInfo(res1: any) {
		this.ready.afterProgress('获取数据...');
		return this.platform.getUserInfo(true, 'withCredentials').then(res2 => this.platform.getUserToken(res1.code, res2.userInfo));
	}
	private async getFriendScore() {
		this.ready.afterProgress('获取好友列表...');
		return this.platform.FriendScoreInit();
	}
	// private async sortFriendScore() {
	// 	this.ready.afterProgress('排序好友列表...');
	// 	return this.platform.getUserSort();
	// }
	private async InitScore() {
		this.ready.afterProgress('查询历史分数...');
		return this.platform.InitScore(1);
	}
}
