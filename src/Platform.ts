/**
 * 平台数据接口。
 * 由于每款游戏通常需要发布到多个平台上，所以提取出一个统一的接口用于开发者获取平台数据信息
 * 推荐开发者通过这种方式封装平台逻辑，以保证整体结构的稳定
 * 由于不同平台的接口形式各有不同，白鹭推荐开发者将所有接口封装为基于 Promise 的异步形式
 */
interface Platform {
	wx: Object
    userInfo: Object
    EndPoint: any
    dataBus: any
	sha1(uid, s): string
	setupScope(): Promise<any>
	getUserInfo(): Promise<any>;
	login(): Promise<any>;
	getStorage(key: string): Promise<any>;
	setStorage(arg: any): Promise<any>;
	setUserCloudStorage(arg: any): Promise<any>;

  createVideoAd(id, success, fail, dataBus) : Promise<any>;

	getUserInfoButton(): Promise<any>;

  postMessage(data)
	shareAppMessage(arg: any): Promise<any>;
	onShareAppMessage(arg: any): Promise<any>;
	getLaunchOptionsSync(arg: any): Promise<any>;

	getUserToken(code: string, ui: Object): Promise<any>;
	SetScore(score: number): Promise<any>;
	getUserSort(): Promise<any>;
	setUserAvatar(user: any): Promise<any>;
	FindBefore(arg: any): Promise<any>;
	setUserToken(score: number, cache: number): Promise<any>;
	FriendScoreInit(): Promise<any>;
	InitScore(status: number): Promise<any>;
	getFriendFromSever(): Promise<any>;
	getSortTotal(score: number): Promise<any>;
	openDataContext: any
	getSystemInfo(): Promise<any>
	navigateToMiniProgram(appId)
	showModel(data)
	onShow(block)
	Storage: any
}

class DebugPlatform implements Platform {

	Storage = null

  EndPoint = null

	userInfo: Object
  dataBus = null
	sha1(uid, s) {
		return ''
	}
	wx = {}

	async onShow(block) {

	}

  async createVideoAd(id, success, fail, dataBus) {

  }

	showModel(data) {

	}

	navigateToMiniProgram(appId) {

	}

	async getSystemInfo() {

	}


	getUserInfoButton() {
		return new Promise((s, f) => {
			s()
		})
	}

	async setupScope() {
		return
	}

	async getUserInfo() {
			return { nickName: 'username' };
	}

	async login() {
		return { code: 'username' };
	}

	async getStorage(key: string = '') {
		let data = window.localStorage.getItem(key);
		try {
			let res = JSON.parse(data);
			return { data: res };
		} catch (e) {
			return { data: data };
		}
	}
	async setStorage(arg: any) {
		return window.localStorage.setItem(arg.key, typeof arg.data == 'object' ? JSON.stringify(arg.data) : arg.data);
	}
	async setUserCloudStorage(arg: any) {
		window.localStorage.setItem('KVDataList', JSON.stringify(arg));
	}
	async shareAppMessage(arg: any) {
		return {};
	}

	async onShareAppMessage(arg: any) {
		return {};
	}

	async getLaunchOptionsSync(arg: any) {
		return {};
	}
	async getFriendFromSever() {
		return {};
	}

  postMessage(data){

  }

	async FindBefore(FriendScore) {
		this.Before = this.After = false;
		FriendScore.reduce((before, next) => {
			if (next.openid == this.UserInfo.openid) this.Before = before;
			if (before.openid == this.UserInfo.openid) this.After = next;
			return next;
		}, false);
		return this.Before;
	}

	async FriendScoreInit() {

	}
	async getSortTotal(score: number) {
		return { count: 10, total: 100 };
	}

	async getUserSort() {
		return Object.keys(this.FriendScore)
			.sort((a, b) => {
				return this.FriendScore[b].score - this.FriendScore[a].score;
			})
			.map((k, i) => {
				this.FriendScore[k].sort = i;
				return this.FriendScore[k];
			});
	}
	async setUserAvatar(u: any) {
		//Promise.all(users.map(u => this.setUserAvatar(u)))
		return u.avatar
			? Promise.resolve(u)
			: new Promise((s, f) => {
					RES.getResByUrl(u.avatarUrl, s, this, RES.ResourceItem.TYPE_IMAGE);
			  }).then(avatar => {
					u.avatar = avatar;
					return u;
			  });
	}

	async getUserToken(code, ui) {
		this.UserInfo.token = '123456';
		this.UserInfo.openid = 'ooKUJ4-ZNrqoHS4PEo9dzEF5YCE0';
		this.UserInfo.score = 125;
		return this.UserInfo;
	}

	async SetScore(score: number) {
		this.setUserCloudStorage({
			KVDataList: [
				{
					key: this.baseScoreKEY,
					value: score + '',
				},
				{
					key: this.baseBoardKEY,
					value: JSON.stringify({
						wxgame: {
							score: score,
							update_time: Date.now(),
						},
					}),
				},
			],
		});
		this.setStorage({
			key: 'SuperScore',
			value: score,
		});
		this.setUserToken(score);
	}
	async setUserToken(score: number, cache: number = 50) {
		return this.getStorage('SuperScoreUpload').then((res: any) => {
			let upscore = res.data || 0;
			if (score - upscore > cache) {
				this.setStorage({
					key: 'SuperScoreUpload',
					value: score,
				});
				return { code: 200, message: '上传成功' };
			}
		});
	}
	async InitScore(status: number) {
		if (status) this.Status = status;
		return this.getStorage('SuperScore').then(res => {
			return this.setUserToken(res.data || 0, 1);
		});
	}
	public openDataContext = {
		postMessage(args: Object) {
			return Promise.resolve([
				{
					openid: 'ooKUJ4-ZNrqoHS4PEo9dzEF5YCE0',
					nickname: '潘丽敏',
					KVDataList: [{ key: this.baseScoreKEY, value: '1' }],
					avatarUrl:
						'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLnLia14uMsD4BlkwI4GQe0d9LYtyV4AXAh7zwXQLqEPNzr6qkSibqBuUtm3mEeiaAQ399icQpjaTY6Aw/132',
				},
				{
					openid: 'ooKUJ43o3e9N68ptodugyL-MRTqQ',
					nickname: '脉脉不得语',
					KVDataList: [{ key: this.baseScoreKEY, value: '2' }],
					avatarUrl: 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKd9Ok81kibwmGYwQssSqeNGQdWpOX7Cx9eiaxufjAjWAVoWIHgHbG708quD5rq8VrLBwg9hiaWax0Fw/132',
				},
				{
					openid: 'ooKUJ43o3e9N68ptodugyL-MRTqQ2',
					nickname: '脉脉不得语2',
					KVDataList: [{ key: this.baseScoreKEY, value: '10' }],
					avatarUrl: 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKd9Ok81kibwmGYwQssSqeNGQdWpOX7Cx9eiaxufjAjWAVoWIHgHbG708quD5rq8VrLBwg9hiaWax0Fw/132',
				},
			]);
		},
		createDisplayObject() {
			return new egret.Bitmap();
		},
	};
	private baseURL = 'http://caige-app.dev.max06.com';
	private baseScoreKEY = 'six-score-dev';
	private baseBoardKEY = 'six-board-dev';
	public Status: number = 1;
	public UserInfo: any = {};
	public FriendScore: any = {};
	public FriendScoreQun: any = {};
	public MySelf: any = {score: 0}
	public Before: any = null;
	public After: any = null;
	public Main: any = null;
	public System: any = { statusBarHeight: 15 };
	public QunId: string;
	public baseShare: any = {
		title: '自从玩了这款游戏，时间过得飞快。',
		imageUrl: 'resource/share.png',
	};
}


declare let platform: Platform;

declare interface Window {
	platform: Platform;
}

if (!window.platform) {
	window.platform = new DebugPlatform();
}
