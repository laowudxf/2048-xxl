class Modal extends egret.Sprite {
	protected platform: any = window.platform;
	constructor() {
		super();
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createSprite, this);
	}
	public Close: DefaultButton;
	protected BgImg: string = 'modal_box';
	private createSprite() {
		this.Mask();
		let bg = this.Background();
		this.SetClose(bg.x, bg.y, bg.width);
		this.Content(bg);
	}
	private Mask() {
		let mask: egret.Shape = new egret.Shape();
		mask.graphics.beginFill(0x000000, 0.3);
		mask.graphics.drawRect(0, 0, this.stage.stageWidth, this.stage.stageHeight);
		mask.touchEnabled = true;
		this.addChild(mask);
		return mask;
	}
	private Background() {
		let img: egret.Bitmap = new egret.Bitmap(getRes(this.BgImg));
		img.x = (this.stage.stageWidth - img.width) / 2;
		img.y = (this.stage.stageHeight - img.height) / 2;
		this.addChild(img);
		return img;
	}
	protected Content(bg: egret.Bitmap) {}
	private SetClose(x, y, w) {
		this.Close = new DefaultButton(x + w - 40, y - 20, 60, 60);
		this.Close.lineChange = [125, 135];
		this.Close.Content.text = 'X';
		this.addChild(this.Close);
		this.Close.addEventListener(egret.TouchEvent.TOUCH_TAP, this.CloseSelf, this);
	}
	public CloseSelf() {
		this.parent.removeChild(this);
	}
	protected addTitleChild(title: egret.TextField, y: number, size: number = 40) {
		title.y = y;
		title.size = size;
		title.width = this.stage.stageWidth;
		title.textAlign = egret.HorizontalAlign.CENTER;
		title.verticalAlign = egret.VerticalAlign.MIDDLE;
		title.filters = [new egret.GlowFilter(0xffffff, 0.8, 20, 20, 1, egret.BitmapFilterQuality.LOW)];
		this.addChild(title);
	}
}
class HelpModal extends Modal {
	public Button: DefaultButton = new DefaultButton(0, 0, 304, 68);
	protected Content(bg: egret.Bitmap) {
		this.Title(bg.y + 24);
		this.TextContent(bg.y);
		this.DefaultButton(bg.y + bg.height - 115);
	}
	private Title(y) {
		let title: egret.TextField = new egret.TextField();
		title.text = '- 复活 -';
		this.addTitleChild(title, y, 40);
	}
	private TextContent(y) {
		let content: egret.TextField = new egret.TextField();
		content.y = y + 88;
		content.height = 200;
		content.width = this.stage.stageWidth;
		content.textFlow = <Array<egret.ITextElement>>[
			{ text: '好友助力\n可', style: { textColor: 0xffffff, size: 34 } },
			{ text: '立即复活', style: { textColor: 0x8dfbff, size: 34 } },
			{ text: '一次', style: { textColor: 0xffffff, size: 34 } },
		];
		content.lineSpacing = 30;
		content.textAlign = egret.HorizontalAlign.CENTER;
		content.verticalAlign = egret.VerticalAlign.MIDDLE;
		this.addChild(content);
		return content;
	}
	private DefaultButton(y: number) {
		this.Button.x = (this.stage.stageWidth - 304) / 2;
		this.Button.y = y;
		this.Button.Content.text = '好友助力复活';
		this.addChild(this.Button);
	}
}
class BackModal extends Modal {
	public Button: DefaultButton;
	public CurrentScore: number = 0;
	public HighScore: number = 0;
	protected Content(bg: egret.Bitmap) {
		this.Title(bg.y + 24);
		this.TextContent(bg.y);
		this.DefaultButton(bg.y + bg.height - 115);
		this.BackButton(bg.y + bg.height - 115);
	}
	private Title(y: number) {
		let title: egret.TextField = new egret.TextField();
		title.text = '- 提示 -';
		this.addTitleChild(title, y, 40);
	}
	private TextContent(y: number) {
		let content: egret.TextField = new egret.TextField();
		content.y = y + 88;
		content.height = 200;
		content.width = this.stage.stageWidth;
		content.textFlow = <Array<egret.ITextElement>>[
			{ text: '当前得分', style: { textColor: 0xffffff, size: 34 } },
			{ text: '' + this.CurrentScore, style: { textColor: 0x8dfbff, size: 34 } },
			{ text: '\n最高得分: ' + this.HighScore, style: { textColor: 0xffffff, size: 34 } },
		];
		content.lineSpacing = 30;
		content.textAlign = egret.HorizontalAlign.CENTER;
		content.verticalAlign = egret.VerticalAlign.MIDDLE;
		this.addChild(content);
		return content;
	}
	private DefaultButton(y: number) {
		let close = new DefaultButton(this.stage.stageWidth / 2 - 200, y, 168, 68);
		close.Content.text = '取消';
		close.lineChange = [135, 150];
		this.addChild(close);
		close.addEventListener(egret.TouchEvent.TOUCH_TAP, this.CloseSelf, this);
	}
	private BackButton(y: number) {
		this.Button = new DefaultButton(this.stage.stageWidth / 2 + 32, y, 168, 68);
		this.Button.Content.text = '退出';
		this.Button.lineChange = [];
		this.addChild(this.Button);
		this.Button.addEventListener(egret.TouchEvent.TOUCH_TAP, this.CloseSelf, this);
	}
}

class TopModal extends Modal {
	public Button: DefaultButton;
	protected BgImg: string = 'modal_top';
	public HighScore: number = 0;
	public Super: any[] = [];
	protected Content(bg: egret.Bitmap) {
		bg.y -= 60;
		this.Title(bg.y + 200);
		this.DrawScore(bg.y + 260);
		this.DrawLine(bg.y + 440);
		if (this.Super.length) {
			this.SuperContent(bg.y + 440, 110, `成功超越${this.Super.length}位好友`);
			this.SuperUsers(bg.y + 540);
		} else {
			this.SuperContent(bg.y + 440, bg.height - 440, '未超越任何好友，请继续努力');
		}
		this.DefaultButton(bg.y + bg.height + 50);
	}
	private Title(y: number) {
		let title: egret.TextField = new egret.TextField();
		title.text = '- 历史最高分 -';
		this.addTitleChild(title, y, 34);
	}
	private DrawScore(y: number) {
		let content: egret.BitmapText = new egret.BitmapText();
		content.font = getRes('hygy_big');
		content.text = '' + this.HighScore;
		content.y = y;
		content.width = this.stage.stageWidth * 2;
		content.textAlign = egret.HorizontalAlign.CENTER;
		content.scaleX = content.scaleY = 0.5;
		this.addChild(content);
	}
	private DrawLine(y: number) {
		let line: egret.Shape = new egret.Shape();
		line.graphics.lineStyle(1, 0xffffff, 0.5);
		line.graphics.moveTo(134, y);
		line.graphics.lineTo(616, y);
		this.addChild(line);
	}
	private SuperContent(y: number, h: number, text: string) {
		let content: egret.TextField = new egret.TextField();
		content.y = y;
		content.height = h;
		content.width = this.stage.stageWidth;
		content.text = text;
		content.textAlign = egret.HorizontalAlign.CENTER;
		content.verticalAlign = egret.VerticalAlign.MIDDLE;
		this.addChild(content);
	}
	private SuperUsers(y: number) {
		let Super = this.Super.slice(-5);
		let startX = (this.stage.stageWidth - Super.length * 118 + 10) / 2;
		Super.forEach((user, i) => {
			this.platform.setUserAvatar(user).then(() => {
				let img: CircleImg = new CircleImg(startX + i * 118, y, 108);
				img.Img = user.avatar;
				img.border = 2;
				this.addChild(img);
			});
		});
	}
	private DefaultButton(y: number) {
		this.Button = new DefaultButton(this.stage.stageWidth / 2 - 230, y, 460, 78);
		this.Button.Content.text = '炫耀';
		this.Button.Content.size = 34;
		// this.Button.lineChange = [135, 150];
		this.addChild(this.Button);
	}
}
