class Desktop extends egret.Sprite {
	public constructor(baseX: number, baseY: number) {
		super();
		this.baseXY = new egret.Point(baseX, baseY);
		this.x = baseX;
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createSprite, this);
	}
	private hexSide: number = 4; //阵列边长
	public cards: Object = {}; //阵列内容
	public cardKeys: Array<string> = []; //阵列键名数组
	private baseXY: egret.Point;
	isFreeLife = false

	public nowDownCards: Card[]

	//周围的格子偏移
	private aroundMap = [
		[0,-1],
		[0, 1],
		[-1, 1],
		[-1,0],
		[1, 0],
		[1, -1]
	]
	//界面被添加
	private createSprite() {
		this.setHexagonGrid();
		this.y = this.baseXY.y + this.height / 2;
	}
	//设置棋盘
	private setHexagonGrid() {

		Card.calcRadius(this.stage.stageWidth)
		this.anchorOffsetX = Math.sqrt(3) / 2 * Card.allRadius
		this.anchorOffsetY = Card.allRadius

			let hexSide = this.hexSide - 1;
			for (let q = -hexSide; q <= hexSide; q++) {
				for (let r = Math.max(-hexSide, -q - hexSide), r2 = Math.min(hexSide, -q + hexSide); r <= r2; r++) {
					let key = q + '|' + r;
					this.cardKeys.push(key);
					let card = new Card(q, r)
					this.cards[key] = this.addChild(card);
				}
			}

	}

	//清除已整齐的点并输出消除数量（重叠部分算多次）
	public ClearBlock() {
		if (!this.nowDownCards) {
			return 0
		}

		let promise = null


		let result = null
		if (this.handleCards) {
			result = this.handleCards.cards.reduce((r, x) => {return x.shouldHighlight || r}, false)
		}

		if (this.handleCards && result) {
			if (this.handleCards.cards[1].shouldHighlight) {
				let c1 = this.nowDownCards[0]
				let c2 = this.nowDownCards[1]
				this.nowDownCards[0] = c2
				this.nowDownCards[1] = c1
			}
		} else {
			this.nowDownCards.sort((a, b) => {
				return a.value - b.value
			})
		}

		for (let downCard of this.nowDownCards) {
			if (promise == null) {
					promise = this.cardClear(downCard)
			} else {
				let t_combo = 0
				promise = promise.then((score) => {
					t_combo = score.combo
					return this.cardClear(downCard, score.combo, score.score)
				})
				.then((score) => {
					if (score.combo > t_combo && this.nowDownCards[0].isHit) {
						return this.cardClear(this.nowDownCards[0], score.combo, score.score)
					}
					return score
				})
			}
		}
		return promise
	}

	public aroundCard(card: Card) {
		return this.aroundMap.map( x => {
			let hex = <Hex>{}
			hex.q = x[0]
			hex.r = x[1]
			let addHex = HexHelper.add(hex, card.hex)
			let maxV = this.hexSide - 1
			if (Math.abs(addHex.q) > maxV || Math.abs(addHex.r) > maxV) {
				return
			}
			return this.boardCard(addHex)
		}).filter(x => x != null)
	}

	public calcMinMaxValue() {

		let min = 0
		let max = 0
		for (let key in this.cards ) {

			let card = this.cards[key] as Card
			if (card.isHit == false) {
				continue
			}

			let value = card.value
			if (min == 0 && max == 0) {
				min = value
				max = value
			} else {
				if (value > max) {
					max = value
				}
				if (value < min) {
					min = value
				}
			}
		}

		if (min > DataBus.share().minValue) {
			DataBus.share().minValue = min
		}

		if (max > DataBus.share().maxValue) {
			DataBus.share().maxValue = max
		}

	}

	private baseClearNumber = 3

	merge_e_time = 300
	showScore_time = 300
	showScoreGap_time = 100
	private async cardClear(card: Card, combo: number = 0, currentScore: number = 0) {
		return new Promise((s, f) => {
			let matchArray = this.oneCardClear(card)

			matchArray.map(x => x.matchMark = false)
			if (matchArray.length >= (combo > 0 ? 2: 3) || (card != null && card.value == 2048)) {

				//2048
				console.log(card.value)

				if (card.value == 2048) { //放置2048道具
					this.combo_2048(combo, card, (b, getScore) => {
						if (b) {
							s({score: currentScore + getScore, combo})
						} else {
							s({score: currentScore, combo})
						}
					})
					return
				}

				let willClearCards = matchArray.filter(x => x != card)
				willClearCards.map((c, index) => {

					let eImage = new egret.Bitmap(getRes('E@2x'))
					eImage.x = c.x
					eImage.y = c.y
					eImage.anchorOffsetX = eImage.width / 2
					eImage.anchorOffsetY = eImage.height / 2
					eImage.x = rectCenter(c).x
					eImage.y = rectCenter(c).y
					let dest = rectCenter(card)
					this.addChild(eImage)

					egret.Tween.get(eImage)
					.to({x: dest.x, y: dest.y}, this.merge_e_time)
					.call(() => {
						this.removeChild(eImage)
						if (index != 0) {
							return
						}
						//合并
						willClearCards.map(x => x.ChangeStatus(5))
						card.updateValue(card.value * 2)

					let light = new egret.Bitmap(getRes('white_3'))
					light.alpha = 0.5
					light.scaleX = 1.2
					light.scaleY = 1.2
					light.width = card.width
					light.height = card.height
					light.anchorOffsetX = light.width / 2
					light.anchorOffsetY = light.height / 2
					light.x = rectCenter(card).x
					light.y = rectCenter(card).y
					this.addChild(light)


					egret.Tween.get(light)
					.to({scaleX: 1.4, scaleY: 1.4, alpha: 0}, 500)
					.wait(200)
					.call(() => {

						this.removeChild(light)

						if (card.value == 2048) {
							this.combo_2048(combo, card, (b, getScore) => {
								if (b) {
									s({score: currentScore + getScore, combo})
								} else {
									s({score: addedScore, combo})
								}
							})
						} else {
							comboBlock()
						}

					})

						let muti = (matchArray.length - this.baseClearNumber + combo) * 2
						let addedScore = 4 * card.value * (muti == 0 ? 1 : muti)


						let t_combo = combo
						if (t_combo >= 2)  {
							t_combo -= 2
							// if (t_combo >= 0)  {
							// 	t_combo -= 0
							let nameList = ['good', 'excellent', 'great']
							let nameList1 = ['good', 'excellent', 'amazing']

							if (t_combo > (nameList.length - 1)) {
								t_combo -= (t_combo - (nameList.length - 1))
							}

							let combo_name = nameList[t_combo]

							DataBus.share().playSoundWith( combo_name)
							card.showIncentive('font_' + nameList1[t_combo])
						}

						t_combo = combo + 1
						if (t_combo > 4) {
							t_combo = 4
						}
						let name = '03_0' + t_combo
						DataBus.share().playSoundWith(name)
						DataBus.share().addScore(addedScore)



						let comboBlock = () => {
							// let aroundCards = this.aroundCard(card)

							let aroundCards = [card]
							let promise: Promise<any> = null
							promise =	aroundCards.reduce((p, x) => {
								if (p == null) { //如果只有一个只执行这句
									return this.cardClear(x, combo + 1, 0)
								} else {
									return p.then((score) => {
										return this.cardClear(x, combo + 1, score)
									})
								}
							}, promise)

							// 积分规则
							if (promise == null) {
								return addedScore
							} else {
								// 子链分数 + 本节点分数
								promise.then((score) => s({score: score.score + addedScore, combo: score.combo}))
							}
						}


						//得分动画
						let getScoreLabel = new egret.TextField()
						getScoreLabel.text = addedScore.toString()
						getScoreLabel.anchorOffsetX = getScoreLabel.width / 2
						getScoreLabel.anchorOffsetY = getScoreLabel.height / 2
						getScoreLabel.x = rectCenter(card).x
						getScoreLabel.y = rectCenter(card).y
						this.addChild(getScoreLabel)

						egret.Tween.get(getScoreLabel)
						.to({y: getScoreLabel.y - 50, scaleX: 1.6, scaleY: 1.6}, this.showScore_time).wait(this.showScoreGap_time)
						.to({y: getScoreLabel.y - 100}, this.showScore_time)
						.call(() => {
							this.removeChild(getScoreLabel)
						})
				})
			})
		} else {
			s({currentScore, combo})
		}
	})
}

private combo_2048(combo_num = 0, card , callBack) {
	let shouldShowBox = (combo_num != 0)
	DataBus.share().playSoundWith('boom')
	let aroundCards = this.aroundCard(card)

	let getScore = aroundCards.filter((x) => x.isHit == true).reduce((cost, x) => {
		let getScore = x.value * (combo_num + 1)
		console.log('getScore:' + getScore)

		let getScoreLabel = new egret.TextField()
		getScoreLabel.text = getScore.toString()
		getScoreLabel.anchorOffsetX = getScoreLabel.width / 2
		getScoreLabel.anchorOffsetY = getScoreLabel.height / 2
		getScoreLabel.x = rectCenter(x).x
		getScoreLabel.y = rectCenter(x).y
		this.addChild(getScoreLabel)

		let starImg = new egret.Bitmap(getRes('bo_small'))
		starImg.anchorOffsetX = starImg.width / 2
		starImg.anchorOffsetY = starImg.height / 2
		starImg.x = rectCenter(x).x
		starImg.y = rectCenter(x).y
		this.addChild(starImg)

		starImg.scaleX = 0
		starImg.scaleY = 0

		let star_light = new egret.Bitmap(getRes('star_light'))
		star_light.anchorOffsetX = star_light.width / 2
		star_light.anchorOffsetY = star_light.height / 2
		star_light.x = rectCenter(x).x
		star_light.y = rectCenter(x).y
		this.addChild(star_light)

		star_light.scaleX = 0
		star_light.scaleY = 0

		x.ChangeStatus(5)

		egret.Tween.get(starImg).wait(100)
		.to({scaleX: 0.4, scaleY: 0.4}, 400, egret.Ease.quintOut)
		.to({scaleX: 0.5, scaleY: 0.5, alpha: 0}, 200)
		.call(() => {
			this.removeChild(starImg)
		})

		egret.Tween.get(star_light).wait(100)
		.to({scaleX: 1, scaleY: 1}, 400, egret.Ease.quintOut)
		.to({scaleX: 0.5, scaleY: 0.5, alpha: 0}, 200)
		.call(() => {
			this.removeChild(star_light)
		})

		egret.Tween.get(getScoreLabel)
		.to({y: getScoreLabel.y - 50, scaleX: 1.6, scaleY: 1.6}, this.showScore_time).wait(this.showScoreGap_time)
		.to({y: getScoreLabel.y - 100}, this.showScore_time)
		.call(() => {
			this.removeChild(getScoreLabel)
		})

		cost += getScore
		return cost
	}, 0)


	let boomImg = new egret.Bitmap(getRes('xuanfen'))
	setCenterAnchor(boomImg)
	boomImg.x = rectCenter(card).x
	boomImg.y = rectCenter(card).y
	this.addChild(boomImg)

	boomImg.scaleX = 2
	boomImg.scaleY = 2

	let red_cycle = new egret.Bitmap(getRes('red_cycle'))
	setCenterAnchor(red_cycle)
	red_cycle.x = rectCenter(card).x
	red_cycle.y = rectCenter(card).y
	this.addChild(red_cycle)

	red_cycle.scaleX = 0
	red_cycle.scaleY = 0
	red_cycle.anchorOffsetX += 20
	red_cycle.anchorOffsetY -= 20
	red_cycle.alpha = 0.2

	let rainbow_cycle = new egret.Bitmap(getRes('rainbow_cycle'))
	setCenterAnchor(rainbow_cycle)
	rainbow_cycle.x = rectCenter(card).x
	rainbow_cycle.y = rectCenter(card).y
	this.addChild(rainbow_cycle)

	rainbow_cycle.scaleX = 0
	rainbow_cycle.scaleY = 0
	rainbow_cycle.alpha = 0.3

	let bo = new egret.Bitmap(getRes('bo'))
	setCenterAnchor(bo)
	bo.x = rectCenter(card).x
	bo.y = rectCenter(card).y
	this.addChild(bo)

	bo.scaleX = 0
	bo.scaleY = 0

	egret.Tween.get(boomImg)
	.to({scaleX: 0.3, scaleY: 0.3, rorate: 180, alpha: 0.5}, 500)
	.call(() => {
		this.removeChild(boomImg)
		DataBus.share().addScore(getScore)

		egret.Tween.get(red_cycle)
		.to({scaleX: 2, scaleY: 2, alpha: 0.2}, 300, egret.Ease.quintOut)
		.to({alpha: 0}, 200)

		egret.Tween.get(rainbow_cycle)
		.to({scaleX: 2, scaleY: 2, alpha: 0.3}, 300, egret.Ease.quintOut)
		.to({alpha: 0}, 200)

		egret.Tween.get(bo)
		.to({scaleX: 1, scaleY: 1, alpha: 0.3}, 300, egret.Ease.quintOut)
		.to({alpha: 0}, 200)
		.wait(200).call(() => {
			card.ChangeStatus(5)
			// s({score: currentScore + getScore, combo_num})
			callBack(true, getScore)
			if (shouldShowBox) {
				DataBus.share().add2048Count()
			}
		})
	})

	if (aroundCards.filter((x) => x.isHit == true).length == 0) {
		callBack(false)
		// return	s({score: addedScore, combo_num})
	}

}

private oneCardClear(card: Card): Card[] {

	if (card == null || card.isHit == false) {
		return []
	}

	let aroundCard = this.aroundCard(card)
	let matchedCards = aroundCard.filter(x => {
		let flag = x != null && x.value == card.value && x.isHit && x.matchMark == false && (x.matchMark = true)
		return flag
	})

	// console.log(`middle - hex: q:${card.hex.q}, r:${card.hex.r} ; matched: ${matchedCards.map(x => 'q:' + x.hex.q + ' r:' + x.hex.r + ';')}`)

	matchedCards =  matchedCards.concat(matchedCards.reduce((result, v) => result.concat(this.oneCardClear(v)), []))
	return matchedCards
}

public boardCard(hex: Hex): Card {
	let key = hex.q + '|' + hex.r;
	return this.cards[key]
}

//检查碰撞
public checkHit(handleCards: Cards) {
	let card = this.cardKeys.reduce((res, key) => {
		if (res) return res;
		let card = this.cards[key];
		let needOffset = handleCards.needOffset
		let testX = 0
		let testY = 0

		testX = card.x + this.x
		testY = card.y + this.y

		return handleCards.Zero.hitTestPoint(testX, testY) ? card : res;
	}, false);
	return card || null;
}


handleCards: Cards
//检查是否完全碰撞
public checkAllHit(handleCards: Cards, handleCard: Card) {
	let hitCards = handleCards.Random.map((point: number[]) => {
		let handleKey = handleCard.hex.q + point[0] + '|' + (handleCard.hex.r + point[1]);
		return this.cards[handleKey];
	});
	let result = hitCards.every(c => c && !c.isHit) ? hitCards : null;
	if (result) {
		result.map( (card, index) => {
			card.value = (handleCards.getChildAt(index) as Card).value
		})
		this.handleCards = handleCards
		this.nowDownCards = hitCards

	}

	return result
}

//重置棋盘（复活） old
public relife() {
	this.cardKeys.forEach(key => {
		this.cards[key].ChangeStatus();
	});
}

//
public freeLife(complete) { //随机生成3个2048 爆炸

	let p:Promise<any> = null

	for (let i = 0; i < 3; i++) {
		if (p == null) {
			p = this.freeLifeOnece()
		} else {
			p.then(() => {
				setTimeout(() => {
					this.freeLifeOnece()
				}, 500 * i)
			})
		}
	}
	p.then(() => {
		console.log('freeLife complete')
		if (complete) {
			complete()
		}
	})

	// let randomCards = [1, 2, 3].map(() => this.generatorRandowCard())
	//
	// let cards = randomCards.map(card => {
	// 	card.updateValue(2048)
	// 	return card
	// })
	//
	// this.nowDownCards = cards.filter(x => x != null)
	// this.isFreeLife = true
	// this.ClearBlock()
	// .then((score) => {
	// 	console.log('freeLife complete')
	// 	if (complete) {
	// 		complete()
	// 	}
	// })

	DataBus.share().currentLife += 1
	DataBus.share().setGameOver(false)
}

freeLifeOnece() {
	return new  Promise((s, f) => {

		let randomCards = [1].map(() => this.generatorRandowCard())

		let cards = randomCards.map(card => {
			card.updateValue(2048)
			return card
		})

		this.nowDownCards = cards.filter(x => x != null)
		this.isFreeLife = true
		this.ClearBlock()
		.then(() => {
				s()
		})
	})
}

generatorRandowCard(): Card {
	let card: Card
	while (card == null) {
		let keys = []
		for (let key in this.cards) {
			keys.push(key)
		}

		let index = Math.floor(Math.random() * keys.length)
		card = this.cards[keys[index]]
		if (card.isHit && card.value != 2048) {
			continue
		} else {
			card = null
		}
	}
	return card
}

public isGameOver () {
	let isGameOver = true
	for (let key in this.cards) {
		isGameOver = isGameOver && (this.cards[key].isHit == true)
	}

	console.log('isGameOver', isGameOver)
	DataBus.share().setGameOver(isGameOver)
	return isGameOver
}

public checkValid(cardsHex): boolean {
	let isGameOver = true
	for (let key in this.cards) {
		isGameOver = isGameOver && (this.cards[key].isHit == true)
	}

	DataBus.share().setGameOver(isGameOver)
	if (isGameOver) {
		return false
	}

	if (cardsHex.length == 1) {
		return true
	}

	for (let key in this.cards) {
		let card = this.cards[key] as Card
		if (card.isHit) {
			continue
		}
		let hex_next = cardsHex[1]
		let boardHex = card.hex
		let boardHex_1 = HexHelper.add(hex_next, boardHex)

		let nextCard = this.cardWithHex(boardHex_1)
		if (nextCard && nextCard.isHit == false) {
			return true
		}
	}
	return false
}

public checkValidCards(cards: Cards): boolean {
	let cardsHex = cards.getHexs()
	return this.checkValid(cardsHex)

}

public cardWithHex(hex: Hex) {
	let key = hex.q + '|' + hex.r;
	return this.cards[key]
}

async saveDeskState() {
	let stateInfos = {}
	for (let key in this.cards)  {
		stateInfos[key] = this.cards[key].stateInfo()
	}
	console.log('save desk:')
	window.platform.setStorage({key: 'deskState', data: stateInfos})
}

async popDeskState() {
	let stateInfos = await window.platform.getStorage('deskState')

	console.log('pop desk')
	console.log(stateInfos.data)
	stateInfos = stateInfos.data
	if (stateInfos == null) {
		return
	}

	for (let key in stateInfos) {
		let card = this.cards[key]
		if (card == null) {
			return
		}
		card.setStateInfo(stateInfos[key])

	}

}
}
