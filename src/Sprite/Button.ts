class Button extends egret.Sprite {
	public constructor(x: number, y: number, w: number, h: number) {
		super();
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
		this.touchEnabled = true;
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createSprite, this);
	}
	protected createSprite() {
		this.drawBackground();
		this.drawContent();
	}
	protected drawBackground() {}
	protected drawContent() {}
	protected drawText(Content: egret.TextField) {
		Content.width = this.width;
		Content.height = this.height;
		Content.textAlign = egret.HorizontalAlign.CENTER;
		Content.verticalAlign = egret.VerticalAlign.MIDDLE;
		this.addChild(Content);
	}
}
class DefaultButton extends Button {
	constructor(x: number, y: number, w: number, h: number) {
		super(x, y, w, h);
	}
	public lineChange: number[] = [140, 190];
	public Content: egret.TextField = new egret.TextField();
	//绘制分数
	protected drawContent() {
		this.Content.textColor = 0xffffff;
		this.Content.size = this.height / 2;
		this.Content.fontFamily = '微软雅黑,黑体';
		this.drawText(this.Content);
	}
	//绘制背景条
	protected drawBackground() {
		let height = this.height / 2;
		this.graphics.lineStyle(3, 0xffffff);
		if (this.lineChange.length) {
			this.graphics.beginGradientFill(egret.GradientType.LINEAR, [0x9d32f8, 0xf870ce], [1, 1], this.lineChange, new egret.Matrix());
		}
		this.graphics.drawArc(height, height, height, Math.PI * 0.5, Math.PI * 1.5, false);
		this.graphics.lineTo(this.width - height, 0);
		this.graphics.drawArc(this.width - height, height, height, Math.PI * 1.5, Math.PI * 0.5, false);
		this.graphics.lineTo(height, height * 2);
		this.graphics.endFill();
	}
}
class IconButton extends Button {
	constructor(content: string, x: number, y: number, w: number, h: number) {
		super(x, y, w, h);
		this.Content = content;
	}
	private Content: string;
	//绘制分数
	protected drawContent() {
		let img: egret.Bitmap = new egret.Bitmap(getRes(this.Content));
		img.x = (this.width - img.width) / 2;
		img.y = (this.height - img.height) / 2;
		this.addChild(img);
	}
	//绘制背景条
	protected drawBackground() {
		this.graphics.beginFill(0x7965ae, 1);
		this.graphics.drawCircle(this.width / 2, this.height / 2, Math.min(this.width, this.height) / 2);
		this.graphics.endFill();
	}
}
class BackButton extends Button {
	constructor(x: number, y: number, w: number, h: number) {
		super(x, y, w, h);
	}
	//绘制分数
	protected drawContent() {
		this.graphics.beginFill(0xffffff, 0);
		this.graphics.drawRect(0, 0, this.width * 2, this.height);
		this.graphics.endFill();
		this.graphics.lineStyle(6.5, 0xffffff);
		this.graphics.moveTo(this.width, 0);
		this.graphics.lineTo(0, this.height / 2);
		this.graphics.lineTo(this.width, this.height);
	}
}
