class UserScore extends egret.Sprite {
	constructor() {
		super();
		this.width = 290;
		this.height = 108;
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createSprite, this);
	}
	public Score: egret.TextField = new egret.TextField();
	private UserAvatar: CircleImg;
	private Status: boolean = false;
	private createSprite() {
		this.Score.width = this.width;
		this.Score.height = this.height;
		this.Score.textAlign = egret.HorizontalAlign.CENTER;
		this.Score.verticalAlign = egret.VerticalAlign.MIDDLE;
		this.addChild(this.Score);
	}
	public SetUserAvatar(avatar: egret.Texture, isRight: boolean = false) {
		if (this.Status && this.UserAvatar) {
			this.Status = false;
			this.removeChild(this.UserAvatar);
		}
		if (avatar) {
			this.Status = true;
			this.UserAvatar = new CircleImg((isRight ? this.width : 0) - 48, 0, 96);
			this.UserAvatar.border = 6;
			this.UserAvatar.Img = avatar;
			this.addChild(this.UserAvatar);
		}
	}
}
