class Cards extends egret.Sprite {
	private Tiles = [
		[[0, 0]],
		[[0, 0], [1, 0]],
		[[0, 0], [0, 1]],
		[[0, 0], [1, -1]]
	]
	private center: egret.Point; //默认坐标
	public color: number; //默认颜色
	public Zero: Card; //虚拟中心点
	public Random: Array<Array<number>>; //卡组阵列
	public needOffset = false
	private baseXY: egret.Point; //中心点
	public hadMoved = false
	public validator: Desktop

	cards: Card[]

	private inAnimation = false

	is2048 = false

	public constructor(baseX: number, baseY: number, validator: Desktop ,is2048: boolean = false) {
		super();
		this.is2048 = is2048
		this.baseXY = new egret.Point(baseX, baseY);
		this.touchEnabled = true;
		this.validator = validator
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createGameSprite, this);
	}
	//界面被添加
	public createGameSprite() {
		this.removeChildren();
		this.setSize();
		this.setZero();
		this.setup()
	}

	setup() {
		if (this.cards.length == 2) {
			if (this.cards[0].value == this.cards[1].value) {
				this.cards[0].setHighlight()
			}
		}
	}

	isTile3(random) {
			if (random.length == 1) {
				return false
			}
			let a = random[0]
			let b = random[1]
			return (a[0] == 0 && a[1] == 0 && b[0] == 1 && b[1] == -1)
	}
	//设置随机卡组阵列
	private setSize() {

	//清除上次画的内容，使width height 计算正确
		this.graphics.clear();


		// let tilesArr =this.transformHexsFromTitle()
		// let a = tilesArr.filter(x => {
		// 	return this.validator.checkValid(x)
		// })
		let validTiles = this.Tiles.filter(tile => {
			let tmp = this.transformHexsFromTile(tile)
			return this.validator.checkValid(tmp)
		})

		console.log(validTiles)
		if (validTiles.length == 0) {
			return
		}
		let randomIndex = Math.floor(Math.random() * validTiles.length)

		if (this.is2048 == true) {
			randomIndex = 0
		}

		// this.Random = this.Tiles[randomIndex]
		this.Random = validTiles[randomIndex]

		this.cards = []
		this.Random.forEach((item, index) => {
			let card = new Card(item[0], item[1]);
			this.cards.push(card)
			if (index == 0) {
				this.Zero = card
			}

			if (this.isTile3(this.Random)) {
				this.needOffset = true
			} else {
					this.needOffset = false
			}
			card.needOffset = this.needOffset
			card.isBeReady = true
			this.addChild(card);
			card.ChangeStatus(2, this.color);

			if (this.is2048) {
				card.updateValue(2048)
			}
		});

		this.x = this.baseXY.x
		this.y = this.baseXY.y
		this.center = new egret.Point(this.x, this.y);

		this.anchorOffsetX = this.width / 2
		this.anchorOffsetY = this.height / 2
	}

	public getHexs(): Hex[] {
			return this.Random.map(x => {
			 let hex = <Hex>{}
				hex.q = x[0]
				hex.r = x[1]
				return hex
			})
	}

	// private Tiles = [
	// 	[[0, 0]],
	// 	[[0, 0], [1, 0]],
	// 	[[0, 0], [0, 1]],
	// 	[[0, 0], [1, -1]]
	// ]
	public transformHexsFromTile(tile) {
		return tile.map(x => {
			let hex = <Hex>{}
			hex.q = x[0]
			hex.r = x[1]
			return hex
		})
		// return this.Tiles.map(x => {
		// 	return x.map(x => {
		// 		let hex = <Hex>{}
		// 		hex.q = x[0]
		// 		hex.r = x[1]
		// 		return hex
		// 	})
		// })
	}

	//deprecated
	//设置虚拟中心点
	private setZero() { }

	//设置缩放
	public Scale(scale: number = 0.8) {
		this.scaleX = scale;
		this.scaleY = scale;
	}
	//设置位置
	public Move(x: number = 0, y: number = 0) {
		this.x = x || this.center.x;
		this.y = y || this.center.y;
		if (x && y) {
			if (Math.abs(x) + Math.abs(y) > 100) {
				this.hadMoved = true
			}
		} else {
			// console.log('move zero x: ' + this.x + 'y: ' + this.y)
		}

	}

	public rotatCard(evt: egret.TouchEvent) {

		if (this.hadMoved) {
			console.log(111111111111)
			// this.hadMoved = false
			return
		}

		if (this.inAnimation) {
			return
		}
		DataBus.share().playSoundWith('02')
		if (this.cards.length == 1) {
			return
		}

		this.inAnimation = true
		let tw = egret.Tween.get(this)

		//里面的数字动画
		let rotation = (this.rotation + 180)
		this.$children.map(x => (x as Card).rotatCard(500))
		tw.to({rotation}, 500)
		.call(() => {
				this.rotation = 0
				if (this.numChildren == 2) {
					let c1 = this.getChildAt(0) as Card
					let c2 = this.getChildAt(1) as Card
					let a = c1.value
					let b = c2.value

					c1.updateValue(b, false)
					c2.updateValue(a, false)

					if(c1.shouldHighlight) {
						c2.setHighlight()
						c1.setHighlight(false)
					} else if (c2.shouldHighlight) {
						c2.setHighlight(false)
						c1.setHighlight()
					}
				}
				this.inAnimation = false
		})
	}

	saveState() {
		let stateInfos = {}
	 stateInfos['cards'] = this.cards.map(card => {
		return card.stateInfo()
	 })
		console.log('save cards')
		window.platform.setStorage({key: 'cardsState', data: stateInfos})
	}

	async popState() {

		let stateInfos = await window.platform.getStorage('cardsState')

		console.log('pop cards')
		console.log(stateInfos.data)
		stateInfos = stateInfos.data

		if (stateInfos == null) {
			return
		}

		let cards = stateInfos['cards'].map(cardInfo => {
			let card = new Card(cardInfo['hex'].q, cardInfo['hex'].r)
			card.setStateInfo(cardInfo, false)
			return card
		})

		this.setStateInfo(cards)
	}

	setStateInfo(cards: Card[]) {
		this.removeChildren();
		this.cards = cards
		let needOffset = false
		this.Random = this.cards.map((card, index) => {
			if (index == 0) {
				this.Zero = card
			}
			needOffset = needOffset || (card.hex.q == 1 && card.hex.r == -1)
			return [card.hex.q, card.hex.r]
		})
		this.needOffset = needOffset
		this.cards.map(card => {
			card.needOffset = needOffset
			this.addChild(card)
		})

		this.anchorOffsetX = this.width / 2
		this.anchorOffsetY = this.height / 2
	}

}
