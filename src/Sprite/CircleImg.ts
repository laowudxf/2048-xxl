class CircleImg extends egret.Sprite {
	public constructor(x: number, y: number, r: number) {
		super();
		this.x = x;
		this.y = y;
		this.width = this.height = r;
		this.touchEnabled = true;
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createSprite, this);
	}
	private createSprite() {
		this.drawBackground();
		this.drawContent();
	}
	private drawBackground() {
		if (this.border) {
			let imgbg: egret.Shape = new egret.Shape();
			imgbg.graphics.beginFill(0x786d9d, 1);
			imgbg.graphics.drawCircle(this.width / 2, this.height / 2, this.width / 2 + this.border);
			imgbg.graphics.endFill();
			this.addChild(imgbg);
		}
	}
	public border: number = 0;
	public Img: egret.Texture;
	private drawContent() {
		let img: egret.Bitmap = new egret.Bitmap(this.Img);
		img.width = img.height = this.width;
		this.addChild(img);
		img.mask = this.setCircleMaskHead();
	}
	private setCircleMaskHead() {
		var circle: egret.Shape = new egret.Shape();
		circle.graphics.beginFill(0, 1);
		circle.graphics.drawCircle(this.width / 2, this.height / 2, this.width / 2);
		circle.graphics.endFill();
		this.addChild(circle);
		return circle;
	}
}
