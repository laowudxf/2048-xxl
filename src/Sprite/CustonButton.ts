class customButtom extends BaseView {

  public backgroundImage: egret.Bitmap
  public textField: egret.TextField
  public clickBlock: () => void

  init() {
  this.backgroundImage = new egret.Bitmap()
    this.textField = new egret.TextField()
    this.backgroundImage = new egret.Bitmap()
    this.textField.x = 0
    this.textField.y = 0
    this.textField.size = 20
    this.textField.textAlign = 'center'
    this.textField.verticalAlign = 'middle'
    
    this.backgroundImage.x = 0
    this.backgroundImage.y = 0
		this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.clicked, this);

  }

   setupUI() {
    this.textField.width = this.frame.width
    this.textField.height = this.frame.height

    this.backgroundImage.width = this.frame.width
    this.backgroundImage.height = this.frame.height
    this.addChild(this.backgroundImage)
    this.addChild(this.textField)
  }

  public clicked() {
    if (this.clickBlock) {
      this.clickBlock()
    }
  }

}
