interface Hex {
	q: number;
	r: number;
}

class HexHelper {
  static add(left: Hex, right: Hex): Hex {
		let result = <Hex>{}
		result.q = left.q + right.q
		result.r = left.r + right.r
		return result
	}
}

class Card extends egret.Sprite {

	public isHitBlock: boolean = false; //是否虚拟渲染
	public hex: Hex; //中心相对坐标
	public color: number; //默认方块颜色
	public isHit: boolean = false; //是否被碰撞
	public isBeReady = false
	public shouldRestore = false
	private radius: number = Card.allRadius - 1; //六边形半径
	public Allradius: number = Card.allRadius; //六边形半径

	public static allRadius = 40

	// for 2048-xxl
	public value: number
	public valueTextField = new egret.TextField()
	public matchMark = false //计算消除的时候的标记
	private static valueList = [2, 4, 8, 16, 32, 64, 128, 256, 512]
	// private static valueList = [512, 256]
	private bgImage = new egret.Bitmap(getRes('card'))
	private numberBgImage = new egret.Bitmap()
	private numberImage = new egret.Bitmap()
	// private static valueList = [2]

	public needOffset = false
	whiteImage = new egret.Bitmap(getRes('white_2'))

	public shouldHighlight = false

	private static generatorValue() {

		// return this.valueList[Math.floor(Math.random() *  this.valueList.length)]

		let min = 2
		let max = DataBus.share().maxValue
		if (max >= 256) {
			min = 8
		}

		if (max > 512) {
			max = 512
		}

		let lists = this.valueList.filter(x => x >= min && x <= max)

		return lists[Math.floor(Math.random() *  lists.length)]
		// return 2
	}

	public constructor(q: number, r: number) {
		super();
		this.hex = { q, r };
		this.hex2pixel(q, r);
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createSprite, this);

	//	test
		this.touchEnabled = true
		this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.clicked, this);
	}

	 clicked() {
		 if (this.isHit && DataBus.share().getIsUseHammer()) { //在棋盘上的棋子
			 DataBus.share().playSoundWith('hammer')
			 console.log('card tap')
			 DataBus.share().setIsUseHammer(false)
			 DataBus.share().gameToolData['setHammerCount'](DataBus.share().gameToolData['getHammerCount']() - 1)
			 this.showHammerAnimation()
		 } else if (DataBus.share().getIsUseHammer()) {
			 DataBus.share().setIsUseHammer(false)
		 }
	}

	showIncentive(imageName: string) {
		let img = new egret.Bitmap(getRes(imageName))
		img.anchorOffsetX = img.width / 2
		img.anchorOffsetY = img.height / 2
		img.x = this.stage.stageWidth / 2
		img.y = this.stage.stageHeight / 2  - 200
		this.parent.parent.addChild(img)
		egret.Tween.get(img)
		.to({scaleX: 1.3, scaleY: 1.3, y: img.y - 100}, 200).wait(200)
		.to({scaleX: 2, scaleY: 2, alpha: 0.1}, 400)
		.call(() => {
			this.parent.parent.removeChild(img)
		})
	}

	cardDisappearAnimation() {

		let starImg = new egret.Bitmap(getRes('bo_small'))
		setCenterAnchor(starImg)
		starImg.x = this.width / 2
		starImg.y = this.height / 2
		this.addChild(starImg)

		starImg.scaleX = 0
		starImg.scaleY = 0

		let star_light = new egret.Bitmap(getRes('star_light'))
		setCenterAnchor(star_light)
		star_light.x = this.width / 2
		star_light.y = this.height / 2
		this.addChild(star_light)

		star_light.scaleX = 0
		star_light.scaleY = 0


		this.ChangeStatus(5)
		egret.Tween.get(starImg).wait(100)
		.to({scaleX: 0.4, scaleY: 0.4}, 400, egret.Ease.quintOut)
		.to({scaleX: 0.5, scaleY: 0.5, alpha: 0}, 200)
		.call(() => {
			this.removeChild(starImg)
		})

		egret.Tween.get(star_light).wait(100)
		.to({scaleX: 1, scaleY: 1}, 400, egret.Ease.quintOut)
		.to({scaleX: 0.5, scaleY: 0.5, alpha: 0}, 200)
		.call(() => {
			this.removeChild(star_light)
		})
	}

	showHammerAnimation() {
		let hammer = new egret.Bitmap(getRes('hammer'))
		setCenterAnchor(hammer)
		this.addChild(hammer)
		hammer.x = this.width / 2 - 10
		hammer.y = this.height / 2 - 10


		let star = new egret.Bitmap(getRes('star'))
		setCenterAnchor(star)
		this.addChild(star)
		star.x = this.width / 2
		star.y = this.height / 2

		star.scaleX = 0
		star.scaleY = 0


		egret.Tween.get(hammer)
		.to({rotation: 45}, 500)
		.to({rotation:-45}, 500)
		.call(() => {
			this.removeChild(hammer)

			this.cardDisappearAnimation()
			// egret.Tween.get(star)
			// .to({scaleX: 4, scaleY: 4, alpha: 0.1}, 300)
			// .call(() => {
			// 	this.removeChild(star)
			// 	this.ChangeStatus(5)
			// })
		})

	}

	//根据相对坐标设置坐标
	private hex2pixel(q: number, r: number) {
		this.x = this.Allradius * Math.sqrt(3) * (q + r / 2);
		this.y = ((this.Allradius * 3) / 2) * r;
	}

	public static calcRadius(stageWidth: number, horizentalCount: number = 7, gap: number = 15) {
		let boardWidht = stageWidth - 2 * gap
		Card.allRadius = boardWidht / horizentalCount / Math.sqrt(3)
	}


	public updateValue(value: number, inBoard: boolean = true) {
		this.value = value
		this.drawSix(1, inBoard)
		this.updateNumberImage()
	}

	//界面被添加
	private createSprite() {
		this.setup()
		if (this.isBeReady) {
			this.value = Card.generatorValue()
		}
		this.isHitBlock ? this.ChangeStatus(1) : this.ChangeStatus(0, this.color);


		// this.showHammerAnimation()

	}

	private setup() {
		this.valueTextField.textColor = 0x000000
		this.valueTextField.width = this.radius * Math.sqrt(3)
		this.valueTextField.height = this.radius * 2
		this.valueTextField.anchorOffsetX = this.valueTextField.width / 2
		this.valueTextField.anchorOffsetY = this.valueTextField.height / 2
		this.valueTextField.x = this.valueTextField.anchorOffsetX
		this.valueTextField.y = this.valueTextField.anchorOffsetY - 5

		let w = this.radius * Math.sqrt(3)
		let h = this.radius * 2

		this.numberImage.x = w / 2 - 2
		this.numberImage.y = h / 2 - 6
		this.numberImage.anchorOffsetX = this.numberImage.width / 2
		this.numberImage.anchorOffsetY = this.numberImage.height / 2

		// this.anchorOffsetX = 0
		// this.anchorOffsetY = 0

		this.valueTextField.textAlign = egret.HorizontalAlign.CENTER
		this.valueTextField.verticalAlign = egret.VerticalAlign.MIDDLE
		this.valueTextField.size = 24
		this.addChild(this.bgImage)

		let gap = 5

		// this.numberBgImage.y += gap
		this.numberBgImage.width = this.bgImage.width
		this.numberBgImage.height = this.bgImage.height
		this.addChild(this.numberBgImage)

		// this.whiteImage.visible = false
		this.whiteImage.width = this.bgImage.width
		this.whiteImage.height = this.bgImage.height
		setCenterAnchor(this.whiteImage)
		this.whiteImage.x = this.whiteImage.width / 2
		this.whiteImage.y = this.whiteImage.height / 2

		egret.Tween.get(this.whiteImage, {loop: true})
		.to({scaleX: 1.1, scaleY: 1.1}, 500)
		.to({scaleX: 1, scaleY: 1}, 500)



		if (this.needOffset) {
			this.valueTextField.y += 1.5 * this.Allradius
			this.numberImage.y += 1.5 * this.Allradius
			this.bgImage.y += 1.5 * this.Allradius
			this.numberBgImage.y += 1.5 * this.Allradius
			this.whiteImage.y += 1.5 * this.Allradius
		}


	}

	setHighlight(highlight = true) {
		this.shouldHighlight = highlight
		if (highlight) {
				this.addChild(this.whiteImage)
		} else {
			this.removeChild(this.whiteImage)
		}

		// if (this.shouldHighlight) {
		// 	this.graphics.clear();
		// 	this.graphics.lineStyle(3, 0xffffff, 1, true, '', egret.CapsStyle.ROUND, egret.JointStyle.ROUND)
		// 	for (var i = 0; i < 7; i++) {
		//
		// 		let shapeY =  -Math.sin(((i * 60 + 30) / 180) * Math.PI) * this.radius + this.Allradius
		// 		if (this.needOffset) {
		// 			shapeY += 1.5 * this.Allradius
		// 		}
		// 		this.graphics[i == 0 ? 'moveTo' : 'lineTo'](
		// 			Math.cos(((i * 60 + 30) / 180) * Math.PI) * this.radius + Math.sqrt(3) / 2 * this.Allradius,
		// 			shapeY,
		// 		);
		// 	}
		//
		// 	this.graphics.endFill();
		// } else {
		// 	this.graphics.clear();
		// }
	}

	//渲染六边形
	private drawSix(alpha: number = 1, inBoard: boolean = true) {


	if (alpha == 1) { //棋子
		this.show(true, inBoard)
	} else {
		this.show(false)
	}

	if (this.isHit || this.isBeReady || this.shouldRestore) {
		this.updateNumberImage()
		this.addChild(this.numberImage)
	} else {
		if (this.numberImage.parent == this) {
			this.removeChild(this.numberImage)
		}
	}
}

public updateNumberImage() {
	this.numberImage.texture = getRes('number_' + this.value)
	this.bgImage.texture = getRes(this.value + "@2x")
	this.numberImage.anchorOffsetX = this.numberImage.width / 2
	this.numberImage.anchorOffsetY = this.numberImage.height / 2
}

private show(shouldShow: boolean = true, inBoard: boolean = true) {
	if (shouldShow) {
		if (inBoard) {
			this.numberBgImage.texture = getRes(this.value + "@2x")
			this.bgImage.texture = getRes('card')
			this.numberBgImage.visible = true
		} else {

			this.numberBgImage.visible = false
			this.bgImage.texture = getRes(this.value + "@2x")
		}
	} else {
		this.numberBgImage.visible = false
	}
}

//渲染虚拟碰撞节点
private drawHit() {
// this.graphics.lineStyle(3, 0x56abcd);
this.scaleX = 0.8;
this.scaleY = 0.8;
let offsetX = Math.cos(Math.PI / 6) * this.radius;
this.graphics.clear();
this.graphics.lineStyle(1, 0x000000);
this.graphics.beginFill(this.color, 1);
this.graphics.drawRect(this.x + offsetX, this.y + this.radius, this.x - offsetX * 2, this.y - this.radius * 2);
this.graphics.endFill();
}
//添加滤镜
public ChangeStatus(status: number = 0, color: number = 0x000000) {
//1：虚拟点，2:棋子，3：棋子Hover棋盘，4:棋盘Hover状态转为嵌入，5:棋子销毁，默认：棋盘底
switch (status) {
case 1:
return this.drawHit();
case 2:
this.color = color;
return this.drawSix(1, false);
case 3:
this.bgImage.texture = getRes('card_selected')
return this.drawSix(0.5);
case 4:
this.isHit = true;
return this.drawSix(1);
case 5:
// this.removeEventListener(egret.Event.ENTER_FRAME, this.onClear, this);
this.isHit = false;
// let arr1 = this.gradientColor(this.color, 0xffffff, 30);
// let arr2 = this.gradientColor(0xffffff, 0xcccccc, 30);
// this.targetColor = arr1.concat(arr2);
// this.targetColor.forEach((color, i) => {
// 	this.graphics.beginFill(color, (1-Math.abs(i-30) / 30) + 0.4);
// 	this.graphics.drawRect(0, i * 20 - this.parent.y - this.y, 20, 20);
// 	this.graphics.endFill();
// });
				// break;
				// return this.addEventListener(egret.Evens.ENTER_FRAME, this.onClear, this);
			default:
				this.bgImage.texture = getRes('card')
				// this.color = color;
				this.isHit = false;
				this.drawSix(0.4);
		}
	}
	private targetColor = [];

	private onClear() {
		this.color = this.targetColor.shift();
		if (!this.color) {
			this.color = 0xcccccc;
			this.removeEventListener(egret.Event.ENTER_FRAME, this.onClear, this);
		}
		this.drawSix(1 - Math.abs(this.targetColor.length - 30) / 30 + 0.4);
	}


	public offsetZeroPoint() {
	}

	public rotatCard(interval: number = 100) {
		let tw = egret.Tween.get(this.numberImage)
		tw.to({rotation: 360}, interval)
	}

	stateInfo() {
		return {
			isHit: this.isHit,
			value: this.value,
			shouldHighlight: this.shouldHighlight,
			hex: this.hex
		}
	}

	setStateInfo(stateInfo, inBoard = true) {
		let isHit = stateInfo.isHit
		this.value = stateInfo.value
		this.isHit = isHit
		this.shouldRestore = (inBoard == false)
		if (stateInfo.shouldHighlight != null) {
			this.setHighlight(stateInfo.shouldHighlight)
		}

		if (isHit || inBoard == false) {
			this.ChangeStatus(inBoard ? 4 : 2)
		} else {
			this.ChangeStatus()
		}

		if (this.shouldRestore) {
			this.updateValue(this.value)
		}

	}
}
