class ScoreScene extends egret.Sprite {
	private platform: any = window.platform;
	public constructor() {
		super();
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createGameScene, this);
	}
	//界面被添加
	private createGameScene() {
		this.SceneBackground();
		this.SceneTopBar((this.platform.System.statusBarHeight * this.stage.stageHeight) / 667 - 10);
		this.SceneTitle((150 * this.stage.stageHeight) / 1334);
		this.SceneScore((224 * this.stage.stageHeight) / 1334);
		this.ScenePK((384 * this.stage.stageHeight) / 1334);
		this.SceneSort((524 * this.stage.stageHeight) / 1334);
		this.SceneButton1((1050 * this.stage.stageHeight) / 1334);
		this.SceneButton2((1173 * this.stage.stageHeight) / 1334);
	}
	//初始化背景
	private SceneBackground() {
		var img: egret.Bitmap = new egret.Bitmap(RES.getRes('play_bg'));
		img.width *= this.stage.stageHeight / img.height;
		img.x = (this.stage.stageWidth - img.width) / 2;
		img.height = this.stage.stageHeight;
		this.addChild(img);
	}
	//初始化顶部
	private SceneTopBar(y: number) {
		let back = new BackButton(50, y, 20, 40);
		back.addEventListener(egret.TouchEvent.TOUCH_TAP, this.BackToReady, this);
		this.addChild(back);
		let Content = new egret.TextField();
		Content.size = 40;
		Content.y = y;
		Content.textColor = 0xffffff;
		Content.text = '六边形';
		Content.width = this.stage.stageWidth;
		Content.textAlign = egret.HorizontalAlign.CENTER;
		Content.verticalAlign = egret.VerticalAlign.MIDDLE;
		this.addChild(Content);
	}
	//第一行标题
	private SceneTitle(y: number) {
		let title: egret.TextField = new egret.TextField();
		title.text = '- 本次得分 -';
		title.y = y;
		title.size = 34;
		title.width = this.stage.stageWidth;
		title.textAlign = egret.HorizontalAlign.CENTER;
		title.verticalAlign = egret.VerticalAlign.MIDDLE;
		title.filters = [new egret.GlowFilter(0xffffff, 0.8, 20, 20, 1, egret.BitmapFilterQuality.LOW)];
		this.addChild(title);
	}
	public CurrentScore: number = 0;
	//本次得分
	private SceneScore(y: number) {
		let content: egret.BitmapText = new egret.BitmapText();
		content.font = RES.getRes('hygy_big');
		content.text = '' + this.CurrentScore;
		content.y = y;
		content.width = this.stage.stageWidth * 2;
		content.textAlign = egret.HorizontalAlign.CENTER;
		content.scaleX = content.scaleY = 0.5;
		this.addChild(content);
	}
	//全球占比与最高分
	private ScenePK(y: number) {
		let title: egret.TextField = new egret.TextField();
		title.lineSpacing = 30;
		title.y = y;
		title.size = 34;
		title.width = this.stage.stageWidth;
		title.textAlign = egret.HorizontalAlign.CENTER;
		title.verticalAlign = egret.VerticalAlign.MIDDLE;
		this.addChild(title);
		this.platform.getSortTotal(this.CurrentScore).then(res => {
			title.textFlow = [
				{ text: '已击败', style: { size: 30 } },
				{ text: Math.ceil((res.count / res.total) * 100) + '%', style: { size: 40, textColor: 0x89f1f8 } },
				{ text: '的玩家', style: { size: 30 } },
				{ text: '\n历史最高分：' + this.platform.MySelf.score, style: { size: 30 } },
			];
		});
	}
	//大板块
	private SceneSort(y: number) {
		let img: egret.Bitmap = new egret.Bitmap(RES.getRes('modal_result'));
		img.x = (this.stage.stageWidth - img.width) / 2;
		img.y = y;
		this.addChild(img);

		let title: egret.TextField = new egret.TextField();
		title.text = '排行榜';
		title.x = img.x + 20;
		title.y = img.y + 30;
		title.size = 30;
		this.addChild(title);

		let right: egret.TextField = new egret.TextField();
		right.text = '查看全部排行 >';
		right.y = img.y + 30;
		right.size = 30;
		right.touchEnabled = true;
		right.addEventListener(egret.TouchEvent.TOUCH_TAP, this.BackToBoard, this);
		this.addChild(right);
		right.x = img.x + img.width - 20 - right.width;

		this.platform
			.FriendScoreInit()
			.then(() => this.platform.getUserSort())
			.then(() => [this.platform.Before, this.platform.MySelf, this.platform.After].filter(u => u))
			.then(Users => {
				let Width = img.width / Users.length;
				Users.forEach((user, i) => {
					if (user.openid == this.platform.MySelf.openid) {
						let bg: egret.Shape = new egret.Shape();
						bg.graphics.beginGradientFill(egret.GradientType.LINEAR, [0x9d32f8, 0xf870ce], [1, 1], [200, 230], new egret.Matrix());
						bg.graphics.drawRect(Width * i + img.x, img.y + 90, Width, img.height - 90);
						bg.graphics.endFill();
						this.addChild(bg);
					}

					let sort: egret.TextField = new egret.TextField();
					sort.text = user.sort + 1 + '';
					sort.y = img.y + 120;
					sort.size = 70;
					sort.textColor = 0xffd617;
					sort.x = Width * i + img.x;
					sort.width = Width;
					sort.textAlign = egret.HorizontalAlign.CENTER;
					this.addChild(sort);

					let name: egret.TextField = new egret.TextField();
					name.text = user.nickname;
					name.y = img.y + 350;
					name.size = 30;
					name.x = sort.x;
					name.width = Width;
					name.textAlign = egret.HorizontalAlign.CENTER;
					this.addChild(name);

					let score: egret.TextField = new egret.TextField();
					score.text = user.score;
					score.y = img.y + 400;
					score.size = 54;
					score.textColor = 0x89f1f8;
					score.x = name.x;
					score.width = Width;
					score.textAlign = egret.HorizontalAlign.CENTER;
					this.addChild(score);

					this.platform.setUserAvatar(user).then(() => {
						let avatar: CircleImg = new CircleImg(score.x + (Width - 128) / 2, img.y + 200, 128);
						avatar.Img = user.avatar;
						avatar.border = 5;
						this.addChild(avatar);
					});
				});
			});
	}
	//查看全部排行
	private BackToBoard() {
		var desk: BoardScene = new BoardScene();
		desk.Scene = 1;
		this.parent.addChild(desk);
		this.parent.removeChild(this);
	}
	private SceneButton1(y: number) {
		let Button = new DefaultButton(this.stage.stageWidth / 2 - 230, y, 460, 78);
		Button.Content.text = '发起挑战';
		Button.Content.size = 34;
		this.addChild(Button);
		Button.addEventListener(egret.TouchEvent.TOUCH_TAP, this.BackToPK, this);
	}
	private SceneButton2(y: number) {
		let Button = new DefaultButton(this.stage.stageWidth / 2 - 230, y, 460, 78);
		Button.Content.text = '再玩一局';
		Button.Content.size = 34;
		Button.lineChange = [];
		this.addChild(Button);
		Button.addEventListener(egret.TouchEvent.TOUCH_TAP, this.BackToPlay, this);
	}
	//退出页面
	private BackToReady() {
		var desk: ReadyScene = new ReadyScene();
		this.parent.addChild(desk);
		this.parent.removeChild(this);
	}
	//退出页面
	private BackToPlay() {
		var desk: PlayScene = new PlayScene();
		this.parent.addChild(desk);
		this.parent.removeChild(this);
	}
	//退出页面
	private BackToPK() {
		var desk: PlayScene = new PlayScene();
		this.parent.addChild(desk);
		this.parent.removeChild(this);
	}
}
