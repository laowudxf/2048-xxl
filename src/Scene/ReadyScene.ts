class ReadyScene extends egret.Sprite implements RES.PromiseTaskReporter , Observer{
	dailyRewardView: DailyRewardView
	private platform: any = window.platform;
	private bgImage: egret.Bitmap

	private titleImage2: egret.Bitmap
	private start_light: egret.Bitmap

	public constructor(isLogin: boolean = false) {
		super();
		this.isLogin = isLogin;
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createGameScene, this);
	}
	private TitanY: number;

	voiceBtn: customButtom
	subscribe(before: any, now: any, key: string,instance: Observable) {
		switch (key) {
			case 'voiceOpen':
			let v = now as boolean
			this.voiceBtn.backgroundImage.texture = getRes(v ? 'voice_open_2': 'voice_close_2')
			break
			case 'redPakage':
			console.log('redPakage----------', now)
					this.redPakageCount.text = '￥' + now
			break
			case 'dailyRewardTimeOffset':
			this.timeLabel.text = DataBus.share().dailyRewardTimeString
			this.timeLabel.visible = DataBus.share().canGetDailyReward == false
			break
			default:
			break
		}

	}

	//开始渲染
	private createGameScene() {

		window.platform.wx['aldSendEvent']("进入游戏首页")
		this.SceneBackground();
		this.topContent()


		// this.SceneUpBackground((348 * this.stage.stageHeight) / 1334);
		this.TitanY = (779 * this.stage.stageHeight) / 1334;
		this.isLogin ? this.SceneProgress(this.stage.stageHeight * 0.8) : this.SceneTitanBar();
		DataBus.share().registerObserver(this, 'voiceOpen')
	}

	timeLabel = new egret.TextField()
	timeControl() {
		DataBus.share().registerObserver(this, 'dailyRewardTimeOffset')
		this.timeLabel.visible = DataBus.share().canGetDailyReward == false
		this.timeLabel.text = '00:00:00'
		this.timeLabel.anchorOffsetX = this.timeLabel.width / 2
		this.timeLabel.x = maxX(this.groupRanksBtn) - this.groupRanksBtn.width / 2
		this.timeLabel.y = maxY(this.groupRanksBtn) + 10
		this.addChild(this.timeLabel)
	}




	highScoreLabel = new egret.TextField()
	scoreBg: BaseView
	private async topContent() {
		let titleImage = new egret.Bitmap(RES.getRes('2048_title'));
		let imageWidth = this.stage.stageWidth * 0.7
		titleImage.height *= imageWidth / titleImage.width
		titleImage.width = imageWidth
		titleImage.anchorOffsetX = titleImage.width / 2
		titleImage.x = this.stage.stageWidth / 2
		if (DataBus.share().systemInfo.model.indexOf('iPhone X') != -1) {
			titleImage.y = 110 + 60
		} else {
			titleImage.y = 110
		}
		this.addChild(titleImage);

		this.titleImage2 = titleImage



		let scoreBg = new BaseView(new egret.Rectangle(0, 0, 300, 60))
		scoreBg.cornerRadius = 30
		scoreBg.bgColorAlpha = 0.2
		scoreBg.backgroundColor = 0
		scoreBg.anchorOffsetX = scoreBg.width / 2
		scoreBg.anchorOffsetY = scoreBg.height / 2
		scoreBg.x = this.stage.stageWidth / 2
		this.scoreBg = scoreBg
		this.addChild(scoreBg)

		let score	 = await DataBus.share().getHighestScore()
		this.highScoreLabel.size = 35
		this.highScoreLabel.textColor = 0xfce145
		this.setHighScore(score)
		this.highScoreLabel.anchorOffsetX = this.highScoreLabel.width / 2
		this.highScoreLabel.x = this.stage.stageWidth / 2
		this.highScoreLabel.y = maxY(this.titleImage2) + 55
		scoreBg.y = this.highScoreLabel.y + this.highScoreLabel.height / 2

		this.addChild(this.highScoreLabel)
	}

	setHighScore(score) {
		this.highScoreLabel.text = '最高分：' + score.toString()
		this.scoreBg.anchorOffsetX = this.scoreBg.width / 2
		this.scoreBg.width = this.highScoreLabel.width + 30
	}

	//渲染背景
	private SceneBackground() {
		var img: egret.Bitmap = new egret.Bitmap(RES.getRes('ready_bg'));
		img.width *= this.stage.stageHeight / img.height;
		img.x = (this.stage.stageWidth - img.width) / 2;
		img.height = this.stage.stageHeight;
		this.bgImage = img
		this.addChild(img);
	}

	private SceneUpBackground(y: number) {
		// var text: egret.Bitmap = new egret.Bitmap(RES.getRes('ready_text'));
		// text.y = y;
		// this.addChild(text);
	}
	//添加按钮
	private SceneTitanBar() {
		let shareBtn = new customButtom(new egret.Rectangle(this.stage.stageWidth / 2, maxY(this.titleImage2) + 160, 332, 105))
		shareBtn.anchorOffsetX = 332 / 2
		shareBtn.backgroundImage.texture = getRes('share1_1')
		shareBtn.clickBlock = () => {
			window.platform.wx['aldSendEvent']("分享")
			this.platform.shareAppMessage({})
		}
		shareBtn.visible = false
		this.addChild(shareBtn)

		let startBtn = new customButtom(new egret.Rectangle(this.stage.stageWidth / 2, maxY(shareBtn) + 10 + 182 / 2, 634, 182))
		startBtn.anchorOffsetX = startBtn.width / 2
		startBtn.anchorOffsetY = startBtn.height / 2
		startBtn.backgroundImage.texture = RES.getRes('start_game')
		startBtn.clickBlock = () => {
			window.platform.wx['aldSendEvent']("开始游戏")
			this.Start()
		}
		this.addChild(startBtn)

		egret.Tween.get(startBtn, {
			loop: true
		})
		// .wait(500)
		.to({scaleX: 0.93, scaleY: 0.93}, 800)
		.to({scaleX: 1, scaleY: 1}, 800)

		let v = new BaseView()
		v.x = startBtn.x - startBtn.width / 2
		v.y = startBtn.y
		v.width = startBtn.width
		v.height = startBtn.height
		this.addChild(v)

		/*
		let startLight = new egret.Bitmap(getRes('start_light'));
		let imageHeight =  startBtn.height
		startLight.width *= imageHeight / startLight.height
		startLight.height = imageHeight
		startLight.x = -startLight.width
		v.addChild(startLight)

		let maskImage = new egret.Bitmap(getRes('start_game'));
		let startBgImageView = startBtn.backgroundImage
		maskImage.width = startBgImageView.width - 10
		maskImage.height = startBgImageView.height
		v.addChild(maskImage)
		v.mask = maskImage
		maskImage.x = 5

		egret.Tween.get(startLight, {loop: true})
		.to({x: startBtn.width}, 1500)
		*/


		let voiceBtn = new customButtom(new egret.Rectangle(14, 84, 92, 92))
		voiceBtn.backgroundImage.texture = getRes(DataBus.share().getVoiceOpen() ? 'voice_open_2' : 'voice_close_2')
		voiceBtn.clickBlock = () => {
			window.platform.wx['aldSendEvent']("声音")
				let v = DataBus.share().getVoiceOpen()
				DataBus.share().setVoiceOpen(!v)
		}

		voiceBtn.textField.text = "声音"
		voiceBtn.textField.size = 25
		voiceBtn.textField.textColor = 0xffffff
		this.addChild(voiceBtn)
		voiceBtn.textField.y += 60
		this.voiceBtn = voiceBtn

		let moreBtn = new customButtom(new egret.Rectangle(14, maxY(voiceBtn) + 60, 92, 92))
		moreBtn.backgroundImage.texture = RES.getRes('ready_more')
		moreBtn.clickBlock = () => {
			this.platform.shareAppMessage({})
			// window.platform.navigateToMiniProgram('wxaa3e45e4b6a1bbd0')

			// window.platform.wx['aldSendEvent']("更多好玩")
			// window.platform.navigateToMiniProgram('wx32f39e398bd85f6d')
		}

		moreBtn.textField.text = "分享"
		moreBtn.textField.size = 25
		moreBtn.textField.textColor = 0xffffff
		this.addChild(moreBtn)
		moreBtn.textField.y += 60

		let ranksBtn = new customButtom(new egret.Rectangle(14, maxY(moreBtn) + 60, 92, 92))
		// let ranksBtn = new customButtom(new egret.Rectangle(30, this.height - 200, 90, 100))
		ranksBtn.backgroundImage.texture = RES.getRes('rank')
		ranksBtn.clickBlock = () => {
			window.platform.wx['aldSendEvent']("排行榜")
			this.platform.FriendScoreInit()
			.then(() => {
				this.bannerAd.hide()
				let v = new RankView(rectCreate(0, 0, this.width, this.height))
					v.sence = 1
					window.platform.wx['aldSendEvent']("进入排行榜页面")
				this.addChild(v)
			})
		}
		ranksBtn.textField.text = "排行榜"
		ranksBtn.textField.size = 25
		ranksBtn.textField.textColor = 0xffffff
		this.addChild(ranksBtn)
		ranksBtn.textField.y += 60


		let groupRanksBtn = new customButtom(new egret.Rectangle(this.stage.stageWidth - 20 - 40, startBtn.y - 150 - startBtn.height / 2, 103, 122))
		// let groupRanksBtn = new customButtom(new egret.Rectangle(this.stage.stageWidth - 20 - 40, startBtn.y - 150 + 122, 103, 122))
		this.groupRanksBtn = groupRanksBtn
		groupRanksBtn.anchorOffsetX = 122 / 2
		// groupRanksBtn.anchorOffsetX = 122 / 2
		groupRanksBtn.backgroundImage.texture = RES.getRes('daily_reward')
		groupRanksBtn.clickBlock = () => {

			if (DataBus.share().canGetDailyReward == false) {
				return
			}

			window.platform.wx['aldSendEvent']("每日登录奖励弹框")
			window.platform.wx['aldSendEvent']("进入每日登录奖励页面")
			this.bannerAd.hide()
			this.dailyRewardView = new DailyRewardView(new egret.Rectangle(0, 0,this.stage.stageWidth, this.stage.stageHeight))
			this.addChild(this.dailyRewardView)
			this.removeChild(this.moreGameBtn)
			this.addChild(this.moreGameBtn)
			this.removeChild(this['collectTip'])
			this.addChild(this['collectTip'])
			this.dailyRewardView.closeBlock = () => {
				this.bannerAd.show()
			}
			this.dailyRewardView.ensureBlock = () => {
				this.bannerAd.show()
				window.platform.wx['aldSendEvent']("每日奖励")
			}
		}
		this.addChild(groupRanksBtn)

		this.timeControl()
		//animation
		let a_time = 400
		let start_y = groupRanksBtn.y
		let start_scale_x = groupRanksBtn.scaleX
		let start_scale_y = groupRanksBtn.scaleY
		let scale_offset = 0.1

		egret.Tween.get(groupRanksBtn, {loop: true})
		.wait(a_time * 3)
		.to({y: start_y - 20, scaleX: start_scale_x - scale_offset}, a_time)
		.to({y: start_y, scaleX: start_scale_x}, a_time)
		.to({scaleX: start_scale_x + scale_offset}, a_time / 2)
		.to({scaleX: start_scale_x}, a_time / 2)

		// egret.Tween.get(groupRanksBtn, {loop: true})
		// .wait(a_time * 3)
		// .to({y: start_y, scaleY: start_scale_x - scale_offset}, a_time)
		// .to({y: start_y, scaleY: start_scale_x}, a_time)
		// .to({scaleX: start_scale_x + scale_offset}, a_time / 2)
		// .to({scaleX: start_scale_x}, a_time / 2)

		this.bannerAd = createAd('adunit-ace9a30b32b22559', DataBus.share().systemInfo.screenHeight)


		let flauntBtn = new customButtom(new egret.Rectangle(30, this.titleImage2.y + 120, 110, 150))
		flauntBtn.backgroundImage.texture = getRes('flaunt')
		flauntBtn.clickBlock = () => {
			this.platform.shareAppMessage({})
		}

		// this.addChild(flauntBtn)
		flauntBtn.anchorOffsetX = flauntBtn.width
		flauntBtn.y = ranksBtn.y
		flauntBtn.x = this.stage.stageWidth - 10

		/*
		if (DataBus.share().calcDateGetDailyReward() && DataBus.share().hadShowDailyReward == false) {
			DataBus.share().hadShowDailyReward = true
			this.dailyRewardView = new DailyRewardView(new egret.Rectangle(0, 0,this.stage.stageWidth, this.stage.stageHeight))
			this.bannerAd.hide()
			this.addChild(this.dailyRewardView)
			this.dailyRewardView.closeBlock = () => {
				this.bannerAd.show()
			}
			this.dailyRewardView.ensureBlock = () => {
				this.bannerAd.show()
				window.platform.wx['aldSendEvent']("每日奖励")
				groupRanksBtn.backgroundImage.texture = getRes('group_rank')
			}
		}
		*/

			let moreGameBtn = new customButtom(new egret.Rectangle(0, this.stage.stageHeight / 2 - 60, 69, 95))
			this.moreGameBtn = moreGameBtn
			moreGameBtn.backgroundImage.texture = getRes('moreGameBtn')
			moreGameBtn.clickBlock = () => {
				let v = new MoreGameView(new egret.Rectangle(0, 0, this.stage.stageWidth, this.stage.stageHeight + 500))
				// let v = new MoreGameView(new egret.Rectangle(0, 0, 500, 500))
				this.addChild(v)
				this.removeChild(this['collectTip'])
				this.addChild(this['collectTip'])
			}

			this.addChild(moreGameBtn)

      let adHeight = DataBus.share().adMaxHeight
				v = new MoreGameView_Bottom(new egret.Rectangle(this.stage.stageWidth / 2, this.stage.stageHeight - adHeight - 35, 570, 90))
				v.anchorOffsetX = 570 / 2
				v.anchorOffsetY = 90
				this.addChild(v)
				this.addRedpakage()
	}
	groupRanksBtn: any
	redPakageCount = new egret.TextField()

	hiddenCollectTip_1() {
		this.removeChild(this['collectTip'])
	}

	showCollectTip_1() {
		this.removeChild(this['collectTip'])
		this['collectTip'].visible = true
		this.addChild(this['collectTip'])
	}

	addRedpakage() {
		let redPakageBtn = new customButtom(new egret.Rectangle(0, 0, 100, 141))
		redPakageBtn.anchorOffsetX = redPakageBtn.width
		redPakageBtn.anchorOffsetY = redPakageBtn.height
		redPakageBtn.x = this.stage.stageWidth - 20
		redPakageBtn.y = this.groupRanksBtn.y - 50
		// redPakageBtn.y =  20
		redPakageBtn.backgroundImage.texture = getRes('red_pakage')
		redPakageBtn.clickBlock = () => {
			let v = new PhoneExchangeView(new egret.Rectangle(0, 0, this.stage.stageWidth, this.stage.stageHeight))
			this.addChild(v)
			this.showCollectTip_1()
		}

		DataBus.share().registerObserver(this, 'redPakage')
		this.redPakageCount.text = '￥' + DataBus.share().redPakage
		this.redPakageCount.textColor = 0x666666
		this.redPakageCount.anchorOffsetX = this.redPakageCount.width / 2
		this.redPakageCount.anchorOffsetY = this.redPakageCount.height
		this.redPakageCount.x = redPakageBtn.x - redPakageBtn.width / 2
		this.redPakageCount.y = redPakageBtn.y - 1
		if (DataBus.share().preProduct_get == false) {
			this.addChild(redPakageBtn)
			this.addChild(this.redPakageCount)
		}
	}

	moreGameBtn: any
	bannerAd: any
	//开始游戏
	private Start() {
		var desk: PlayScene = new PlayScene();
		this.parent.addChildAt(desk, 0);

		this.removeFromParent()
	}

	removeFromParent() {
		if (this.bannerAd) {
			this.bannerAd.hide()
			this.bannerAd.destroy()
		}
		DataBus.share().removeObserver(this, 'voiceOpen')
		this.parent.removeChild(this);
	}
	private isLogin = false; //是否第一次初始化
	private progress: egret.TextField; //初始化进度条
	//加载进度条
	private SceneProgress(y: number) {
		this.progress = new egret.TextField();
		this.progress.text = '开始加载';
		this.progress.y = y;
		this.progress.width = this.stage.stageWidth;
		this.progress.textAlign = egret.HorizontalAlign.CENTER;
		this.addChild(this.progress);
	}
	//进度条加载资源
	public onProgress(current: number, total: number): void {
		this.progress.text = `资源加载中...${current}/${total}`;
	}
	//进度条读取用户数据
	public afterProgress(text: string): void {
		if (!text) {
			this.removeChild(this.progress);
			this.SceneTitanBar();
		} else {
			this.progress.text = text;
		}
	}
}
