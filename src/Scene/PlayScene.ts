class PlayScene extends egret.Sprite  implements Observer {
	private desktop: Desktop; //六边形桌面阵列
	private moves: Array<Cards> = []; //六边形移动阵列
	private handleCards: Cards; //抓在手上的移动阵列
	private handleCard: Card; //被碰撞的位置
	private handleHitCards: Array<Card>; //预碰撞的块数组
	private platform: any = window.platform;
	private dailyRewardView: any

	// private bitmap: egret.Bitmap;
	private TopText: egret.TextField = new egret.TextField(); //顶部积分
	private LeftUser: UserScore = new UserScore(); //左侧用户
	private RightUser: UserScore = new UserScore(); //右侧用户
	private CurrentScore: number = 0; //当前分数

	private topSelfScore = new egret.TextField();
	private cycleImage = new egret.Bitmap(getRes('cycle'))
	private gameOverView: LuckyUserView
	private relifeView: ReLifeView
	private hammerView: UserHammerView
	private theCards: Cards
	private bgImg: egret.Bitmap
	private menuView: MenuView
	private gameCalcView: GameCalcView
	private tipView: VideoRelifeTipView
	bannerAdIsHidden: boolean = false
	moreGameBtn: customButtom
	moreGameBtnLabel: egret.TextField



	private instruView: BaseView
	private videoTipFlag = true

	//game tool
	tool_2048: customButtom
	tool_hammer: customButtom
	tool_refresh: customButtom

	tool_2048_cards: Cards
	public constructor() {
		super();
		this.touchEnabled = true;
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createGameScene, this);
		this.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.onMove, this);
		this.addEventListener(egret.TouchEvent.TOUCH_END, this.onEnd, this);

		DataBus.share().registerObserver(this, 'gameOver')
		DataBus.share().registerObserver(this, 'score')
		DataBus.share().registerObserver(this, 'isUseHammer')
		DataBus.share().registerObserver(this, 'highestScore')
		DataBus.share().registerObserver(this, 'voiceOpen')

  let gameToolKeys = [
    'hammerCount',
    'refreshCount',
    'tool2048Count'
  ]

	gameToolKeys.map((key) => {
		DataBus.share().registerObserver(this, key)
	})


	}

	//observer score
	public	subscribe(before, now, key, instance) {

		switch (key) {
			case 'score':
			console.log(now)
			this.topSelfScore.text = (now as number).toString()
			break
			case 'gameOver':
			// if ((now as boolean)) {
			// 	this.bannerAd.hide()
			// 	this.bannerAdIsHidden = true
			// }
			if (!this.gameOverView || !this.relifeView) {
				return
			}
			if (DataBus.share().currentLife < DataBus.share().freeLife) {
				this.gameOverView.visible = (now as boolean)
			} else if (DataBus.share().currentLife < DataBus.share().amountLife){
				this.gameOverView.visible = false
				if (this.videoTipFlag == true) {
					if ((now as boolean) == true) {
						DataBus.share().showOpenDataView(false)
						window.platform.wx['aldSendEvent']("进入提示页面")
						this.bannerAdIsHidden = true
						this.tipView.visible = true
						this.videoTipFlag = false
						this.showCollectTip()
						this.removeChild(this.moreGameBtn)
						this.addChild(this.moreGameBtn)
					} else {
					}
				} else {
					if (now) {
						DataBus.share().showOpenDataView(false)
						window.platform.wx['aldSendEvent']("进入死亡页面")
						this.showCollectTip()
						this.removeChild(this.moreGameBtn)
						this.addChild(this.moreGameBtn)
					} else {
					}

					this.relifeView.visible = (now as boolean)
				}
			} else {

				this.relifeView.visible = false

				if (now) {
						DataBus.share().showOpenDataView(false)
					window.platform.wx['aldSendEvent']("进入结算页面")
					DataBus.share().showPreAfterView()
					this.bannerAdIsHidden = true
					this.showCollectTip()
					this.removeChild(this.moreGameBtn)
					this.addChild(this.moreGameBtn)
				} else {
				}

				this.gameCalcView.visible = (now as boolean)
				this.gameCalcView['hiddenTip'] = () => {
					this.hiddenCollectTip()
				}
			}
			this.relifeView.update()
			this.gameCalcView.update()
			break
			case 'isUseHammer':
			this.hammerView.visible = (now as boolean)
			break
			case 'hammerCount':
			this.tool_hammer.textField.text = 'x' + now
			break
			case 'refreshCount':
			this.tool_refresh.textField.text = 'x' + now
			break
			case 'tool2048Count':
			this.tool_2048.textField.text = 'x' + now
			break
			case 'highestScore':
			this.TopText.text = '最高分数：' + (now as number).toString()
			this.menuView.setScore(now as number)
			break
			case 'voiceOpen':
			this.menuView.voiceBtn.backgroundImage.texture = getRes((now as boolean) ?  'voice_open' : 'voice_close')
			break
			case 'dailyRewardTimeOffset':
			this.timeLabel.text = DataBus.share().dailyRewardTimeString
			this.timeLabel.visible = DataBus.share().canGetDailyReward == false
			break
			default:
			break
		}

	}

	bannerAd: any
	//界面被添加
	private createGameScene() {
		DataBus.share().showChaseView()
		window.platform.wx['aldSendEvent']("进入关卡页面")
		//ad
		this.bannerAd = createAd('adunit-d19ca4006ee43ade', DataBus.share().systemInfo.screenHeight)
		setInterval(() => {
			if (this.stage == null) {
				// clearInterval(id1)
				return
			}

			if (this.bannerAdIsHidden) {
				return
			}

			if (this.bannerAd) {
				this.bannerAd.destroy()
				this.bannerAd = createAd('adunit-d19ca4006ee43ade', DataBus.share().systemInfo.screenHeight)
			}

		}, 1000 * 60 * 1)

		this.SceneBackground()
		this.addMoreGame()
		this.SceneTopBack()
		// this.SetVSBar((128 * this.stage.stageHeight) / 1334);
		this.SceneBoard()
		this.SceneCards();
		this.addTools()

		this.setGameOverView()
		//gammo

		this.setUserHammer()
		this.setupMenu()
		this.timeControl()
		//test
		DataBus.share().addBoxViewBlock = () => {
			this.bannerAd.hide()
			this.bannerAdIsHidden = true
			let v = new GetBox(rectCreate(0, 0, this.stage.stageWidth, this.stage.stageHeight))
			window.platform.wx['aldSendEvent']("进入免费宝箱页面")
			this.addChild(v)
		}

		this.tool_2048_cards.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onStart_2048, this, true);

		this.popDeskState()
		this.setupGameCalcView()
		this.setupTipView()


		if (DataBus.share().isFirstGame) {
			this.bannerAd.hide()
			this.bannerAdIsHidden = true
			//finger
			let finger = new egret.Bitmap(getRes('finger'))
			this.finger = finger
			finger.x = this.cycleImage.x
			finger.y = this.cycleImage.y
			this.addChild(finger)
			egret.Tween.get(finger, {loop: true})
			.wait(500)
			.to({y: this.desktop.y}, 2000)
			.wait(500)


			//
			this.instruView = new InstruView(rectCreate(0, 0, this.stage.stageWidth, this.stage.stageHeight))
			this.addChild(this.instruView)
			DataBus.share().isFirstGame = false
			// DataBus.share().showWillOverView(false)
			DataBus.share().showOpenDataView(false)
		} else {
			DataBus.share().showChaseView()
		}

		window.platform.wx['onHide'](() => {
			console.log('onHide')
			this.saveDeskState()
		})


		let v = createCollectView(this)
		v.visible = false

	}
	finger : any

	timeLabel = new egret.TextField()
	timeControl() {
		DataBus.share().registerObserver(this, 'dailyRewardTimeOffset')
		this.timeLabel.visible = DataBus.share().canGetDailyReward == false
		this.timeLabel.text = '00:00:00'
		this.timeLabel.anchorOffsetX = this.timeLabel.width / 2
		this.timeLabel.x = maxX(this.groupRanksBtn) - this.groupRanksBtn.width / 2
		this.timeLabel.y = maxY(this.groupRanksBtn) + 10
		this.addChild(this.timeLabel)
	}


	groupRanksBtn: any
	addMoreGame() {

		let groupRanksBtn = new customButtom(new egret.Rectangle(this.stage.stageWidth - 20 - 60, 100, 103, 122))
		groupRanksBtn.anchorOffsetX = 122 / 2
		groupRanksBtn.backgroundImage.texture = RES.getRes('daily_reward')
		this.groupRanksBtn = groupRanksBtn

		groupRanksBtn.clickBlock = () => {

			if (DataBus.share().canGetDailyReward == false) {
				return
			}
			window.platform.wx['aldSendEvent']("每日登录奖励弹框")
			window.platform.wx['aldSendEvent']("进入每日登录奖励页面")
			this.bannerAd.hide()
			this.dailyRewardView = new DailyRewardView(new egret.Rectangle(0, 0,this.stage.stageWidth, this.stage.stageHeight))
			this.addChild(this.dailyRewardView)
			this.removeChild(this.moreGameBtn)
			this.addChild(this.moreGameBtn)
			this.showCollectTip()

			DataBus.share().showOpenDataView(false)

			// this.removeChild(this['collectTip'])
			// this.addChild(this['collectTip'])
			// this['collectTip'].visible = true
			this.dailyRewardView.closeBlock = () => {
				DataBus.share().showChaseView()
				this.bannerAd.show()
				this.hiddenCollectTip()
			}

			this.dailyRewardView.ensureBlock = () => {
				DataBus.share().showChaseView()
				this.bannerAd.show()
				window.platform.wx['aldSendEvent']("每日奖励")
			}
		}

		let a_time = 400
		let start_y = groupRanksBtn.y
		let start_scale_x = groupRanksBtn.scaleX
		let start_scale_y = groupRanksBtn.scaleY
		let scale_offset = 0.1

		egret.Tween.get(groupRanksBtn, {loop: true})
		.wait(a_time * 3)
		.to({y: start_y - 20, scaleX: start_scale_x - scale_offset}, a_time)
		.to({y: start_y, scaleX: start_scale_x}, a_time)
		.to({scaleX: start_scale_x + scale_offset}, a_time / 2)
		.to({scaleX: start_scale_x}, a_time / 2)

		// let a_time = 400
		// let start_y = groupRanksBtn.y
		// let start_scale_x = groupRanksBtn.scaleX
		// let start_scale_y = groupRanksBtn.scaleY
		// let scale_offset = 0.1
		// egret.Tween.get(groupRanksBtn, {loop: true})
		// .wait(a_time * 3)
		// .to({y: start_y - 40, scaleX: start_scale_x - 0.2}, a_time)
		// .to({y: start_y, scaleX: start_scale_x}, a_time)
		// .to({scaleX: start_scale_x + 0.2}, a_time / 2)
		// .to({scaleX: start_scale_x}, a_time / 2)

		this.addChild(groupRanksBtn)


		let moreGameBtn = new customButtom(new egret.Rectangle(0, this.stage.stageHeight / 2 + 50, 69, 95))
		moreGameBtn.backgroundImage.texture = getRes('moreGameBtn')
		moreGameBtn.clickBlock = () => {
			let v = new MoreGameView(new egret.Rectangle(0, 0, this.stage.stageWidth, this.stage.stageHeight + 500))
			v['closeBlock'] = () => {
				this.hiddenCollectTip()
			}
			// let v = new MoreGameView(new egret.Rectangle(0, 0, 500, 500))
			this.addChild(v)
			this.showCollectTip()
			// this.removeChild(this['collectTip'])
			// this.addChild(this['collectTip'])
			// this['collectTip'].visible = true
		}

		this.moreGameBtn = moreGameBtn
		this.addChild(moreGameBtn)
	}

	setupTipView() {
		let v = new VideoRelifeTipView(rectCreate(0, 0, this.stage.stageWidth, this.stage.stageHeight))

		v.closeBlock = () => {
			this.returnToHome()
		}
		v.visible = false
		this.tipView = v

		v.relifeBlock = () => {
			this.tipView.visible = false
			this.bannerAdIsHidden = false
			this.hiddenCollectTip()
			this.desktop.freeLife(() => {
				this.refreshCard()
			})
		}

		v.ensureBlock = () => {
			v.visible = false
			this.restart(() => {
				this.returnToHome()
					// v.visible = true
			})
		}

		this.addChild(v)
	}

	setupGameCalcView() {
		let v = new GameCalcView(rectCreate(0, 0, this.stage.stageWidth, this.stage.stageHeight))
		v.visible = false
		this.gameCalcView = v
		v.ensureBlock = () => {
			v.visible = false
			DataBus.share().hiddenPreAfterView()
			this.restart(() => {
				this.returnToHome()
			// DataBus.share().showPreAfterView()
			// 		v.visible = true
			})
		}
		v.closeBlock = () => {
			this.bannerAdIsHidden = false
			DataBus.share().showOpenDataView(false)
			DataBus.share().reset(true)
			this.desktop.relife()
			this.refreshCard()
			this.returnToHome()
		}

		v.rankBoardBtn.clickBlock = () => {
			let vv = new RankView(new egret.Rectangle(0, 0, this.width, this.height))
			vv.clickExBlock = () => {
				DataBus.share().showPreAfterView()
			}
			this.bannerAd.hide()
			this.addChild(vv)
		}
		this.addChild(v)

	}

	hiddenCollectTip() {
		this.removeChild(this['collectTip'])
	}

	showCollectTip() {
		this.removeChild(this['collectTip'])
		this['collectTip'].visible = true
		this.addChild(this['collectTip'])
	}

	restart(failBlock = null) {
		let v = new EnsureRelifeView(rectCreate(0, 0, this.stage.stageWidth, this.stage.stageHeight))

		this.addChild(v)
		this.showCollectTip()
		this.removeChild(this.moreGameBtn)
		this.addChild(this.moreGameBtn)
		v.closeBlock = () => {
			if (failBlock) {
				failBlock()
			}
		}

		v.ensureBlock = () => {
			this.hiddenCollectTip()
			DataBus.share().showChaseView()
			this.removeChild(v)
			DataBus.share().reset(true)
			this.desktop.relife()
			this.refreshCard()
			this.menuView.visible = false
			this.menuView.bannerAd.hide()
			this.bannerAd.show()
			this.gameCalcView.visible = false
			this.tipView.visible = false
		}

	}

	setupMenu() {
		let v = new MenuView(rectCreate(0, 0, this.width, this.height))
		this.menuView = v
		v.visible = false
		v.restartBlock = () => {
			window.platform.wx['aldSendEvent']("暂停页-重新开始")
			this.restart()
		}

		this.addChild(v)

		v.returnBlock = () => {
			window.platform.wx['aldSendEvent']("暂停页-返回主页")
			this.returnToHome()
		}
	}

	returnToHome() {
		if (this.bannerAd) {
			this.bannerAd.hide()
			this.bannerAdIsHidden = true
			this.bannerAd.destroy()
		}
		DataBus.share().showWillOverView(false)
		// DataBus.share().main.removeOpenDataCanvase()
		DataBus.share().showOpenDataView(false)
		let desk = new ReadyScene();
		this.saveDeskState()
		// this.parent.addChild(desk);
		this.parent.addChildAt(desk, 0);
		createCollectView(desk)
		DataBus.share().removeObserver(this, null)
		this.parent.removeChild(this);
	}

	setUserHammer() {
		let view = new UserHammerView(new egret.Rectangle(0, this.height - 500, this.width, 500))
		view.visible = false
		this.hammerView = view
		console.log(this.hammerView)
		this.addChild(view)
	}

	//初始化背景
	private setGameOverView() {
		this.gameOverView = new LuckyUserView(new egret.Rectangle(0, 0, this.stage.stageWidth, this.stage.stageHeight))
		this.gameOverView.closeBlock = () => {
			this.returnToHome()
		}
		this.gameOverView.ensureBlock = () => {
			this.desktop.freeLife(() => {
				this.refreshCard()
			})
		}
		this.gameOverView.visible = false
		this.addChild(this.gameOverView)


		this.relifeView = new ReLifeView(new egret.Rectangle(0, 0,this.stage.stageWidth, this.stage.stageHeight))
		this.relifeView.ensureBlock = () => {
			if (DataBus.share().isPreProduct() == false) {
				window.platform.createVideoAd('adunit-7ba317a81824b938', () => {
					this.relifeView.visible = false
					this.hiddenCollectTip()
					DataBus.share().showChaseView()
					this.desktop.freeLife(() => {
						this.refreshCard()
					})
				}, () => {
						window.platform.wx['showModal']({
							title: '复活失败',
							content: '复活失败',
							showCancel: false
						})

				}, DataBus.share())

				// DataBus.share().shareTime = Date.now()
				// window.platform.shareAppMessage({})
				// DataBus.share().onShowBlocks.push(() => {
				// 	let now = Date.now()
				// 	let timeOffset = now - DataBus.share().shareTime
				// 	if (timeOffset < 4 * 1000) { //fail
				// 		window.platform.wx['showModal']({
				// 			title: '分享失败',
				// 			content: '需要分享到不同的群',
				// 			showCancel: false
				// 		})
				// 		return
				// 	}
				// 	this.desktop.freeLife(() => {
				// 		this.refreshCard()
				// 	})
				// })
			} else {
				this.desktop.freeLife(() => {
					this.refreshCard()
				})
			}
		}

		this.relifeView.closeBlock = () => {
			this.returnToHome()
		}
		this.relifeView.visible = false
		// this.relifeView.visible = true
		this.addChild(this.relifeView)

	}

	private addTools() {

		let tool_2048 = this.generatorTools(170, null)
		tool_2048.x = 40
		tool_2048.y = this.cycleImage.y
		tool_2048.anchorOffsetY = tool_2048.height / 2
		tool_2048.clickBlock = () => {
			console.log('click tool 2048')
		}
		this.tool_2048 = tool_2048
		tool_2048.textField.text = 'x' + DataBus.share().gameToolData['getTool2048Count']()
		let card_2048 = new Cards(rectCenter(tool_2048).x, tool_2048.y, this.desktop,true)
		this.tool_2048_cards = card_2048
		this.addChild(tool_2048)
		this.addChild(card_2048)
		// tool_2048.anchorOffsetY = tool_2048.textField.height

		let tool_refresh = this.generatorTools(113, 'tool_refresh')
		tool_refresh.x = this.stage.stageWidth - 10
		tool_refresh.y = this.cycleImage.y
		tool_refresh.anchorOffsetY = tool_refresh.height / 2
		tool_refresh.anchorOffsetX = tool_refresh.width
		tool_refresh.textField.text = 'x' + DataBus.share().gameToolData['getRefreshCount']()
		tool_refresh.clickBlock = () => {
			let gameTool = DataBus.share().gameToolData
			if(gameTool['getRefreshCount']() <= 0) {
				return
			}
			window.platform.wx['aldSendEvent']("关卡页-更换")
			DataBus.share().playSoundWith('refresh')
			this.refreshCard()
			gameTool['setRefreshCount'](gameTool['getRefreshCount']() - 1)
		}

		this.tool_refresh = tool_refresh
		this.addChild(tool_refresh)

		let hammer = this.generatorTools(113, 'hammer')
		hammer.x = this.stage.stageWidth - 20 - tool_refresh.width
		hammer.y = this.cycleImage.y
		hammer.anchorOffsetY = hammer.height / 2
		hammer.anchorOffsetX = hammer.width
		hammer.textField.text = 'x' + DataBus.share().gameToolData['getHammerCount']()
		hammer.clickBlock = () => {
			let gameTool = DataBus.share().gameToolData
			if (gameTool['getHammerCount']() <= 0 ){
				return
			}
			window.platform.wx['aldSendEvent']("关卡页-锤子")
			DataBus.share().setIsUseHammer(true)
		}
		this.tool_hammer = hammer
		this.addChild(hammer)
	}

	private generatorTools(width:number, contentImage: string) {
		let toolsBtn = new customButtom(rectCreate(0, 0 , width, width))
		// let toolsBtn = new customButtom()
		toolsBtn.backgroundImage.texture = getRes('air_bubbles')
		if (contentImage != null) {

		let img = new egret.Bitmap(getRes(contentImage))
		img.anchorOffsetX = img.width / 2
		img.anchorOffsetY = img.height / 2
		img.x = toolsBtn.width / 2
		img.y = toolsBtn.height / 2
		toolsBtn.addChild(img)
		}

		toolsBtn.textField.text = "x10"
		toolsBtn.textField.size = 30
		toolsBtn.textField.textColor = 0xffffff
		toolsBtn.textField.y += width / 2 - 10
		return toolsBtn
	}

	private SceneBackground() {
		var img: egret.Bitmap = new egret.Bitmap(getRes('game_bg'));
		img.width *= this.stage.stageHeight / img.height;
		img.x = (this.stage.stageWidth - img.width) / 2;
		img.height = this.stage.stageHeight;
		this.bgImg = img
		this.bgImg.touchEnabled = true
		this.bgImg.addEventListener(egret.TouchEvent.TOUCH_TAP, this.viewTap, this);
		this.addChild(img);
	}
	//初始化对战条
	private SceneTopBack(y: number = 10) {
		let back = new customButtom(rectCreate(30, 30, 90, 90))
		back.backgroundImage.texture = getRes('pause_1')
		back.clickBlock = () => {
			// this.bannerAd.hide()

      window.platform.wx['aldSendEvent']("关卡页-暂停")
      window.platform.wx['aldSendEvent']("进入暂停页面")
			this.menuView.visible = true
			this.removeChild(this.moreGameBtn)
			this.addChild(this.moreGameBtn)
			this.menuView.bannerAd.show()
			this.bannerAd.hide()
		}
		this.addChild(back);


		if (DataBus.share().systemInfo.model.indexOf('iPhone X') != -1) {
			this.SetTopText( rectCenter(back).y);
		} else {
			this.SetTopText( back.y + 5);
		}
	}
	//顶部文字
	private SetTopText(y: number) {
		this.TopText.y = y;
		this.TopText.size = 30;
		this.TopText.width = this.stage.stageWidth;
		// this.TopText.height = 50;
		this.TopText.textAlign = egret.HorizontalAlign.CENTER;
		this.TopText.verticalAlign = egret.VerticalAlign.MIDDLE;
		DataBus.share().getHighestScore()
		.then(x => this.TopText.text = '最高分数：' + x)

		this.TopText.anchorOffsetY = this.TopText.height/ 2
		this.addChild(this.TopText);

		this.topSelfScore.width = 400
		this.topSelfScore.size = 70
		this.topSelfScore.x = rectCenter(this.TopText).x
		this.topSelfScore.y = maxY(this.TopText) + 55
		this.topSelfScore.textAlign = 'center'
		this.topSelfScore.verticalAlign = 'middle'
		this.topSelfScore.text = '0'
		this.topSelfScore.bold = true
		this.topSelfScore.fontFamily = 'HYe0gj'
		this.topSelfScore.anchorOffsetX = this.topSelfScore.width / 2
		this.addChild(this.topSelfScore)

	}
	//初始化棋盘
	private SceneBoard() {
		this.desktop = new Desktop(this.stage.stageWidth / 2, maxY(this.topSelfScore) + 20);
		this.desktop.anchorOffsetX = this.desktop.width / 2
		this.desktop.x = this.stage.stageWidth / 2
		this.addChild(this.desktop);
	}
	//初始化棋子
	private SceneCards() {

		let yOffset = this.desktop.y + this.desktop.height / 2 + 120

		this.cycleImage.x = this.stage.stageWidth / 2
		this.cycleImage.y = yOffset
		this.cycleImage.anchorOffsetX = this.cycleImage.width / 2
		this.cycleImage.anchorOffsetY = this.cycleImage.height / 2
		this.cycleImage.touchEnabled = true

		egret.Tween.get(this.cycleImage, {loop: true})
		.to({rotation: 360}, 7000)
		this.addChild(this.cycleImage)

		for (let i = 0; i < 1; i++) {
			let cards = new Cards(this.stage.stageWidth / 2, yOffset, this.desktop)
			this.theCards = cards
			this.moves[i] = cards
			this.addChild(this.moves[i]);
			this.moves[i].addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onStart, this, true);
			this.moves[i].addEventListener(egret.TouchEvent.TOUCH_TAP, this.moves[i].rotatCard, this.moves[i]);
		}


		this.cycleImage.addEventListener(egret.TouchEvent.TOUCH_TAP,this.theCards.rotatCard, this.theCards)
		this.cycleImage.addEventListener(egret.TouchEvent.TOUCH_BEGIN,this.onStart, this, true)


	}

	private viewTap() {
		console.log('viewTap')
		if (DataBus.share().getIsUseHammer()) {
			DataBus.share().setIsUseHammer(false)
		}
	}


	//开始触摸
	private offset: egret.Point; //触摸偏移量
	private shouldMoved = false
	private onStart(evt: egret.TouchEvent) {
		if (DataBus.share().isMergeCard) {
			return
		}

		this.handleCard = null;
		this.handleCards = this.theCards
		this.offset = new egret.Point(evt.stageX, evt.stageY);
		this.handleCards.Scale(1);
	}

	private onStart_2048(evt: egret.TouchEvent) {
		if (DataBus.share().isMergeCard || DataBus.share().gameToolData['getTool2048Count']() <= 0) {
			return
		}

		this.handleCard = null;
		this.handleCards = this.tool_2048_cards
		this.offset = new egret.Point(evt.stageX, evt.stageY);
		// this.handleCards.Move(evt.stageX, evt.stageY - 100);
		this.handleCards.Scale(1);
	}

	//移动棋子(判断碰撞)
	private onMove(evt: egret.TouchEvent) {
		this.videoTipFlag = false
		if (!this.handleCards) return;

		let amountOffset = (Math.abs(evt.stageX - this.offset.x) + Math.abs(evt.stageY - this.offset.y))
		if ( amountOffset < 100 && !this.shouldMoved) {
			return
		}
		this.handleCards.hadMoved = true

		this.shouldMoved = true

		if (this.handleHitCards) this.setHandleHitCards(false);
		this.handleCards.Move(evt.stageX, evt.stageY );
		this.handleCard = this.desktop.checkHit(this.handleCards);
		if (this.handleCard) this.setHandleHitCards(true);
	}
	//放下棋子（判断完全碰撞并嵌入）
	private onEnd(evt: egret.TouchEvent) {
		this.shouldMoved = false
		if (!this.handleCards) return;
		if (this.handleCards == this.tool_2048_cards) {
			window.platform.wx['aldSendEvent']("关卡页-2048")
		}
		if (this.handleHitCards) this.ComputeScore();
		this.handleCards.Move();
		setTimeout(() => {
			this.theCards.hadMoved = false
		}, 100)
		this.handleCard = this.handleCards = this.handleHitCards = null;
		// this.theCards.hadMoved = false
	}
	//设置/清除预染色
	private setHandleHitCards(Hit: boolean = false) {
		if (Hit) {
			this.handleHitCards = this.desktop.checkAllHit(this.handleCards, this.handleCard);
			if (this.handleHitCards) this.handleHitCards.forEach(c => c.ChangeStatus(3, this.handleCards.color));
		} else {
			this.handleHitCards.forEach(c => c.ChangeStatus(0));
			this.handleHitCards = null;
		}
	}

	isUseTool2048 = false

	//完成碰撞-嵌入-计分-判败
	private ComputeScore() {
		if (this.finger) {
			this.removeChild(this.finger)
		}
		DataBus.share().playSoundWith('02')
	this.isUseTool2048 = this.handleCards == this.tool_2048_cards
	if (this.isUseTool2048) {
		let gameTool = DataBus.share().gameToolData
		gameTool['setTool2048Count'](gameTool['getTool2048Count']() - 1)
	}

		this.handleHitCards.forEach(c => c.ChangeStatus(4));
		DataBus.share().isMergeCard = true
		this.moves[0].visible = false || this.isUseTool2048
		 this.desktop.ClearBlock()
		 .then((clear) =>  {
			 DataBus.share().isMergeCard = false
			 if (this.isUseTool2048 == false) {
				 this.desktop.calcMinMaxValue()
				 this.refreshCard()
				 this.moves[0].visible = true
				 console.log(6666666666, clear.combo)
				 this.getRedPakage(clear.combo)

			 }
			 if (clear.score > 0) this.DefaultScore(clear);
		 })

	}

	get lastRedPackageTime() {
		return window.platform.Storage.get('lastRedPackageTime')
	}

	set lastRedPackageTime(value) {
			window.platform.Storage.set('lastRedPackageTime', Date.now())
	}

  get redRecord() {
    return window.platform.Storage.get('redRecord')
  }

  set redRecord(value) {
    window.platform.Storage.set('redRecord', value)
  }

  addRedRecord() {
    let date = new Date()
    let mon = date.getMonth()
    let day = date.getDay()
    let dateKey = mon + '-' + day
    let value = this.getValidRedRecord()
    this.redRecord = {
      date: dateKey,
      count: (value + 1)
    }
  }

  getValidRedRecord() {
    let date = new Date()
    let mon = date.getMonth()
    let day = date.getDay()
    let value = this.redRecord
    let dateKey = mon + '-' + day

    if (value == null) {
      return 0
    }

    if (value.date == dateKey) {
      return value.count
    } else {
        this.redRecord = {
          date: dateKey,
          count: 0
        }
    }
  }
	getRedPakage(combo) {
		if (DataBus.share().preProduct_get == true) {
				return
		}

		if (this.getValidRedRecord() > 20) {
			return
		}

		if (combo < 3) {
			return
		}

		if (this.lastRedPackageTime != null && (Date.now() - this.lastRedPackageTime < 2000)) {
			return
		}

		if (Math.random() < 0.5) {
				return
		}

		this.lastRedPackageTime = Date.now()
		let v = new GetRedPackageView(new egret.Rectangle(0, 0, this.stage.stageWidth, this.stage.stageHeight))
		DataBus.share().showOpenDataView(false)
		this.addChild(v)
		this.showCollectTip()
		this.removeChild(this.moreGameBtn)
		this.addChild(this.moreGameBtn)

	}

	refreshCard() {
		this.desktop.calcMinMaxValue()
		this.theCards.createGameSprite();
		while (this.desktop.checkValidCards(this.theCards) == false && DataBus.share().getGameOver() == false) {
			this.theCards.createGameSprite();
		}
	}

	//普通记分
	private DefaultScore(clear: number) {
		// DataBus.share().addScore(clear)
		// this.CurrentScore += clear;
		// if (this.CurrentScore > this.platform.MySelf.score) this.SuperScore();
		// this.SetLeft();

	}

	saveDeskState() {
		console.log('saving')
		this.desktop.saveDeskState()
		this.theCards.saveState()
		window.platform.setStorage({key: 'gameRecord', data: '1'})
		DataBus.share().saveState()
		DataBus.share().saveCurrentScore()
	}

	async popDeskState() {
		let gameRecord = await window.platform.getStorage('gameRecord')
		gameRecord = gameRecord.data
		if (gameRecord == null || gameRecord != '1') {
			return
		}
		console.log('recored: ' + gameRecord)

		await this.desktop.popDeskState()
		await this.theCards.popState()
		await DataBus.share().popScore()
		await DataBus.share().popState()
		let isGameOver = this.desktop.isGameOver()
	}
}
