
class VideoRelifeTipView extends BaseView {
  relifeImage = new egret.Bitmap(getRes('tip_bg'))
  tipLabel = new egret.TextField()
  relifeBtn = new customButtom(new egret.Rectangle(this.width / 2, 0, 406, 136))
  videoRelifeBtn = new customButtom(new egret.Rectangle(this.width / 2, 0, 408, 120))
  ensureBlock: () => void
  closeBtn = new customButtom(new egret.Rectangle( 0, 0, 60, 60))
  closeBlock: () => void
  relifeBlock: () => void
  rewardedVideoAd: any


  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  setupUI() {
  this.rewardedVideoAd = window.platform.wx['createRewardedVideoAd']({ adUnitId: 'adunit-fd4411e3a02e08cc' })

    setCenterAnchor(this.relifeImage)
    this.relifeImage.x = rectCenter(this).x
    this.relifeImage.y = rectCenter(this).y - 250
    this.addChild(this.relifeImage)

    this.tipLabel.size = 35
    this.tipLabel.text = '已有游戏存档,\n是否重新开始游戏'
    this.tipLabel.textAlign = 'center'
    setCenterAnchor(this.tipLabel)
    this.tipLabel.x = this.width / 2
    this.tipLabel.y = this.relifeImage.y
    // this.addChild(this.tipLabel)

    this.update()

    this.relifeBtn.anchorOffsetX = this.relifeBtn.width / 2
    this.relifeBtn.x = this.stage.stageWidth / 2
    this.relifeBtn.y = maxY(this.relifeImage) + 60
    this.relifeBtn.backgroundImage.texture = getRes('game_restart')

    this.relifeBtn.clickBlock = () => {
      window.platform.wx['aldSendEvent']("提示页重新开始")
      if (this.ensureBlock) {
        this.ensureBlock()
      }
    }

    this.addChild(this.relifeBtn)

    this.videoRelifeBtn.anchorOffsetX = this.videoRelifeBtn.width / 2
    this.videoRelifeBtn.x = this.stage.stageWidth / 2
    this.videoRelifeBtn.y = maxY(this.relifeBtn) + 50
    this.videoRelifeBtn.backgroundImage.texture = getRes('relife_video')

    this.videoRelifeBtn.clickBlock = () => {
      this.rewardedVideoAd.onClose(res => {
        // 用户点击了【关闭广告】按钮
        // 小于 2.1.0 的基础库版本，res 是一个 undefined
        if (res && res.isEnded || res === undefined) {
          // 正常播放结束，可以下发游戏奖励
          if (this.relifeBlock) {
            window.platform.wx['aldSendEvent']("提示页视频复活")
            this.relifeBlock()
          }

        }
        else {
          // 播放中途退出，不下发游戏奖励
        }
      })

      this.rewardedVideoAd.show().catch((err) => {
        console.log(err)
        this.rewardedVideoAd.load()
        .then(() => this.rewardedVideoAd.show())
      })
      .catch(err => {
        window.platform.shareAppMessage({})
      })

      this.rewardedVideoAd.onError((err) => {
        DataBus.share().shareTime = Date.now()
        DataBus.share().onShowBlocks.push(() => {
          let now = Date.now()
          let timeOffset = now - DataBus.share().shareTime
          if (timeOffset < 4 * 1000 && DataBus.share().isPreProduct() == false) { //fail
            window.platform.wx['showModal']({
              title: '分享失败',
              content: '需要分享到不同的群',
              showCancel: false
            })
            return
          }

          if (this.relifeBlock) {
            window.platform.wx['aldSendEvent']("提示页视频复活")
            this.relifeBlock()
          }
        window.platform.shareAppMessage({})
        console.log(err)
      })
      })
    }

    this.addChild(this.videoRelifeBtn)

    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.anchorOffsetY = this.closeBtn.height
    this.closeBtn.x = this.stage.stageWidth / 2
    this.closeBtn.y = this.stage.stageHeight - DataBus.share().adMaxHeight
    this.closeBtn.backgroundImage.texture = getRes('reward_close')
    this.closeBtn.clickBlock = () => {
            window.platform.wx['aldSendEvent']("提示页关闭")
        if (this.closeBlock) {
          this.closeBlock()
        }
    }

    this.addChild(this.closeBtn)
  }

  update() {
    // this.relifeBtn.backgroundImage.texture = getRes( DataBus.share().currentLife < 4 ? 'relife_btn_enable_1': 'relife_btn_1')
  }
}
