
class ReLifeView extends BaseView {
  relifeImage = new egret.Bitmap(getRes('relife_bg_1'))
  scoreLabel = new egret.TextField()
  tipLabel = new egret.TextField()
  cureentLife = new egret.TextField()
  relifeBtn = new customButtom(new egret.Rectangle(this.width / 2, 0, 408, 120))
  ensureBlock: () => void
  closeBtn = new customButtom(new egret.Rectangle( 0, 0, 60, 60))

  closeBlock: () => void

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  setupUI() {

    this.scoreLabel.text = '最高得分：' + DataBus.share().getScore().toString()
    this.scoreLabel.size = 43

    this.tipLabel.text = '每局可复活两次'
    this.tipLabel.size = 30
    this.tipLabel.textColor = 0xffdf01

    this.cureentLife.text = '+' + (DataBus.share().currentLife + 1)
    this.cureentLife.size = 50

    setCenterAnchor(this.relifeImage)
    this.relifeImage.x = rectCenter(this).x
    this.relifeImage.y = rectCenter(this).y - 130
    this.addChild(this.relifeImage)

    setCenterAnchor(this.cureentLife)
    this.cureentLife.x = this.relifeImage.x  - 155
    this.cureentLife.y = this.relifeImage.y + 5
    this.cureentLife.rotation = -25
    this.addChild(this.cureentLife)

    setCenterAnchor(this.scoreLabel)
    this.scoreLabel.x = rectCenter(this).x
    this.scoreLabel.y = this.relifeImage.y - 250
    this.addChild(this.scoreLabel)

    setCenterAnchor(this.tipLabel)
    this.tipLabel.x = rectCenter(this).x
    this.tipLabel.y = maxY(this.relifeImage)
    this.addChild(this.tipLabel)

    this.update()
    this.relifeBtn.anchorOffsetX = this.relifeBtn.width / 2
    this.relifeBtn.y = maxY(this.relifeImage) + 50

    this.relifeBtn.clickBlock = () => {
      if (DataBus.share().currentLife < 4) {

        window.platform.wx['aldSendEvent']("复活")
        if (this.ensureBlock) {
          this.parent['bannerAd'].show()
          this.parent['bannerAdIsHidden'] = false
          this.ensureBlock()
        }
      } else {

      }
    }

    this.addChild(this.relifeBtn)

    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.anchorOffsetY = this.closeBtn.height
    this.closeBtn.x = this.stage.stageWidth / 2
    this.closeBtn.y = this.stage.stageHeight - DataBus.share().adMaxHeight
    this.closeBtn.backgroundImage.texture = getRes('reward_close')
    this.closeBtn.clickBlock = () => {
        window.platform.wx['aldSendEvent']("死亡页面关闭")
        this.visible = false
        if (this.closeBlock) {
          this.closeBlock()
        }
    }

    this.addChild(this.closeBtn)
  }

  update() {
    this.relifeBtn.backgroundImage.texture = getRes('relife_video')
    this.scoreLabel.text = '最高得分：' + DataBus.share().getScore().toString()
    setCenterAnchor(this.scoreLabel)
    this.cureentLife.text = '+' + (DataBus.share().currentLife + 1)
  }
}
