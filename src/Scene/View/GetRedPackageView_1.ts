
class GetRedPackageView_1 extends BaseView {

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  bgImageView: egret.Bitmap
  closeBtn: customButtom

  change_1: customButtom
  // change_2: customButtom
  cost = new egret.TextField()
  this_cost = new egret.TextField()
  change_type = 1


  setupUI() {
    this.bgImageView = new egret.Bitmap(getRes('red_pakage_bg'))
    this.bgImageView.anchorOffsetY = this.bgImageView.height / 2
    this.bgImageView.anchorOffsetX = this.bgImageView.width / 2
    this.bgImageView.y = this.stage.stageHeight / 2 - 80
    this.bgImageView.x = this.stage.stageWidth / 2
    this.addChild(this.bgImageView)

    this.closeBtn = new customButtom(new egret.Rectangle(0, 0, 60, 60))
    this.closeBtn.backgroundImage.texture = getRes('reward_close')
    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.anchorOffsetY = this.closeBtn.height

    this.closeBtn.y = this.stage.stageHeight - DataBus.share().adMaxHeight
    this.closeBtn.x = this.stage.stageWidth / 2
    this.closeBtn.clickBlock = () => {
      this.parent['hiddenCollectTip']()
      DataBus.share().showChaseView()
      this.parent.removeChild(this)
    }
    this.addChild(this.closeBtn)

    this.change_1 = new customButtom(new egret.Rectangle(0, 0, 295, 83))
    this.change_1.backgroundImage.texture = getRes('change')
    this.change_1.anchorOffsetX = this.change_1.width / 2
    this.change_1.anchorOffsetY = this.change_1.height
    this.change_1.y = maxY(this.bgImageView) - 110
    this.change_1.x = this.width / 2

    this.change_1.clickBlock = () => {
      DataBus.share().showChaseView()
      let v = new PhoneExchangeView(new egret.Rectangle(0, 0, this.width, this.height))
      this.parent.addChild(v)
      this.parent.removeChild(this)
    }

    this.addChild(this.change_1)

    // this.cost.type = egret.TextFieldType.INPUT;
    // this.cost.inputType = egret.TextFieldInputType.TEL;
    this.cost.size = 35
    // this.cost.width = 500
    // this.cost.height = 50
    this.cost.textColor = 0xdc2626
    this.cost.text = '话费总额：3.35元'
    this.cost.y = this.bgImageView.y - this.bgImageView.height / 2 + 150
    this.cost.x = this.width / 2
    this.cost.anchorOffsetX = this.cost.width / 2
    this.addChild(this.cost)

    this.this_cost.textColor = 0xdc2626
    this.this_cost.text = '1.25元'
    this.this_cost.y = this.bgImageView.y
    this.this_cost.x = this.width / 2
    this.this_cost.anchorOffsetX = this.this_cost.width / 2
    this.addChild(this.this_cost)

    let redPackageCount = this.calcGetRedCount()
    DataBus.share().redPakage += redPackageCount
    this.cost.text = '话费总额：' + DataBus.share().redPakage + '元'
    this.this_cost.text = redPackageCount + '元'
    this.cost.anchorOffsetX = this.cost.width / 2
    this.cost.x = this.width / 2

    this.this_cost.anchorOffsetX = this.this_cost.width / 2
    this.this_cost.x = this.width / 2

    window.platform.EndPoint.modifyUserInfo('hongbao', redPackageCount + '', DataBus.share().userInfo)
    .then((data) => {
      console.log(data, 55555555555)
    })

  }

  get redRecord() {
    return window.platform.Storage.get('redRecord')
  }

  set redRecord(value) {
    window.platform.Storage.set('redRecord', value)
  }

  addRedRecord() {
    let date = new Date()
    let mon = date.getMonth()
    let day = date.getDay()
    let dateKey = mon + '-' + day
    let value = this.getValidRedRecord()
    this.redRecord = {
      date: dateKey,
      count: (value + 1)
    }
  }

  getValidRedRecord() {
    let date = new Date()
    let mon = date.getMonth()
    let day = date.getDay()
    let value = this.redRecord
    let dateKey = mon + '-' + day

    if (value == null) {
      return 0
    }

    if (value.date == dateKey) {
      return value.count
    } else {
        this.redRecord = {
          date: dateKey,
          count: 0
        }
    }
  }

  calcGetRedCount() {
    let result = 0
      let nowCount = DataBus.share().redPakage
      if (nowCount == 0) { //first
          result = Math.random() + 1
          result = Math.floor(result * 100) / 100
          return result
      }

      //reset get redpacakge count daily
      let getRedRecord = this.getValidRedRecord()
      console.log('getRedREcord', getRedRecord)
      let random = Math.random()
      if (nowCount > 15) {
        result = random * 0.04 + 0.01
      } else if (getRedRecord < 10) {
        result = random * 0.4 + 0.1
      } else if (getRedRecord < 20) {
        result = random * 0.1 + 0.02
      }
      this.addRedRecord()
      result = Math.floor(result * 100) / 100
      return result
  }
}
