
class DailyRewardView extends BaseView {
  relifeImage = new egret.Bitmap(getRes('daily_reward_bg'))
  // box = new egret.Bitmap(getRes('daily_box'))

  box = new egret.Bitmap(getRes('box_close'))
  bg_light = new egret.Bitmap(getRes('bg_light_1'))

  scoreLabel = new egret.TextField()
  relifeBtn = new customButtom(new egret.Rectangle(this.width / 2, 0, 345, 120))
  ensureBlock: () => void
  closeBtn = new customButtom(new egret.Rectangle( 0, 0, 60, 60))

  closeBlock: () => void

  rewardedVideoAd = window.platform.wx['createRewardedVideoAd']({ adUnitId: 'adunit-e0507c56e717d49a' })
  // rewardedVideoAd = window.platform.wx['createRewardedVideoAd']({ adUnitId: 'adunit-e0507c56e717' })
  finished = false

  bannerAd: any

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.7
  }

  setupUI() {

		this.bannerAd = createAd('adunit-ace9a30b32b22559', DataBus.share().systemInfo.screenHeight)

    this.scoreLabel.text = '恭喜你，获得道具宝箱！'
    this.scoreLabel.size = 30
    setCenterAnchor(this.relifeImage)
    this.relifeImage.x = rectCenter(this).x
    this.relifeImage.y = rectCenter(this).y - 400
    this.addChild(this.relifeImage)


    setCenterAnchor(this.box)
    this.box.x = rectCenter(this).x
    this.box.y = rectCenter(this).y  - 100

    // setCenterAnchor(this.bg_light)
    this.bg_light.anchorOffsetX = this.bg_light.width / 2 + 11
    this.bg_light.anchorOffsetY = this.bg_light.width / 2 - 7
    this.bg_light.x = this.box.x
    this.bg_light.y = this.box.y
    this.addChild(this.bg_light)
    this.addChild(this.box)


    egret.Tween.get(this.bg_light)
    .to({rotation: 360}, 7000)

    this.relifeBtn.anchorOffsetX = this.relifeBtn.width / 2
    this.relifeBtn.backgroundImage = new egret.Bitmap(getRes('free_get'))
    this.relifeBtn.y = maxY(this.bg_light) + 20

    setCenterAnchor(this.scoreLabel)
    this.scoreLabel.x = rectCenter(this).x
    this.scoreLabel.y = this.relifeBtn.y - 40
    this.addChild(this.scoreLabel)
    this.relifeBtn.clickBlock = () => {

			window.platform.wx['aldSendEvent']("每日登录奖励页免费领取")
      if (this.finished) {
        this.clicked()
        return
      }

      this.rewardedVideoAd.onClose(res => {
        // 用户点击了【关闭广告】按钮
        // 小于 2.1.0 的基础库版本，res 是一个 undefined
        if (res && res.isEnded || res === undefined) {
          // 正常播放结束，可以下发游戏奖励
          this.clicked()
          if (this.ensureBlock) {
            this.ensureBlock()
          }
        } else {
          // 播放中途退出，不下发游戏奖励
        }
      })

      this.rewardedVideoAd.show().catch(() => {
        this.rewardedVideoAd.load()
        .then(() => this.rewardedVideoAd.show())
      })

      this.rewardedVideoAd.onError(err => {
        DataBus.share().shareTime = Date.now()
        DataBus.share().onShowBlocks.push(() => {
          let now = Date.now()
          let timeOffset = now - DataBus.share().shareTime
          if (timeOffset < 4 * 1000 && DataBus.share().isPreProduct() == false) { //fail
            window.platform.wx['showModal']({
              title: '分享失败',
              content: '需要分享到不同的群',
              showCancel: false
            })
            this.finished = false
            return
          }

          this.clicked()
          if (this.ensureBlock) {
            this.ensureBlock()
          }
        })
        window.platform.shareAppMessage({})

        console.log(err)
      })

    }

    this.addChild(this.relifeBtn)

    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.anchorOffsetY = 60
    this.closeBtn.x = this.stage.stageWidth / 2
    this.closeBtn.y = this.stage.stageHeight - DataBus.share().adMaxHeight - 10
    this.closeBtn.backgroundImage.texture = getRes('reward_close')
    this.closeBtn.clickBlock = () => {
      window.platform.wx['aldSendEvent']("每日登录奖励页关闭")
      this.bannerAd.hide()
      this.bannerAd.destroy()
      if (this.closeBlock) {
        this.closeBlock()
      }
      this.parent.removeChild(this)
    }

    this.addChild(this.closeBtn)
  }

  clicked() {
    if (this.finished) {
      // this.visible = false
      if (this['closeBlock']) {
        this['closeBlock']()
      }
      this.parent.removeChild(this)
      return
    }

    DataBus.share().lastGetDailyRewardTime = Date.now()

    this.finished = true
    this.box.visible = false
    this.relifeBtn.backgroundImage.texture = getRes('free_get_1')
    this.scoreLabel.visible = false
    this.getTool()
    // this.parent.removeChild(this)
  }

  getTool() {
      let result = Math.random()
      let tool = new egret.Bitmap()
        let gameTool = DataBus.share().gameToolData
      if (result < 1) { // 2048
        tool.texture = getRes('2048_tool_1')
        gameTool['addTool2048Count']()
      } else if (result < 0.65) { // hammer
        tool.texture = getRes('hammer')
        gameTool['addHammerCount']()
      } else { // refresh
        tool.texture = getRes('tool_refresh')
        gameTool['addRefreshCount']()
      }

      tool.width = 100
      tool.height = 100

      setCenterAnchor(tool)
      tool.x = this.width / 2
      tool.y = this.height / 2  - 100

      let b = new egret.Bitmap(getRes('air_bubbles'))
      setCenterAnchor(b)
      b.x = tool.x
      b.y = tool.y
      this.addChild(b)
      this.addChild(tool)
    }
}
