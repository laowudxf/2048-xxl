
class BaseView extends egret.Sprite {
  public frame: egret.Rectangle
  public backgroundColor: number = null
  public bgColorAlpha: number = 1
  cornerRadius = 0
  cornerRadiusArr: number[] = []
  closeBlock: () => void

  public constructor(frame: egret.Rectangle = new egret.Rectangle()) {
    super()
    this.setFrame(frame)
    this.touchEnabled = true
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.draw, this);
    this.init()
  }

  init() {}

  beforeDraw() {}
  public setupUI() {}

  public draw() {
    this.beforeDraw()

    if (this.backgroundColor != null) {

      if (this.cornerRadiusArr.length > 0 && this.cornerRadiusArr.length < 4) {
        while (this.cornerRadiusArr.length < 4) {
          this.cornerRadiusArr.push(0)
        }
      }

      this.graphics.beginFill(this.backgroundColor, this.bgColorAlpha)
      if (this.cornerRadius == 0 && this.cornerRadiusArr.length == 0) {
        this.graphics.drawRect(0, 0, this.width, this.height)
      } else if (this.cornerRadius != 0) {
        this.graphics.drawRoundRect(0, 0, this.width, this.height, this.cornerRadius * 2, this.cornerRadius * 2)
      } else {
        let pi = Math.PI
        for (let index in this.cornerRadiusArr) {
          let i = parseInt(index)
          let r = this.cornerRadiusArr[i]
          if (i == 0) {
            this.graphics.drawArc(r, r, r,-pi, -pi / 2)
          } else if (i == 1) {
            this.graphics.lineTo(this.width - r, 0)
            this.graphics.drawArc(this.width - r, r, r, -pi + pi / 2, -pi / 2 + pi / 2)
          } else if (i == 2) {
            this.graphics.lineTo(this.width, this.height - r)
            this.graphics.drawArc(this.width - r, this.height - r, r, -pi + (pi / 2) * 2, -pi / 2 + (pi / 2) * 2)
          } else if (i == 3) {
            this.graphics.lineTo(r, this.height)
            this.graphics.drawArc( r, this.height - r, r, -pi + (pi / 2) * 3, -pi / 2 + (pi / 2) * 3)
          }
        }
        this.graphics.lineTo(0, this.cornerRadiusArr[0])
      }
      // this.graphics.endFill()
    }
    this.setupUI()
  }

  protected setFrame(frame: egret.Rectangle) {
    this.frame = frame
    this.x = frame.x
    this.y = frame.y
    this.width = frame.width
    this.height = frame.height
  }

}

class LuckyUserView extends BaseView {
  luckyUserImage = new egret.Bitmap(getRes('lucky_user'))
  ensureBtn: customButtom
  closeBtn = new customButtom(new egret.Rectangle( 0, 0, 70, 70))
  tipLabel: egret.TextField

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  public setupUI() {
    setCenterAnchor(this.luckyUserImage)
    this.luckyUserImage.x = rectCenter(this).x
    this.luckyUserImage.y = rectCenter(this).y
    this.addChild(this.luckyUserImage)

    this.ensureBtn = new customButtom(rectCreate(0, 0, 332, 94))
    this.ensureBtn.backgroundImage.texture = getRes('ensure')
    this.addChild(this.ensureBtn)
    this.ensureBtn.anchorOffsetX = this.ensureBtn.width / 2
    this.ensureBtn.anchorOffsetY = 94
    this.ensureBtn.x = this.width / 2
    this.ensureBtn.y = maxY(this.luckyUserImage) - 40

    // this.addChild(this.ensureBtn)
    // this.ensureBtn.width = this.width * 0.5
    // this.ensureBtn.height = 80
    // console.log(this.width / 2)
    // this.ensureBtn.anchorOffsetX = this.ensureBtn.width / 2
    // this.ensureBtn.anchorOffsetY = 80
    // this.ensureBtn.x = this.width / 2
    // this.ensureBtn.y = maxY(this.luckyUserImage) - 50
    // this.ensureBtn.backgroundImage.width = this.ensureBtn.width
    // this.ensureBtn.backgroundImage.height = this.ensureBtn.height
    this.ensureBtn.clickBlock = () => {
      console.log('复活')
      if (this.ensureBlock) {
        this.parent['bannerAd'].show()
      this.parent['bannerAdIsHidden'] = false
        this.ensureBlock()
      }
    }

    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.x = maxX(this.luckyUserImage)
    this.closeBtn.y = this.luckyUserImage.y - this.luckyUserImage.height / 2 - this.closeBtn.height / 2
    this.closeBtn.backgroundImage.texture = getRes('close_btn')
    this.closeBtn.clickBlock = () => {
        this.visible = false
        if (this.closeBlock) {
          this.closeBlock()
        }
    }
    this.addChild(this.closeBtn)

    this.tipLabel = new egret.TextField()
    this.tipLabel.text = '小贴士：每局可复活两次'
    this.tipLabel.textColor = 0xfdd901
    this.tipLabel.anchorOffsetX = this.tipLabel.width / 2
    this.tipLabel.x = this.stage.stageWidth / 2
    this.tipLabel.y = maxY(this.luckyUserImage) + 30
    this.addChild(this.tipLabel)
  }

  ensureBlock: () => void
}
