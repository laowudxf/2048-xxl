class EnsureRelifeView extends BaseView {
  luckyUserImage = new egret.Bitmap(getRes('restart_game_tip'))
  ensureBtn: customButtom
  closeBtn = new customButtom(new egret.Rectangle( 0, 0, 60, 60))

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  public setupUI() {
    setCenterAnchor(this.luckyUserImage)
    this.luckyUserImage.x = rectCenter(this).x
    this.luckyUserImage.y = rectCenter(this).y - 150
    this.addChild(this.luckyUserImage)

    this.ensureBtn = new customButtom(rectCreate(0, 0, 406, 136))
    this.ensureBtn.backgroundImage.texture = getRes('intro_ensure')
    this.addChild(this.ensureBtn)
    this.ensureBtn.anchorOffsetX = this.ensureBtn.width / 2
    this.ensureBtn.anchorOffsetY = 0
    this.ensureBtn.x = this.width / 2
    this.ensureBtn.y = maxY(this.luckyUserImage) + 40
    this.ensureBtn.clickBlock = () => {
      console.log('复活')
      if (this.ensureBlock) {
        this.ensureBlock()
      }
    }

    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.anchorOffsetY = this.closeBtn.height
    this.closeBtn.x = this.stage.stageWidth / 2
    this.closeBtn.y = this.stage.stageHeight - DataBus.share().adMaxHeight
    this.closeBtn.backgroundImage.texture = getRes('reward_close')
    this.closeBtn.clickBlock = () => {
      if (this.closeBlock) {
        this.closeBlock()
      }
        this.visible = false
    }

    this.addChild(this.closeBtn)
  }

  ensureBlock: () => void
}
