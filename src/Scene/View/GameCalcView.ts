
class GameCalcView extends BaseView {
  relifeImage = new egret.Bitmap(getRes('game_calc_bg'))
  // scoreBg = new egret.Bitmap(getRes('score_bg'))
  scoreTitle = new egret.TextField()
  scoreLabel = new egret.TextField()
  relifeBtn = new customButtom(new egret.Rectangle(this.width / 2, 0, 406, 136))
  shareBtn = new customButtom(new egret.Rectangle(this.width / 2, 0, 408, 120))
  ensureBlock: () => void
  closeBtn = new customButtom(new egret.Rectangle( 0, 0, 60, 60))
  closeBlock: () => void
  rankBoardBtn = new customButtom(new egret.Rectangle(0, 0, 300, 30))


  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  setupUI() {

    this.scoreLabel.text = DataBus.share().getScore().toString()
    this.scoreLabel.size = 35
    // this.scoreTitle.text = '本次得分'
    // this.scoreTitle.size = 30
    setCenterAnchor(this.relifeImage)
    this.relifeImage.anchorOffsetY = 0
    this.relifeImage.x = rectCenter(this).x
    this.relifeImage.y = 100
    this.addChild(this.relifeImage)

    // setCenterAnchor(this.scoreBg)
    //
    // this.scoreBg.x = rectCenter(this).x
    // this.scoreBg.y = this.relifeImage.y - 100
    // this.addChild(this.scoreBg)



    setCenterAnchor(this.scoreLabel)
    this.scoreLabel.x = rectCenter(this).x
    this.scoreLabel.y = this.relifeImage.y + 160
    this.addChild(this.scoreLabel)

    // setCenterAnchor(this.scoreTitle)
    // this.scoreTitle.x = rectCenter(this).x
    // this.scoreTitle.y = rectCenter(this).y - 100 - 30
    // this.addChild(this.scoreTitle)

    this.update()

    this.relifeBtn.anchorOffsetX = this.relifeBtn.width / 2
    this.relifeBtn.y = maxY(this.relifeImage) + 50
    this.relifeBtn.backgroundImage.texture = getRes('again_game')

    this.relifeBtn.clickBlock = () => {
					window.platform.wx['aldSendEvent']("结算页-再来一局")
      this.ensureBlock()
    }

    this.addChild(this.relifeBtn)

    this.shareBtn.anchorOffsetX = this.relifeBtn.width / 2
    this.shareBtn.y = maxY(this.relifeBtn) + 40
    this.shareBtn.backgroundImage.texture = getRes('challenge')
    this.shareBtn.clickBlock = () => {

					window.platform.wx['aldSendEvent']("结算页-发起挑战")
      window.platform.shareAppMessage({})
    }

    this.addChild(this.shareBtn)

    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.anchorOffsetY = this.closeBtn.height
    this.closeBtn.x = this.stage.stageWidth / 2
    this.closeBtn.y = this.stage.stageHeight - DataBus.share().adMaxHeight
    this.closeBtn.backgroundImage.texture = getRes('reward_close')
    this.closeBtn.clickBlock = () => {
					window.platform.wx['aldSendEvent']("结算页-关闭")
        this.visible = false
        if (this.closeBlock) {

          if (this['hiddenTip']) {
            this['hiddenTip']()
          }
          this.closeBlock()
        }
    }

    this.addChild(this.closeBtn)

    this.rankBoardBtn.textField.text = '查看完整排行榜 >>'
    this.rankBoardBtn.textField.size = 30
    this.rankBoardBtn.anchorOffsetX = this.rankBoardBtn.width / 2
    this.rankBoardBtn.anchorOffsetY = this.rankBoardBtn.height
    this.rankBoardBtn.x = this.stage.stageWidth / 2
    this.rankBoardBtn.y = maxY(this.relifeImage) - 30
    this.addChild(this.rankBoardBtn)
  }

  update() {
    // this.relifeBtn.backgroundImage.texture = getRes( DataBus.share().currentLife < 4 ? 'relife_btn_enable_1': 'relife_btn_1')
    this.scoreLabel.text = '本次得分：' + DataBus.share().getScore().toString()
    this.scoreLabel.textColor = 0x666666
    setCenterAnchor(this.scoreLabel)
  }
}
