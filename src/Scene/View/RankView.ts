
class RankView extends  BaseView {

  returnBtn: customButtom
  shareBtn: customButtom
  moreBtn: customButtom
  bgImageView: egret.Bitmap
  title: egret.TextField
  headerView: BaseView
  footerView: BaseView
  clickExBlock: any

  bgView: BaseView
  sence = 1
  setupUI() {

    this.bgImageView = new egret.Bitmap(getRes('ready_bg'))

    let scaleThis = this.height / this.width
    let scaleImage = this.bgImageView.height / this.bgImageView.width
    if (scaleThis > scaleImage) { //height拉满
      let scaleH = this.height / this.bgImageView.height
      this.bgImageView.height *= scaleH
      this.bgImageView.width *= scaleH
    } else {
      let scaleW = this.height / this.bgImageView.height
      this.bgImageView.height *= scaleW
      this.bgImageView.width *= scaleW
    }

    this.addChild(this.bgImageView)
    // this.bgView = new BaseView(rectCreate(0, 0, this.width, this.height))
    // this.addChild(this.bgView)


    this.returnBtn = new customButtom(rectCreate(20, 30, 100, 100))
    this.returnBtn.backgroundImage.texture = getRes('board_return')
    this.returnBtn.clickBlock = () => {
      window.platform.wx['aldSendEvent']("排行榜页返回")
      DataBus.share().hiddenBoard()
      this.parent['bannerAd'].show()
      this.parent['bannerAdIsHidden'] = false
      this.parent.removeChild(this)
      if (this.clickExBlock) {
          this.clickExBlock()
      }
    }
    this.addChild(this.returnBtn)





    // this.bgView.graphics.beginFill(0xf1edff, 1)
    // this.bgView.graphics.drawRoundRect(leftGap - 15, maxY(this.returnBtn) + 30, 650, 30, 30)
    // this.bgView.graphics.drawRoundRect(leftGap - 15, maxY(this.returnBtn) + 45 + 6 * 130 + 20 - 15, 650, 30, 30)
    // this.bgView.graphics.beginFill(0x7e6ac4, 1)
    // this.bgView.graphics.drawRect(leftGap - 10, maxY(this.headerView), 640, 6 * 130  - 10 + 30)
    // this.bgView.graphics.endFill()

    // this.footerView = new BaseView(rectCreate(0, 0, 640, 110))
    // this.footerView.cornerRadiusArr = [0, 0, 30, 30]
    // this.footerView.anchorOffsetX = this.footerView.width / 2
    // this.footerView.x = this.stage.stageWidth / 2
    // this.footerView.backgroundColor = 0xdcc7fd
    // this.footerView.y = maxY(this.headerView) + 6 * 130 + 20
    // this.addChild(this.footerView)

    // let data = {
    //   command: 'Board',
    //   x: leftGap,
    //   y: maxY(this.returnBtn) + 45 + 15 + 100,
    //   sence: this.sence
    // }
    // if (this.sence == 2) {
    //   data['qun'] =  DataBus.share().shareTicket
    // }
    // window.platform.openDataContext.postMessage(data)

    DataBus.share().postMessage({
      method: 'refreshBoard',
      param: {}
    })
    DataBus.share().showRankBoard()


    this.shareBtn = new customButtom(rectCreate(0, 0, 406, 136))
    this.shareBtn.backgroundImage.texture = getRes('board_share')
    this.shareBtn.anchorOffsetX = this.shareBtn.width / 2
    this.shareBtn.x = this.stage.stageWidth / 2
    // if (DataBus.share().systemInfo.model.indexOf('iPhone X') != -1) {
      this.shareBtn.y = this.stage.stageHeight - 180
    // } else {
    //   this.shareBtn.y = maxY(this.footerView) + 20
    // }
    this.addChild(this.shareBtn)
    this.shareBtn.clickBlock = () => {
      window.platform.wx['aldSendEvent']("排行榜页分享")
      window.platform.shareAppMessage({})
    }
  }

}
