
class PhoneEnterView extends BaseView {

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  bgImageView: egret.Bitmap
  closeBtn: customButtom

  change_1: customButtom
  change_2: customButtom
  cost = new egret.TextField()
  change_type = 1


  setupUI() {
    this.bgImageView = new egret.Bitmap(getRes('phoneEnterBg'))
    this.bgImageView.anchorOffsetY = this.bgImageView.height / 2
    this.bgImageView.anchorOffsetX = this.bgImageView.width / 2
    this.bgImageView.y = this.stage.stageHeight / 2 - 50
    this.bgImageView.x = this.stage.stageWidth / 2
    this.addChild(this.bgImageView)

    this.closeBtn = new customButtom(new egret.Rectangle(0, 0, 60, 60))
    this.closeBtn.backgroundImage.texture = getRes('reward_close')
    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.anchorOffsetY = this.closeBtn.height

    this.closeBtn.y = this.stage.stageHeight - DataBus.share().adMaxHeight
    this.closeBtn.x = this.stage.stageWidth / 2
    this.closeBtn.clickBlock = () => {
      this.parent.removeChild(this)
    }
    this.addChild(this.closeBtn)

    this.change_1 = new customButtom(new egret.Rectangle(0, 0, 249, 87))
    this.change_1.backgroundImage.texture = getRes('change_1')
    this.change_1.anchorOffsetX = this.change_1.width
    this.change_1.anchorOffsetY = this.change_1.height
    this.change_1.y = maxY(this.bgImageView) - 20
    this.change_1.x = this.bgImageView.x - 30

    this.change_1.clickBlock = () => {
      this.clickExchange()
    }

    this.addChild(this.change_1)

    // this.change_2 = new customButtom(new egret.Rectangle(0, 0, 249, 87))
    // this.change_2.backgroundImage.texture = getRes('change_1')
    // this.change_2.anchorOffsetX = 0
    // this.change_2.anchorOffsetY = this.change_1.height
    // this.change_2.y = maxY(this.bgImageView) - 20
    // this.change_2.x = this.bgImageView.x + 30
    // this.change_2.clickBlock = () => {
    //   this.exchange()
    // }
    // this.addChild(this.change_2)

    // this.cost.text = '话费余额:' + DataBus.share().redPakage + '元'
    this.cost.type = egret.TextFieldType.INPUT;
    this.cost.inputType = egret.TextFieldInputType.TEL;
    this.cost.size = 30
    this.cost.width = 500
    this.cost.height = 50
    this.cost.textColor = 0x666666
    this.cost.x = this.bgImageView.x - this.bgImageView.width / 2 + 70
    this.cost.y = this.bgImageView.y - this.bgImageView.height / 2 + 150
    this.addChild(this.cost)
    this.cost.setFocus()

  }

  clickExchange() {

    if (this.isMobileNumber(this.cost.text) == false) {
      return
    }
    // if (DataBus.share().redPakage < (type == 1 ? 20 : 50)) {
    //   window.platform['wx']['showModal']({
    //     title: '提示',
    //     content: '话费不足',
    //     showCancel: false
    //   })
    //   return
    // }

    // shop_id, mobile, userInfo, method = 'POST'
    window.platform.EndPoint.exchangeRedPakage(this.change_type == 1 ? 78: 79, this.cost.text, DataBus.share().userInfo)
    .then((data) => {
      DataBus.share().redPakage -= (this.change_type == 1 ? 20 : 50)
      window.platform['wx']['showToast']({
        title: '兑换成功！',
      })
      this.parent.removeChild(this)
      console.log(data, 5555555555)
    })
  }

 isMobileNumber(phone) {
    var flag = false;
    var message = "";
    var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(17[0-9]{1})|(15[0-3]{1})|(15[4-9]{1})|(18[0-9]{1})|(199))+\d{8})$/;
    if (phone == '') {
        // console.log("手机号码不能为空");
        message = "手机号码不能为空！";
    } else if (phone.length != 11) {
        //console.log("请输入11位手机号码！");
        message = "请输入11位手机号码！";
    } else if (!myreg.test(phone)) {
        //console.log("请输入有效的手机号码！");
        message = "请输入有效的手机号码！";
    } else {
        flag = true;
    }
    if (message != "") {
      window.platform['wx']['showToast']({
        title: message,
        icon: 'none'
      })
        // alert(message);
    }
    return flag;
  }


}
