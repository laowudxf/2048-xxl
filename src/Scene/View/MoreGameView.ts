
class MoreGameView extends BaseView {
  bgView: egret.Bitmap
  closeBtn: customButtom
  gameMap: any

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  setupUI() {
    // this.bgColorAlpha = 0.5
    this.bgView = new egret.Bitmap(getRes('moreGame_bg'))
    this.bgView.y = this.stage.stageHeight / 2
    this.anchorOffsetY = this.bgView.height / 2

    this.closeBtn = new customButtom(new egret.Rectangle(0, 0, 74, 94))
    this.closeBtn.backgroundImage.texture = getRes('moreGameBtn_open')
    this.closeBtn.anchorOffsetY = this.closeBtn.height / 2
    this.closeBtn.x = maxX(this.bgView) - 7
    this.closeBtn.y = rectCenter(this.bgView).y
    this.closeBtn.clickBlock = () => {
      // this.cancel()
    }
    this.addChild(this.closeBtn)
    this.addChild(this.bgView)


    this.gameMap = [
      ['wx32f39e398bd85f6d',"脑力挑战"],
      ['wx17c68d004a3e2685',"欢乐连连"],
      ['wx3419d17f02fbe61a',"欢乐一画"],
      ['wxbd66bb89ec2b9805',"来赚一亿"],
      ['wx03eb4405d98fd065',"每天步赚"],
      ['wx0154c51079b81af8',"皇上吉祥"],
      ['wxbe04dd8bfd5ad65c',"王牌狙击"],
      ['wx5857c62152010a85',"拆散情侣"],
      ['wx36b6c21b34074aa6',"头条视频"],
      ['wx8113af51e011f04c',"猜图大全"],
    ]

    let colume = 4
    let bgWidth = this.bgView.width
    let bgHeight = this.bgView.height

    for (let i = 0; i < this.gameMap.length; i++) {
        let item = this.gameMap[i]
        let row = Math.floor(i / colume)
        let c = i % colume
        let v = new customButtom(new egret.Rectangle(25 + (c * (90 + 25)), 25 + (row * (90 + 45)) + this.bgView.y, 90, 90))
        let shp = new egret.Sprite()
        shp.graphics.beginFill(0x00ff00);
        shp.graphics.drawCircle(v.x + 45, v.y + 45, 45);
        shp.graphics.endFill();
        this.addChild(shp)
        v.mask = shp
        v.cornerRadius = 45
        v.backgroundImage.texture = getRes('more_0' + (i + 1))
        v.mask
        let label = new egret.TextField()
        // label.width = 90
        label.textColor = 0xbababd
        label.size = 20
        label.text = item[1]
        // label.x = v.x + v.width / 2
        label.x = v.x + 2
        label.y = maxY(v) + 15
        label.textAlign = 'center'

        this.addChild(v)
        this.addChild(label)
        v.clickBlock = () => {
          window.platform.navigateToMiniProgram(item[0])
        }
    }

    this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.cancel, this)
		// back.addEventListener(egret.TouchEvent.TOUCH_TAP, this.Back, this);
  }

  cancel() {
    if (this['closeBlock']) {
      this['closeBlock']()
    }
      this.parent.removeChild(this)
  }

}
