
class MenuView extends BaseView {

   goonBtn: customButtom
   restartBtn: customButtom
   returnBtn: customButtom
   instruBtn: customButtom
   instrBtn: customButtom

   instruView: BaseView
   scoreBgImageView: egret.Bitmap

   scoreLabel: egret.TextField

   restartBlock: () => void
   returnBlock: () => void

   voiceBtn: customButtom

  bannerAd: any
  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  setScore(score: number) {
    this.scoreLabel.text = '最高得分:' +  score.toString()
    setCenterAnchor(this.scoreLabel)
  }

  setupUI() {

		this.bannerAd = createAd('adunit-ce2c8cdc5b4c8697', DataBus.share().systemInfo.screenHeight)
    this.bannerAd.hide()

    createCollectView(this)
    // let collectTip = new egret.Bitmap(getRes('collect'))
    //   if (DataBus.share().systemInfo.model.indexOf('iPhone X') != -1) {
    //     collectTip.y = 80
    //   } else {
    //     collectTip.y = 30
    //   }
    //
    //   collectTip.anchorOffsetX = collectTip.width
    //   collectTip.x = this.stage.stageWidth - 250
    //   let begin_x = collectTip.x
    //   egret.Tween.get(collectTip, {loop: true})
    //   .to({x: begin_x + 50}, 500)
    //   .to({x: begin_x}, 500)
    //
    //   this.addChild(collectTip)


    this.restartBtn = new customButtom(rectCreate(this.stage.stageWidth / 2, this.stage.stageHeight / 2, 340, 116))
    this.restartBtn.backgroundImage.texture = getRes('restart_2')
    setCenterAnchor(this.restartBtn)
    this.addChild(this.restartBtn)

    this.goonBtn = new customButtom(rectCreate(this.stage.stageWidth  / 2, this.stage.stageHeight / 2 - 160, 340, 116))
    this.goonBtn.backgroundImage.texture = getRes('goon_2')
    setCenterAnchor(this.goonBtn)
    this.addChild(this.goonBtn)

    this.returnBtn = new customButtom(rectCreate(this.stage.stageWidth / 2, this.stage.stageHeight / 2 + 160, 340, 116))
    this.returnBtn.backgroundImage.texture = getRes('return_2')
    setCenterAnchor(this.returnBtn)
    this.addChild(this.returnBtn)

    this.goonBtn.clickBlock = () => {
      this.parent['bannerAd'].show()
      this.bannerAd.hide()
			window.platform.wx['aldSendEvent']("暂停页-继续游戏")
        this.visible = false
    }

    this.restartBtn.clickBlock = () => {
      if (this.restartBlock) {
        this.restartBlock()
      }
    }

    this.returnBtn.clickBlock = () => {
      this.bannerAd.hide()
      if (this.returnBlock) {
        this.returnBlock()
      }
    }

    this.scoreBgImageView = new egret.Bitmap(getRes('highscore_bg'))
    setCenterAnchor(this.scoreBgImageView)
    this.scoreBgImageView.x = this.stage.stageWidth / 2
    this.scoreBgImageView.y = this.goonBtn.y - 160
    this.addChild(this.scoreBgImageView)

    this.scoreLabel = new egret.TextField()
    this.scoreLabel.size = 40
    this.scoreLabel.textColor = 0xffffff
    this.scoreLabel.y = this.goonBtn.y - 160
    this.scoreLabel.x = this.stage.stageWidth / 2
    DataBus.share().getHighestScore()
    .then((score) => this.setScore(score))
    this.addChild(this.scoreLabel)



    this.instruView = new InstruView(rectCreate(0, 0, this.stage.stageWidth, this.stage.stageHeight))
    this.instruView.visible = false

    this.instruBtn = new customButtom(rectCreate(this.stage.stageWidth / 2 + 30, maxY(this.returnBtn) + 40, 93, 93))
    this.instruBtn.backgroundImage.texture = getRes('instru')
    this.addChild(this.instruBtn)

    this.instruBtn.clickBlock = () => {
      window.platform.wx['aldSendEvent']("暂停页-问号")
      // DataBus.share().showWillOverView(false)
      DataBus.share().showOpenDataView(false)
      this.instruView.visible = true
      this.bannerAd.hide()
    }

    this.voiceBtn = new customButtom(rectCreate(this.stage.stageWidth / 2 - 30, maxY(this.returnBtn) + 40, 93, 93))
    this.voiceBtn.backgroundImage.texture = getRes(DataBus.share().getVoiceOpen() ? 'voice_open' : 'voice_close')
    this.voiceBtn.anchorOffsetX = this.voiceBtn.width
    this.addChild(this.voiceBtn)

    this.voiceBtn.clickBlock = () => {
      window.platform.wx['aldSendEvent']("暂停页-声音")
				let v = DataBus.share().getVoiceOpen()
        console.log(v)
				DataBus.share().setVoiceOpen(!v)
    }

    this.addChild(this.instruView)
  }

}
