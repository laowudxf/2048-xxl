class UserHammerView extends BaseView {

  beforeDraw() {
      this.backgroundColor = 0x000000
      this.bgColorAlpha = 0.4
  }

  setupUI() {

    let mc1 = frameAnimationGenerator('hammer_gif', 'run')
    this.addChild(mc1);
    mc1.y = 100
    mc1.frameRate = 12
    mc1.gotoAndPlay(1, -1)
  }
}
