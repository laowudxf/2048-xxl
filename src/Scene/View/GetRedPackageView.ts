
class GetRedPackageView extends BaseView {

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  bgImageView: egret.Bitmap
  closeBtn: customButtom

  openBtn: customButtom


  setupUI() {
    this.bgImageView = new egret.Bitmap(getRes('red_package_unlock'))
    this.bgImageView.anchorOffsetY = this.bgImageView.height / 2
    this.bgImageView.anchorOffsetX = this.bgImageView.width / 2
    this.bgImageView.y = this.stage.stageHeight / 2 - 80
    this.bgImageView.x = this.stage.stageWidth / 2
    this.addChild(this.bgImageView)

    this.closeBtn = new customButtom(new egret.Rectangle(0, 0, 60, 60))
    this.closeBtn.backgroundImage.texture = getRes('reward_close')
    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.anchorOffsetY = this.closeBtn.height

    this.closeBtn.y = this.stage.stageHeight - DataBus.share().adMaxHeight
    this.closeBtn.x = this.stage.stageWidth / 2
    this.closeBtn.clickBlock = () => {
      this.parent['hiddenCollectTip']()
      this.parent.removeChild(this)
      DataBus.share().showChaseView()
    }
    this.addChild(this.closeBtn)


    this.openBtn = new customButtom(new egret.Rectangle(0, 0, 200, 200))
    // this.openBtn.backgroundImage.texture = getRes('reward_close')
    this.openBtn.anchorOffsetX = this.openBtn.width / 2
    this.openBtn.anchorOffsetY = this.openBtn.height
    this.openBtn.x = this.stage.stageWidth / 2
    this.openBtn.y = this.height / 2 + 90
    this.openBtn.clickBlock = () => {
      console.log(1111)
				window.platform.createVideoAd('adunit-aacd22039ba2ed3f', () => {
          let v = new GetRedPackageView_1(new egret.Rectangle(0, 0, this.width, this.height))
          this.parent.addChild(v)

          this.parent['showCollectTip']()
          this.parent.removeChild(this.parent['moreGameBtn'])
          this.parent.addChild(this.parent['moreGameBtn'])
          this.parent.removeChild(this)

        }, () => {
						window.platform.wx['showToast']({
							title: '开启红包失败',
							icon: 'none',
						})
        }, DataBus.share())
    }
    this.addChild(this.openBtn)
  }

}
