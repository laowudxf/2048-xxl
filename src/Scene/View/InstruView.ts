class InstruView extends BaseView {
  instruImage: egret.Bitmap
  returnBtn: customButtom
  ensureBtn: customButtom

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  setupUI() {
    this.instruImage = new egret.Bitmap(getRes('rule'))
    setCenterAnchor(this.instruImage)
    this.instruImage.x = this.width / 2
    this.instruImage.y = this.height / 2 - 40
    this.addChild(this.instruImage)




    this.returnBtn = new customButtom(rectCreate( maxX(this.instruImage), this.instruImage.y - (this.instruImage.height / 2), 70, 70))
    this.returnBtn.anchorOffsetX = this.returnBtn.width / 2
    this.returnBtn.anchorOffsetY = this.returnBtn.height / 2
    this.returnBtn.backgroundImage.texture = getRes('close_btn')
    this.addChild(this.returnBtn)
    this.returnBtn.clickBlock = () => {
      window.platform.wx['aldSendEvent']("暂停页-问号-关闭")
      this.parent['bannerAd'].show()
      this.parent['bannerAdIsHidden'] = false
      DataBus.share().showChaseView()
      this.visible = false
    }

    this.ensureBtn = new customButtom(rectCreate( this.width / 2, maxY(this.instruImage) + 30, 406, 136))
     this.ensureBtn.anchorOffsetX = this.ensureBtn.width / 2
    this.ensureBtn.backgroundImage.texture = getRes('intro_ensure')
     this.addChild(this.ensureBtn)

    this.ensureBtn.clickBlock = () => {
      window.platform.wx['aldSendEvent']("暂停页-问号-关闭")
      this.parent['bannerAd'].show()
      this.parent['bannerAdIsHidden'] = false
      DataBus.share().showChaseView()
      this.visible = false
    }
  }
}
