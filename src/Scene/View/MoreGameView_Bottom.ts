
class MoreGameView_Bottom extends BaseView {
  bgView: egret.Bitmap
  gameMap: any

  beforeDraw() {
    // this.backgroundColor = 0x000000
    // this.bgColorAlpha = 0.4
  }

  setupUI() {
    // this.bgColorAlpha = 0.5
    // this.bgView = new egret.Bitmap(getRes('moreGame_bg'))
    // this.bgView.y = this.height / 2
    // this.anchorOffsetY = this.bgView.height / 2
    // this.addChild(this.bgView)
    this.gameMap = [
      ['wx32f39e398bd85f6d',"脑力挑战"],
      ['wx17c68d004a3e2685',"欢乐连连"],
      ['wx3419d17f02fbe61a',"欢乐一画"],
      ['wxbd66bb89ec2b9805',"来赚一亿"],
      ['wx03eb4405d98fd065',"每天步赚"],
      ['wx0154c51079b81af8',"皇上吉祥"],
      ['wxbe04dd8bfd5ad65c',"王牌狙击"],
      ['wx5857c62152010a85',"拆散情侣"],
      ['wx36b6c21b34074aa6',"头条视频"],
      ['wx8113af51e011f04c',"猜图大全"],
    ]

    let colume = 5
    let bgWidth = this.height
    let bgHeight = this.width

    let r = this.getRandomIndex()
    for (let i = 0; i < r.length; i++) {
        // let item = this.gameMap[i]
        let index = r[i]
        let item = this.gameMap[index]
        let row = Math.floor(i / colume)
        let c = i % colume
        let v = new customButtom(new egret.Rectangle((c * (90 + 30)),(row * (90 + 50)), 90, 90))
        let shp = new egret.Sprite()
        shp.graphics.beginFill(0x00ff00);
        // shp.graphics.drawCircle(v.x + 45, v.y + 45, 45);
        shp.graphics.drawRoundRect(v.x, v.y, 90, 90, 20, 20)
        shp.graphics.endFill();
        this.addChild(shp)
        v.mask = shp
        v.cornerRadius = 45
        v.backgroundImage.texture = getRes('more_0' + (index + 1))
        v.mask
        let label = new egret.TextField()
        // label.width = 90
        label.textColor = 0xffffff
        label.size = 20
        label.text = item[1]
        // label.x = v.x + v.width / 2
        label.x = v.x + 5
        label.y = maxY(v) + 15
        label.textAlign = 'center'

        this.addChild(v)
        this.addChild(label)
        v.clickBlock = () => {
          window.platform.navigateToMiniProgram(item[0])
        }
    }

    // this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.cancel, this)
		// back.addEventListener(egret.TouchEvent.TOUCH_TAP, this.Back, this);
  }

  cancel() {
      // this.parent.removeChild(this)
  }

  getRandomIndex() {
    var arr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    var result = [ ];

    var ranNum = 5;

    for (var i = 0; i < ranNum; i++) {

      var ran = Math.floor(Math.random() * arr.length);

      result.push(arr[ran]);

      var center = arr[ran];

      arr[ran] = arr[arr.length - 1];

      arr[arr.length - 1] = center;

      arr = arr.slice(0, arr.length - 1);
    }

    return result
  }

}
