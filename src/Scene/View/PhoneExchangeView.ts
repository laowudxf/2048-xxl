
class PhoneExchangeView extends BaseView {

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.6
  }

  bgImageView: egret.Bitmap
  closeBtn: customButtom

  change_1: customButtom
  change_2: customButtom
  cost = new egret.TextField()


  setupUI() {
    this.bgImageView = new egret.Bitmap(getRes('prepaid_recharge'))
    this.bgImageView.anchorOffsetY = this.bgImageView.height / 2
    this.bgImageView.anchorOffsetX = this.bgImageView.width / 2
    this.bgImageView.y = this.stage.stageHeight / 2 - 150
    this.bgImageView.x = this.stage.stageWidth / 2
    this.addChild(this.bgImageView)

    this.closeBtn = new customButtom(new egret.Rectangle(0, 0, 60, 60))
    this.closeBtn.backgroundImage.texture = getRes('reward_close')
    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.anchorOffsetY = this.closeBtn.height

    this.closeBtn.y = this.stage.stageHeight - DataBus.share().adMaxHeight
    this.closeBtn.x = this.stage.stageWidth / 2
    this.closeBtn.clickBlock = () => {
      this.parent.removeChild(this)
    }
    this.addChild(this.closeBtn)

    this.change_1 = new customButtom(new egret.Rectangle(0, 0, 249, 87))
    this.change_1.backgroundImage.texture = getRes('change_1')
    this.change_1.anchorOffsetX = this.change_1.width
    this.change_1.anchorOffsetY = this.change_1.height
    this.change_1.y = maxY(this.bgImageView) - 20
    this.change_1.x = this.bgImageView.x - 30

    this.change_1.clickBlock = () => {
      this.exchange(1)
    }

    this.addChild(this.change_1)

    this.change_2 = new customButtom(new egret.Rectangle(0, 0, 249, 87))
    this.change_2.backgroundImage.texture = getRes('change_1')
    this.change_2.anchorOffsetX = 0
    this.change_2.anchorOffsetY = this.change_1.height
    this.change_2.y = maxY(this.bgImageView) - 20
    this.change_2.x = this.bgImageView.x + 30
    this.change_2.clickBlock = () => {
      this.exchange(2)
    }
    this.addChild(this.change_2)

    this.cost.text = '话费余额:' + DataBus.share().redPakage + '元'
    this.cost.size = 30
    this.cost.textColor = 0x666666
    this.cost.x = this.bgImageView.x - this.bgImageView.width / 2 + 34
    this.cost.y = this.bgImageView.y - this.bgImageView.height / 2 + 145
    this.addChild(this.cost)

    // this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.backToReady, this);

  }

  // backToReady() {
  //     this.parent.removeChild(this)
  // }

  exchange(type) {
    if (DataBus.share().redPakage < (type == 1 ? 20 : 50)) {
      window.platform['wx']['showToast']({
        title: '话费不足',
        icon: 'none'
      })
      return
    }

    let v = new PhoneEnterView(new egret.Rectangle(0, 0, this.width, this.height))
    v.change_type = type
    this.parent.addChild(v)
    this.parent.removeChild(this)

    // shop_id, mobile, userInfo, method = 'POST'

    // window.platform.EndPoint.exchangeRedPakage(type == 1 ? 78: 79, '13968933306', DataBus.share().userInfo)
    // .then((data) => {
    //   console.log(data, 5555555555)
    // })
  }


}
