
class GetBox extends BaseView {

  toolBgImage: egret.Bitmap
  boxImage: egret.Bitmap
  shareBtn: customButtom
  titleImage: egret.Bitmap
  toolCount: egret.Bitmap
  closeBtn: customButtom
  share_get_btn: customButtom
  video_get_btn: customButtom

  hadShared = false
  finished = false

  rewardedVideoAd = window.platform.wx['createRewardedVideoAd']({ adUnitId: 'adunit-25209c26785fc476' })

  beforeDraw() {
    this.backgroundColor = 0x000000
    this.bgColorAlpha = 0.4
  }

  setupUI() {
    this.toolBgImage = new egret.Bitmap(getRes('bg_light_1'))
    this.addChild(this.toolBgImage)

    this.boxImage = new egret.Bitmap(getRes('box_close'))
    setCenterAnchor(this.boxImage)
    this.boxImage.x = this.width / 2
    this.boxImage.y = this.height / 2 - 50
    this.addChild(this.boxImage)
    this.boxImage.touchEnabled = true
    this.boxImage.addEventListener('touchTap', this.clicked, this)

    this.toolBgImage.x = this.boxImage.x
    this.toolBgImage.y = this.boxImage.y
    this.toolBgImage.anchorOffsetX = this.toolBgImage.width / 2 + 11
    this.toolBgImage.anchorOffsetY = this.toolBgImage.width / 2 - 7

    this.shareBtn = new customButtom(rectCreate(0, 0, 335, 112))
    this.shareBtn.anchorOffsetX = 335 / 2
    this.shareBtn.x = this.width / 2
    this.shareBtn.y = maxY(this.boxImage) + 150
    this.shareBtn.backgroundImage.texture = getRes('free_get')
    this.shareBtn.visible = false
    this.addChild(this.shareBtn)

    this.share_get_btn = new customButtom(rectCreate(0, 0, 332, 111))
    this.share_get_btn.anchorOffsetX = this.share_get_btn.width / 2
    this.share_get_btn.x = this.stage.stageWidth / 2
    this.share_get_btn.y = maxY(this.boxImage) + 150
    this.share_get_btn.backgroundImage.texture = getRes('free_get')
    this.addChild(this.share_get_btn)

    this.video_get_btn = new customButtom(rectCreate(0, 0, 332, 111))
    this.video_get_btn.visible = false
    this.video_get_btn.anchorOffsetX = this.video_get_btn.width / 2
    this.video_get_btn.x = this.stage.stageWidth / 2
    this.video_get_btn.y = maxY(this.share_get_btn) + 40
    this.video_get_btn.backgroundImage.texture = getRes('video_get')
    this.addChild(this.video_get_btn)

    this.video_get_btn.clickBlock = () => {
      this.rewardedVideoAd.onClose(res => {
        // 用户点击了【关闭广告】按钮
        // 小于 2.1.0 的基础库版本，res 是一个 undefined
        if (res && res.isEnded || res === undefined) {
          // 正常播放结束，可以下发游戏奖励
          this.hadShared = true
          this.shareBtn.visible = true
          this.video_get_btn.visible = this.share_get_btn.visible = false
          window.platform.wx['aldSendEvent']("免费宝箱视频获得")
          this.clicked()

        }
        else {
          // 播放中途退出，不下发游戏奖励
        }
      })
      this.rewardedVideoAd.show().catch(() => {
        this.rewardedVideoAd.load()
        .then(() => this.rewardedVideoAd.show())
      })

      this.rewardedVideoAd.onError(err => {
        DataBus.share().shareTime = Date.now()

        DataBus.share().onShowBlocks.push(() => {
          let now = Date.now()
          let timeOffset = now - DataBus.share().shareTime
          if (timeOffset < 4 * 1000 && DataBus.share().isPreProduct() == false) { //fail
            window.platform.wx['showModal']({
              title: '分享失败',
              content: '需要分享到不同的群',
              showCancel: false
            })
            return
          }

          this.hadShared = true
          this.shareBtn.visible = true
          this.video_get_btn.visible = this.share_get_btn.visible = false
          window.platform.wx['aldSendEvent']("免费宝箱视频获得")
          this.clicked()
        })
        window.platform.shareAppMessage({})

        console.log(err)
      })
    }

    this.share_get_btn.clickBlock = this.shareBtn.clickBlock = () => {
      if (!this.finished) {
        console.log('share')
        window.platform.createVideoAd("adunit-f8b28e2e96634341", () => {
          this.hadShared = true
          this.shareBtn.visible = true
          this.video_get_btn.visible = this.share_get_btn.visible = false
          window.platform.wx['aldSendEvent']("免费宝箱分享获得")
          this.clicked()
        }, () => {
          window.platform.wx['showModal']({
            title: '获取失败',
            content: '获取宝箱失败',
            showCancel: false
          })
        }, DataBus.share())

        // if (DataBus.share().isPreProduct() == false) {
        //   window.platform.shareAppMessage({})
        //   DataBus.share().shareTime = Date.now()
        //   DataBus.share().onShowBlocks.push(() => {
        //     let now = Date.now()
        //     let timeOffset = now - DataBus.share().shareTime
        //     if (timeOffset < 4 * 1000) { //fail
        //       window.platform.wx['showModal']({
        //         title: '分享失败',
        //         content: '需要分享到不同的群',
        //         showCancel: false
        //       })
        //       return
        //     }
        //     this.hadShared = true
        //     this.shareBtn.visible = true
        //     this.video_get_btn.visible = this.share_get_btn.visible = false
        //     window.platform.wx['aldSendEvent']("免费宝箱分享获得")
        //     this.clicked()
        //   })
        // } else {
        //   this.hadShared = true
        //     window.platform.wx['aldSendEvent']("免费宝箱分享获得")
        //   this.clicked()
        // }
      } else { //领取奖励
        this.parent['bannerAd'].show()
      this.parent['bannerAdIsHidden'] = false
        this.parent.removeChild(this)
      }

    }

    this.titleImage = new egret.Bitmap(getRes('get_box'))
    this.titleImage.anchorOffsetX = this.titleImage.width / 2
    this.titleImage.x = this.stage.stageWidth / 2
    this.titleImage.y = this.boxImage.y - 400
    this.addChild(this.titleImage)

    this.toolCount = new egret.Bitmap(getRes('x1'))
    this.toolCount.anchorOffsetY = this.toolCount.height
    this.toolCount.anchorOffsetX = this.toolCount.width / 2
    this.toolCount.x = maxX(this.boxImage)
    this.toolCount.y = maxY(this.boxImage)
    this.toolCount.visible = false
    this.addChild(this.toolCount)

    this.closeBtn = new customButtom(rectCreate(0, 0, 60, 60))
    this.closeBtn.backgroundImage.texture = getRes('reward_close')
    this.closeBtn.anchorOffsetX = this.closeBtn.width / 2
    this.closeBtn.anchorOffsetY = this.closeBtn.height
    this.closeBtn.x = this.stage.stageWidth / 2
    this.closeBtn.y = this.stage.stageHeight - DataBus.share().adMaxHeight
    this.closeBtn.clickBlock = () => {
      window.platform.wx['aldSendEvent']("免费宝箱页-关闭")
      this.parent.removeChild(this)
    }
    console.log(this.closeBtn.x)
    console.log(this.closeBtn.y)
    this.addChild(this.closeBtn)

    //animation

    this.titleImage.scaleX = 0
    this.titleImage.scaleY = 0

    this.boxImage.scaleX = 0.2
    this.boxImage.scaleY = 0.2

    this.toolBgImage.scaleX = 0
    this.toolBgImage.scaleY = 0

    this.shareBtn.scaleX = 0
    this.shareBtn.scaleY = 0

    this.closeBtn.scaleX = 0
    this.closeBtn.scaleY = 0

    egret.Tween.get(this.boxImage)
    .to({scaleX: 2, scaleY: 2}, 200)
    .call(() => {
      egret.Tween.get(this.titleImage)
      .to({scaleX: 1.2, scaleY: 1.2}, 200)
      .to({scaleX: 1, scaleY: 1}, 200)

      egret.Tween.get(this.toolBgImage)
      .to({scaleX: 1, scaleY: 1}, 400)
      .to({rotation: 360}, 7000)

    })
    .to({scaleX: 1, scaleY: 1}, 200).wait(200)
    .call(() => {
      egret.Tween.get(this.shareBtn)
      .to({scaleX: 1, scaleY: 1}, 200)

      egret.Tween.get(this.closeBtn)
      .to({scaleX: 1, scaleY: 1}, 200)
    })
  }

  clicked() {
    if (!this.hadShared) {
      this.shareBtn.clickBlock()
      return
    }

    if (this.finished) {
      return
    }

    this.finished = true
    this.shareBtn.backgroundImage.texture = getRes('free_get_1')
    console.log('click')
    this.boxImage.texture = getRes('air_bubbles')
    this.toolBgImage.visible = false
    this.toolCount.visible = true
    setCenterAnchor(this.boxImage)

    this.getTool()
    // this.parent.removeChild(this)
  }

  getTool() {
      let result = Math.random()
      let tool = new egret.Bitmap()
        let gameTool = DataBus.share().gameToolData
      if (result < 0.3) { // 2048
        tool.texture = getRes('2048_tool_1')
        gameTool['addTool2048Count']()
      } else if (result < 0.65) { // hammer
        tool.texture = getRes('hammer')
        gameTool['addHammerCount']()
      } else { // refresh
        tool.texture = getRes('tool_refresh')
        gameTool['addRefreshCount']()
      }

      tool.width = 100
      tool.height = 100

      setCenterAnchor(tool)
      tool.x = this.width / 2
      tool.y = this.boxImage.y
      this.addChild(tool)
    }

}
