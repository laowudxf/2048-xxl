class BoardScene extends egret.Sprite {
	private platform: any = window.platform;
	public Scene: number = 0; //0好友1全球2群
	public constructor() {
		super();
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createGameScene, this);
	}
	//界面被添加
	private createGameScene() {
		this.SceneBackground();
		this.SceneTopBar((this.platform.System.statusBarHeight * this.stage.stageHeight) / 667 - 10);
		this.BackgroundRect((150 * this.stage.stageHeight) / 1334);
		switch (this.Scene) {
			case 1:
				this.SceneBoardSever((150 * this.stage.stageHeight) / 1334);
				break;
			case 2:
				this.SceneBoardQun((150 * this.stage.stageHeight) / 1334);
				break;
			default:
				this.SceneBoard((150 * this.stage.stageHeight) / 1334);
		}
		this.SceneButton((1150 * this.stage.stageHeight) / 1334);
	}
	//初始化背景
	private SceneBackground() {
		var img: egret.Bitmap = new egret.Bitmap(getRes('play_bg'));
		img.width *= this.stage.stageHeight / img.height;
		img.x = (this.stage.stageWidth - img.width) / 2;
		img.height = this.stage.stageHeight;
		img.height = this.stage.stageHeight;
		this.addChild(img);
	}
	//初始化顶部
	private SceneTopBar(y: number) {
		let back = new BackButton(50, y, 20, 40);
		back.addEventListener(egret.TouchEvent.TOUCH_TAP, this.Back, this);
		this.addChild(back);
		this.DrawText('六边形', y);
	}
	//绘制全部排行
	private SceneBoardSever(y: number) {
		this.DrawText('全球排行榜', y + 50, 0x89f1f8);
		this.platform.getFriendFromSever().then(FriendScore => {
			FriendScore.forEach((u, s) => {
				let i = s + 1;
				let bar = this.UserBar(y, i);
				i <= 3 ? this.UserBarIcon(bar, 'list_' + i) : this.UserBarIconC(bar, i);
				this.UserSort(bar, u);
			});
		});
	}
	//绘制群排行
	private SceneBoardQun(y: number) {
		this.DrawText('群排行', y + 50, 0x89f1f8);
		this.platform.FriendScoreInit().then(FriendScore => {
			FriendScore.forEach((u, s) => {
				let i = s + 1;
				let bar = this.UserBar(y, i);
				i <= 3 ? this.UserBarIcon(bar, 'list_' + i) : this.UserBarIconC(bar, i);
				this.UserSort(bar, u);
			});
		});
	}
	//绘制好友排行榜
	private SceneBoard(y: number) {
		this.DrawText('好友排行榜', y + 50, 0x89f1f8);
		this.platform
			.FriendScoreInit()
			.then(() => this.platform.getUserSort())
			.then(FriendScore => FriendScore.slice(0, 6))
			.then(FriendScore => {
				FriendScore.forEach((u, s) => {
					let i = s + 1;
					let bar = this.UserBar(y, i);
					i <= 3 ? this.UserBarIcon(bar, 'list_' + i) : this.UserBarIconC(bar, i);
					this.UserSort(bar, u);
				});
			});
	}
	//绘制群分享按钮
	private SceneButton(y: number) {
		let see = new DefaultButton(145, y, 460, 78);
		see.Content.text = '查看群排行';
		this.addChild(see);
		see.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ShareQun, this);
	}
	//分享到群
	private ShareQun() {
		if (this.platform.QunId) {
			var desk: BoardScene = new BoardScene();
			desk.Scene = 2;
			this.parent.addChild(desk);
			this.parent.removeChild(this);
		} else {
			this.platform.shareAppMessage(this.platform.baseShare).then(res => {
				if (res.errMsg == 'shareAppMessage:ok') return;
			});
		}
	}
	//用户条
	private UserBar(y: number, i: number) {
		let radius = 20;
		let bar: egret.Sprite = new egret.Sprite();
		bar.x = 85;
		bar.y = y + 130 * i;
		bar.graphics.beginFill(0x3e2e72, 1);
		bar.graphics.drawArc(radius, radius, radius, Math.PI * 1, Math.PI * 1.5, false);
		bar.graphics.lineTo(580 - radius, 0);
		bar.graphics.drawArc(580 - radius, radius, radius, Math.PI * 1.5, Math.PI * 2, false);
		bar.graphics.lineTo(580, 120 - radius);
		bar.graphics.drawArc(580 - radius, 120 - radius, radius, Math.PI * 2, Math.PI * 0.5, false);
		bar.graphics.lineTo(radius, 120);
		bar.graphics.drawArc(radius, 120 - radius, radius, Math.PI * 0.5, Math.PI * 1, false);
		bar.graphics.lineTo(0, radius);
		bar.graphics.endFill();
		this.addChild(bar);
		return bar;
	}
	//图片前缀
	private UserBarIcon(bar: egret.Sprite, res: string) {
		let img: egret.Bitmap = new egret.Bitmap(getRes(res));
		img.x = 30;
		img.y = (bar.height - img.height) / 2;
		bar.addChild(img);
	}
	//圆形前缀
	private UserBarIconC(bar: egret.Sprite, sort: number) {
		let c: egret.Shape = new egret.Shape();
		c.graphics.beginFill(0xffffff, 0.1);
		c.graphics.drawCircle(60, bar.height / 2, 30);
		c.graphics.endFill();
		bar.addChild(c);
		let t: egret.TextField = new egret.TextField();
		t.x = 30;
		t.y = bar.height / 2 - 30;
		t.width = 60;
		t.height = 60;
		t.text = sort + '';
		t.textAlign = egret.HorizontalAlign.CENTER;
		t.verticalAlign = egret.VerticalAlign.MIDDLE;
		bar.addChild(t);
	}
	//绘制用户数据
	private UserSort(bar: egret.Sprite, user: any) {
		this.platform.setUserAvatar(user).then(() => {
			let img: CircleImg = new CircleImg(130, 20, 80);
			img.Img = user.avatar;
			img.border = 2;
			bar.addChild(img);
		});
		let nickname: egret.TextField = new egret.TextField();
		nickname.text = user.nickname;
		nickname.x = 230;
		nickname.height = 120;
		nickname.verticalAlign = egret.VerticalAlign.MIDDLE;
		nickname.size = 28;
		bar.addChild(nickname);
		let score: egret.TextField = new egret.TextField();
		score.text = user.score;
		score.height = 120;
		score.verticalAlign = egret.VerticalAlign.MIDDLE;
		score.width = 550;
		score.textAlign = egret.HorizontalAlign.RIGHT;
		score.size = 34;
		score.textColor = 0xffde00;
		bar.addChild(score);
	}
	//卷轴长条
	private BackgroundBar(color: number, y: number) {
		let bar: egret.Shape = new egret.Shape();
		bar.graphics.lineStyle(30, color);
		bar.graphics.moveTo(65, y);
		bar.graphics.lineTo(685, y);
		bar.graphics.endFill();
		this.addChild(bar);
	}
	//卷轴
	private BackgroundRect(y: number) {
		this.BackgroundBar(0x342375, y);
		this.BackgroundBar(0x342375, y + 910);
		let rect: egret.Shape = new egret.Shape();
		rect.graphics.beginFill(0x7e6ac4, 1);
		rect.graphics.drawRect(65, y, 620, 910);
		rect.graphics.endFill();
		this.addChild(rect);
	}
	//居中文字
	private DrawText(text: string, y: number, color: number = 0xffffff) {
		let Content = new egret.TextField();
		Content.size = 40;
		Content.y = y;
		Content.textColor = color;
		Content.text = text;
		Content.width = this.stage.stageWidth;
		Content.textAlign = egret.HorizontalAlign.CENTER;
		this.addChild(Content);
	}
	//退出页面
	private Back() {
		var desk: ReadyScene = new ReadyScene();
		this.parent.addChild(desk);
		this.parent.removeChild(this);
	}
}
