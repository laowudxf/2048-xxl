
interface ViewType {
   x: number
   y: number
   width: number
   height: number
   anchorOffsetX: number
   anchorOffsetY: number
}

function maxY(view: ViewType) {
  return view.y + view.height - view.anchorOffsetY
}

function maxX(view: ViewType) {
  return view.x + view.width - view.anchorOffsetX
}

function rectCenter(view: ViewType) {
    return new egret.Point(view.x + view.width / 2, view.y + view.height / 2)
}

function relativeCenter(view: ViewType) {
    return new egret.Point(view.width / 2,view.height / 2)
}


function rectCreate(x: number, y: number, width: number, height: number) {
  return new egret.Rectangle(x, y, width, height)
}

function setCenterAnchor(view: ViewType) {
  view.anchorOffsetX = view.width / 2
  view.anchorOffsetY = view.height / 2
}

function frameAnimationGenerator(fileName: string, animationName: string = 'run') {
  var data = RES.getRes(fileName + ".json");
  var txtr = RES.getRes(fileName + ".png");
  var mcFactory:egret.MovieClipDataFactory = new egret.MovieClipDataFactory( data, txtr );
  var clipData = mcFactory.generateMovieClipData(animationName)
  var mc1:egret.MovieClip = new egret.MovieClip( clipData);
  return mc1
}

function downcatFirstchar(str: string): string {
  let sub = str.substr(1)
  let result = (str[0] as string).toLowerCase() + sub
  return result
}

function getRes(name: string) {
  if (DataBus.share().spriteSheet) {
    let texture = DataBus.share().spriteSheet.getTexture(name)
    if (texture == null) {
      return RES.getRes(name)
    }
      return texture
  } else {
      return RES.getRes(name)
  }
}

function createAd(banner_id, stageHeight) {
  let width = DataBus.share().systemInfo.windowWidth
  let bannerAd = window.platform.wx['createBannerAd']({
    adUnitId: banner_id,
    style: {
      left: (width - 325) / 2,
      top: stageHeight + 250,
      width: 325
    }
  })

  bannerAd.show()
  .then(() => {
    if (bannerAd) {
      let info = DataBus.share().systemInfo
      if (info.model.indexOf('iPhone X') != -1) {
        bannerAd.style.top = stageHeight - bannerAd.style.realHeight - 5
      } else {
        bannerAd.style.top = stageHeight - bannerAd.style.realHeight
      }

    }
  })

  bannerAd.onError(err => {
    console.log(err)
  })

  return bannerAd
}

function createCollectView(view) {

    let collectTip = new egret.Bitmap(getRes('collect'))
      if (DataBus.share().systemInfo.model.indexOf('iPhone X') != -1) {
        collectTip.y = 80
      } else {
        collectTip.y = 30
      }

      collectTip.anchorOffsetX = collectTip.width
      collectTip.x = view.stage.stageWidth - 250
      let begin_x = collectTip.x
      egret.Tween.get(collectTip, {loop: true})
      .to({x: begin_x + 50}, 500)
      .to({x: begin_x}, 500)

      view.addChild(collectTip)
      view['collectTip'] = collectTip
      return collectTip

}
