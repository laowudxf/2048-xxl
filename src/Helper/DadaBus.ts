

interface Observable {
  observers: any
  registerObserver: (observer: Observer, key: string) => void
  removeObserver: (observer: Observer, key: string) => void
  notice: (value: any, key: string) => void
}

interface Observer {
    subscribe: (before: any, now: any, key: string,instance: Observable) => void
}

class GameToolsData {
  private hammerCount = 0
  private refreshCount = 0
  private tool2048Count = 0
  observableNameList = [
    'HammerCount',
    'RefreshCount',
    'Tool2048Count'
  ]

 downcatFirstchar(str: string): string {
  let sub = str.substr(1)
  let result = (str[0] as string).toLowerCase() + sub
  return result
}

  setupSetGetFunction() {
      this.observableNameList.map((key) => {

      let downCas = this.downcatFirstchar(key)
        this['set' + key] = (value) => {
            console.log('set function')
            console.log(value)
          if ((value == null) || value == "") {
            value = 0
          }
          if ((typeof value) == 'string') {
            console.log(3333333)
            value = parseInt(value)
          }

          // console.log('value: ' + value)
          this.setJacker(value, downCas)
            this[downCas] = value
        }

        this['get' + key] = () => {
          return this[downCas]
        }

        this['add' + key] = () => {
            let v = this['get' + key]()
            this['set' + key](v + 1)
        }
      })

  }

  constructor() {
    this.setupSetGetFunction()
  }

  noticeBlock: (value: any, key: string, oldValue: any) => void

  setJacker(value: any, key: string) {
    window.platform.setStorage({key, data: value.toString()})

    if (this.noticeBlock) {
      this.noticeBlock(value, key, this[key])
    }
  }

  updateFromServer() {
    //-- to do
    return new Promise((s, f) => {
      return s()
    })
  }
}

class DataBus implements Observable {
  private static shareInstanse: DataBus = new DataBus()
  main: Main
  hadShowDailyReward = false
  share_content: any

  gameToolData = new GameToolsData()

  public observers = {}
  userInfo = {}

  isDebug = false
  baseUrl = this.isDebug ? 'https://caige-app.sl.idler8.com/program/' : 'https://caige1.sanliwenhua.com/program/'
  baseKey = this.isDebug ? 68 : 68

  shareTime: any
  //should observable
  private score: number = 0
  private highestScore: number = null
  private gameOver = false
  public isFirstGame = false
  private voiceOpen = true

  //
  public isMergeCard = false
  public freeLife = 0
  public amountLife = 2
  public currentLife = 0
  merge2048Count = 0
  shareTicket: string
  spriteSheet: egret.SpriteSheet
  config: Object
  preProduct: boolean = true
  build: number = 1
  preProduct_1: boolean = true

  local_build: number = 2
  systemInfo: any


  //userTool
  private isUseHammer = false

  //generat value
  public minValue = 2
  public maxValue = 4
	private AudioBg: egret.SoundChannel;

  _redPkakage: number = 0

  _dailyRewardTimeOffset: number = null

  get dailyRewardTimeOffset() {
      return this._dailyRewardTimeOffset
  }

  set dailyRewardTimeOffset(value) {
    this.notice(value, 'dailyRewardTimeOffset')
    this._dailyRewardTimeOffset = value
  }


  get lastGetDailyRewardTime() {
    let r = null
    try {
      r = window.platform.Storage.get('lastGetDailyRewardTime')
    } catch(err) {
      return null
    }
    return r
  }

  set lastGetDailyRewardTime(time) {
    window.platform.Storage.set('lastGetDailyRewardTime', time)
  }

  get canGetDailyReward() {
    return this.dailyRewardTimeOffset == null || this.dailyRewardTimeOffset > (60 * 60 * 1000)
  }

  get dailyRewardTimeString() {
    let str = this.calcTimeStr(3600 * 1000 - this.dailyRewardTimeOffset)
    return str
  }

  calcTimeStr(offset) {
    offset = Math.floor(offset / 1000)
    let sec = offset % 60
    let min = Math.floor(offset / 60) % 60
    let hour = Math.floor(offset / 3600) % 24
    let str = this.timeFormate(hour) + ':' + this.timeFormate(min) + ':' + this.timeFormate( sec)
    return str
  }

  timeFormate(str) {
    str = str + ''
    if (str.length == 1) {
      return ('0' + str)
    }
    return str
  }

  get redPakage() {
      return this._redPkakage
  }

  set redPakage(value) {
    value = Math.floor(value * 100) / 100
    this.notice(value, 'redPakage')
    this._redPkakage = value
  }

  showChaseView() {
    this.postMessage({
      method: 'setRootView',
      param: {
        index: 1
      }
    })

    this.showOpenDataView()
  }

  showRankBoard() {
    this.postMessage({
      method: 'setRootView',
      param: {
        index: 0
      }
    })
    this.showOpenDataView()
  }

  hiddenBoard() {
    this.showOpenDataView(false)
  }

  showPreAfterView() {
    let score = this.score
    console.log(score, 'score ------')
    this.postMessage({
      method: 'setRootView',
      param: {
        index: 2,
        score: score
      }
    })

    this.showOpenDataView()
  }

  hiddenPreAfterView() {
    this.showOpenDataView(false)
  }

  showOpenDataView(show = true) {
    if (show) {
      this.postMessage({
        method: 'shouldRefresh',
        param: true
      })
      this.main.createOpenDataCanvase()
    } else {
      this.postMessage({
        method: 'shouldRefresh',
        param: false
      })
      this.main.removeOpenDataCanvase()
    }
  }

  get adMaxHeight() {
    let info = this.systemInfo
    if (info.model.indexOf('iPhone X') != -1) {
      return 230 + 80
    } else {
      return 230
    }
  }

  // get lastDailyRewardTime() {
  //   return window.platform.Storage.get('lastDailyRewardTime')
  // }
  //
  // set lastDailyRewardTime(value) {
  //   window.platform.Storage.set('lastDailyRewardTime', value)
  // }
  //
  // calcDateGetDailyReward(date = new Date()) {
  //   let last = this.lastDailyRewardTime
  //   if (last == null) {
  //     return true
  //   }
  //
  //   let lastDate = new Date(last)
  //
  //   if (lastDate.getDay() == date.getDay() &&
  //     lastDate.getMonth() == date.getMonth() &&
  //   lastDate.getFullYear() == date.getFullYear()) { //同一天
  //     console.log('同一天 无法领取每日奖励')
  //       return false
  //   }
  //   return true
  // }

  isPreProduct() {

    return this.preProduct_1 && this.local_build == this.build
  }

  get preProduct_get() {
    return this.isPreProduct()
  }

  saveState() {
    window.platform.setStorage({key: 'currentLife', data: this.currentLife.toString()})
  }

  async popState() {
    let currentLife = await window.platform.getStorage('currentLife')
    if (currentLife.data) {
      this.currentLife = parseInt(currentLife.data)
      console.log('currentLife:' + this.currentLife)
    }
  }

  onShowBlocks = []

  onShow() {
    console.log('onShow')
    this.onShowBlocks.map( block => {
      block()
    })

    this.onShowBlocks.length = 0

  }

  get hadShowCourse() {
      return window.platform.Storage.get('hadShowCourse')
  }

  set hadShowCourse(value) {
      window.platform.Storage.set('hadShowCourse', value)
  }

  private constructor() {

    //set interval

    setInterval(() => {
      if (this.lastGetDailyRewardTime) {
        this.dailyRewardTimeOffset = Date.now() - this.lastGetDailyRewardTime
      }
    }, 1000)

    this.gameToolData.noticeBlock = (value, key, oldValue) => {
      this.notice(value, key, oldValue)
    }

    this.setupTool()
    .then(() => this.reset())
    this.setupSetGetFunction()


    window.platform.wx['request']({
      url: this.baseUrl + 'config',
      data: {
        app: 68
      },
      success: (data) => {
        console.log(data)
        if (data.data.code != '200') {
          console.log('request switch fail')
          return
        }
        let config = JSON.parse(data.data.data)
        console.log(config)
        this.preProduct = (config.preproduct == 1)
        this.build = config.build
        this.preProduct_1 = (config.preProduct_1 == 1)
        console.log(this.isPreProduct(), 2222222222222222222222)
      }
    })


  }

  postMessage(data) {
		window.platform.postMessage(data)
  }

  showWillOverView(show: boolean = true) {
    if (show) {

		window.platform.openDataContext.postMessage({
			command: 'Play',
			// command: 'Board',
			x: 30,
			y: 130
		})
  } else {
		window.platform.openDataContext.postMessage({
			command: 'Init',
		})

  }
  }

  playSound(on: boolean = true) {
    if (on) {
        // let sound: egret.Sound = RES.getRes('mu_bg');
      if (this.AudioBg == null) {
        // this.AudioBg = sound.play();
      } else {
        // this.AudioBg = sound.play(this.AudioBg.position, 0);
      }
    } else{
        // this.AudioBg.stop()
    }
  }

  static share() {
    return this.shareInstanse
  }

  public async setupTool() {

    console.log('setup Tool')
      let isFirstGame = await window.platform.getStorage('isFirstGame')
      console.log(isFirstGame)
      if (isFirstGame.data == null) {
        this.isFirstGame = true
        return
      }

      this.isFirstGame =  isFirstGame.data == '1' ? false : true
  }

  public async reset(isRelife = false) {
    this.currentLife = 0
    this.minValue = 2
    this.maxValue = 4
    this.setScore(0)
    this.isMergeCard = false
    this.merge2048Count = 0
    this.gameOver = false
    this.highestScore = await this.getHighestScore()

    if (this.isFirstGame) {
      this.gameToolData['setHammerCount'](2)
      this.gameToolData['setRefreshCount'](2)
      this.gameToolData['setTool2048Count'](1)
      window.platform.setStorage({key: 'isFirstGame', data: '1'})
    } else {
      let hammer = await window.platform.getStorage('hammerCount')
      let refresh = await window.platform.getStorage('refreshCount')
      let tool2048 = await window.platform.getStorage('tool2048Count')

      console.log('tool count')
      console.log(hammer)
      // hammer = hammer.data ? hammer.data : 0
      // refresh = refresh.data ? refresh.data : 0
      // tool2048 = tool2048.data ? tool2048.data : 0

      this.gameToolData['setHammerCount'](hammer.data)
      this.gameToolData['setRefreshCount'](refresh.data)
      this.gameToolData['setTool2048Count'](tool2048.data)

    }
    if (isRelife) {
      this.saveState()
    }
  }



  setupSetGetFunction() {
  }

  public registerObserver(observer: Observer, key: string) {
    let lists = this.observers[key]

    if (lists == null) {
      lists = []
      this.observers[key] = lists
    }

    if (lists.indexOf(observer) == -1) {
      lists.push(observer)
    }
  }

  public removeObserver( observer: Observer, key: string) {
    if (key != null) {
      let lists = this.observers[key]
      if (lists == null) {
        return
      }

      let index = lists.indexOf(observer)
      if (index != null) {
        lists.splice(index, 1)
      }
    } else {
      for (let key in this.observers) {
        let lists = this.observers[key]
        if (lists == null) {
          continue
        }

        let index = lists.indexOf(observer)
        if (index != null) {
          lists.splice(index, 1)
        }
      }
    }
  }

  public notice(value: any, key: string, oldValue: any = this[key]) {
    let lists = this.observers[key]
    if (lists == null) {
      return
    }

    lists.map((observer) => {
      observer.subscribe(this[key], value, key, this)
    })
  }

  addBoxViewBlock: () => void

  add2048Count() {
    this.merge2048Count += 1
    this.addBoxViewBlock()
  }

  public getScore() {
    return this.score
  }

  public setScore(score: number) {
    this.notice(score, 'score')
    this.updateScore(score)

    this.postMessage({
      method: 'refreshChaseInfo',
      param: {
        score: score
      }
    })
    this.score = score
  }


  public addScore(score: number) {
    this.setScore(this.getScore() + score)
  }

  public setGameOver(isOver: boolean) {
    this.notice(isOver, 'gameOver')
    this.gameOver = isOver
  }

  public getGameOver() {
    return this.gameOver
  }

  public setIsUseHammer(is: boolean) {
    this.notice(is, 'isUseHammer')
    this.isUseHammer = is
  }

  public getIsUseHammer() {
    return this.isUseHammer
  }

  getVoiceOpen() {
    return this.voiceOpen
  }

  setVoiceOpen(open: boolean) {
    this.voiceOpen = open
    this.notice(open, 'voiceOpen')
  }

  //score
  updateScore(score: number) {
  if (score > this.highestScore) {
    this.setHighestScore(score)
  }
}

setHighestScore(highestScore: number) {
  this.notice(highestScore, 'highestScore')
  this.highestScore = highestScore
  window.platform.setStorage({key: 'highestScore', data: highestScore.toString()})
  window.platform.SetScore(highestScore)

  if (DataBus.share().userInfo == null) {
    return
  }

  //test
  window.platform.wx['request']({
  method: 'POST',
  url: DataBus.share().baseUrl  + 'update_rank',
  data: {
    x_token: DataBus.share().userInfo['x_token'],
    time: Date.now(),
    key: window.platform.sha1(DataBus.share().userInfo['id'], '' + Date.now() + '0' + highestScore),
    frequency: 0,
    pass: highestScore,
    app: DataBus.share().baseKey,
  },
  success: (data) => {
    console.log('update_rank')
    console.log(data)
    if (data.data.code != '200') {
      console.log('update_rank fail' + highestScore)
      return
    }
  }
})

}


public async getHighestScore() {
  if (this.highestScore == null) {
    let b = await  window.platform.getStorage('highestScore')
    if (b.data == null) {
      return 0

    }
    return b.data as number
  } else {
    return this.highestScore
  }
}

public async saveCurrentScore() {

  window.platform.setStorage({key: 'currentScore', data: this.score.toString()})
}

public async popScore() {
  let b = await  window.platform.getStorage('currentScore')
  this.setScore(b.data as number)
}


public playSoundWith(name) {
  if (this.voiceOpen == false) {
    return
  }

  let audio = window.platform.wx['createInnerAudioContext']()
  audio.src = 'resource/music/assets/' + name + '.mp3' // src 可以设置 http(s) 的路径，本地文件路径或者代码包文件路径
  console.log(audio.src)
  audio.play()

}

}
